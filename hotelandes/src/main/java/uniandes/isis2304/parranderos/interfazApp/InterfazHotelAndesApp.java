
package uniandes.isis2304.parranderos.interfazApp;

import java.awt.BorderLayout;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.jdo.JDODataStoreException;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

import uniandes.isis2304.parranderos.negocio.Descuento;
import uniandes.isis2304.parranderos.negocio.HotelAndes;
import uniandes.isis2304.parranderos.negocio.VOPlanUso;
import uniandes.isis2304.parranderos.negocio.VOCuenta;
import uniandes.isis2304.parranderos.negocio.VODescuento;
import uniandes.isis2304.parranderos.negocio.VOUsuario;

/**
 * Clase principal de la interfaz
 * 
 * @author Dany Benavides y Isabela Sarmiento
 */
@SuppressWarnings("serial")

public class InterfazHotelAndesApp extends JFrame implements ActionListener {
	/*
	 * ****************************************************************
	 * Constantes
	 *****************************************************************/
	/**
	 * Logger para escribir la traza de la ejecución
	 */
	private static Logger log = Logger.getLogger(InterfazHotelAndesApp.class.getName());

	/**
	 * Ruta al archivo de configuración de la interfaz
	 */
	private static final String CONFIG_INTERFAZ = "./src/main/resources/config/interfaceConfigApp.json";

	/**
	 * Ruta al archivo de configuración de los nombres de tablas de la base de
	 * datos
	 */
	private static final String CONFIG_TABLAS = "./src/main/resources/config/TablasBD_A.json";

	/*
	 * ****************************************************************
	 * Atributos
	 *****************************************************************/
	/**
	 * Objeto JSON con los nombres de las tablas de la base de datos que se
	 * quieren utilizar
	 */
	private JsonObject tableConfig;

	/**
	 * Asociación a la clase principal del negocio.
	 */
	private HotelAndes hotelAndes;

	/*
	 * ****************************************************************
	 * Atributos de interfaz
	 *****************************************************************/
	/**
	 * Objeto JSON con la configuración de interfaz de la app.
	 */
	private JsonObject guiConfig;

	/**
	 * Panel de despliegue de interacción para los requerimientos
	 */
	private PanelDatos panelDatos;

	/**
	 * Menú de la aplicación
	 */
	private JMenuBar menuBar;

	/*
	 * **************************************************************** Métodos
	 *****************************************************************/
	/**
	 * Construye la ventana principal de la aplicación. <br>
	 * <b>post:</b> Todos los componentes de la interfaz fueron inicializados.
	 */
	public InterfazHotelAndesApp() {
		// Carga la configuración de la interfaz desde un archivo JSON
		guiConfig = openConfig("Interfaz", CONFIG_INTERFAZ);

		// Configura la apariencia del frame que contiene la interfaz gráfica
		configurarFrame();
		if (guiConfig != null) {
			crearMenu(guiConfig.getAsJsonArray("menuBar"));
		}

		tableConfig = openConfig("Tablas BD", CONFIG_TABLAS);
		hotelAndes = new HotelAndes(tableConfig);

		String path = guiConfig.get("bannerPath").getAsString();
		panelDatos = new PanelDatos();

		setLayout(new BorderLayout());
		add(new JLabel(new ImageIcon(path)), BorderLayout.NORTH);
		add(panelDatos, BorderLayout.CENTER);
	}

	/*
	 * **************************************************************** Métodos
	 * de configuración de la interfaz
	 *****************************************************************/
	/**
	 * Lee datos de configuración para la aplicació, a partir de un archivo JSON
	 * o con valores por defecto si hay errores.
	 * 
	 * @param tipo
	 *            - El tipo de configuración deseada
	 * @param archConfig
	 *            - Archivo Json que contiene la configuración
	 * @return Un objeto JSON con la configuración del tipo especificado NULL si
	 *         hay un error en el archivo.
	 */
	private JsonObject openConfig(String tipo, String archConfig) {
		JsonObject config = null;
		try {
			Gson gson = new Gson();
			FileReader file = new FileReader(archConfig);
			JsonReader reader = new JsonReader(file);
			config = gson.fromJson(reader, JsonObject.class);
			log.info("Se encontró un archivo de configuración válido: " + tipo);
		} catch (Exception e) {
			// e.printStackTrace ();
			log.info("NO se encontró un archivo de configuración válido");
			JOptionPane.showMessageDialog(null,
					"No se encontró un archivo de configuración de interfaz válido: " + tipo, "Parranderos App",
					JOptionPane.ERROR_MESSAGE);
		}
		return config;
	}

	/**
	 * Método para configurar el frame principal de la aplicación
	 */
	private void configurarFrame() {
		int alto = 0;
		int ancho = 0;
		String titulo = "";

		if (guiConfig == null) {
			log.info("Se aplica configuración por defecto");
			titulo = "Parranderos APP Default";
			alto = 300;
			ancho = 500;
		} else {
			log.info("Se aplica configuración indicada en el archivo de configuración");
			titulo = guiConfig.get("title").getAsString();
			alto = guiConfig.get("frameH").getAsInt();
			ancho = guiConfig.get("frameW").getAsInt();
		}

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocation(50, 50);
		setResizable(true);
		setBackground(Color.WHITE);

		setTitle(titulo);
		setSize(ancho, alto);
	}

	/**
	 * Método para crear el menú de la aplicación con base em el objeto JSON
	 * leído Genera una barra de menú y los menús con sus respectivas opciones
	 * 
	 * @param jsonMenu
	 *            - Arreglo Json con los menùs deseados
	 */
	private void crearMenu(JsonArray jsonMenu) {
		// Creación de la barra de menús
		menuBar = new JMenuBar();
		for (JsonElement men : jsonMenu) {
			// Creación de cada uno de los menús
			JsonObject jom = men.getAsJsonObject();

			String menuTitle = jom.get("menuTitle").getAsString();
			JsonArray opciones = jom.getAsJsonArray("options");

			JMenu menu = new JMenu(menuTitle);
			menu.addMenuListener(new SampleMenuListener(this));
			menu.setActionCommand(jom.get("event").getAsString());

			for (JsonElement op : opciones) {
				// Creación de cada una de las opciones del menú
				JsonObject jo = op.getAsJsonObject();
				String lb = jo.get("label").getAsString();
				String event = jo.get("event").getAsString();

				JMenuItem mItem = new JMenuItem(lb);
				mItem.addActionListener(this);
				mItem.setActionCommand(event);
				mItem.setVisible(false);
				menu.add(mItem);
			}
			menuBar.add(menu);
		}
		setJMenuBar(menuBar);
	}

	/*
	 * ****************************************************************
	 * PRIVACIDAD
	 *****************************************************************/

	public void administrador() {
		if (esAdministrador()) {
			hacerVisible(0);
		}

	}

	public void recepcionista() {
		if (esRecepcionista()) {
			hacerVisible(1);
		}

	}

	public void empleado() {
		if (esEmpleado()) {
			hacerVisible(2);
		}

	}

	public void cliente() {
		if (esCliente()) {
			hacerVisible(3);
		}

	}

	public void organizadoreventos() {
		if (esOrganizadorEventos()) {
			hacerVisible(4);
		}

	}

	public void gerente() {
		if (esGerente()) {
			hacerVisible(5);
		}

	}

	public void funcionalidades() {
		hacerVisible(6);

	}

	public void db() {
		hacerVisible(7);

	}

	public void hacerVisible(int n) {
		JMenu menu = menuBar.getMenu(n);
		for (int j = 0; j < menu.getMenuComponentCount(); j++) {
			java.awt.Component comp = menu.getMenuComponent(j);
			if (comp instanceof JMenuItem) {
				JMenuItem menuItem = (JMenuItem) comp;
				menuItem.setVisible(true);
			}
		}

	}

	public boolean esAdministrador() {
		long id = Long.parseLong(JOptionPane.showInputDialog(this, "Ingrese su número de cedula: ",
				"Confirmación de permisos", JOptionPane.QUESTION_MESSAGE));
		long rol = hotelAndes.esAdministrador(id);
		System.out.println(rol);
		if (rol == 4)
			return true;
		else
			return false;
	}

	public boolean esRecepcionista() {
		long id = Long.parseLong(JOptionPane.showInputDialog(this, "Ingrese su número de cedula: ",
				"Confirmación de permisos", JOptionPane.QUESTION_MESSAGE));
		long rol = hotelAndes.esAdministrador(id);
		System.out.println(rol);
		if (rol == 2)
			return true;
		else
			return false;
	}

	public boolean esEmpleado() {
		long id = Long.parseLong(JOptionPane.showInputDialog(this, "Ingrese su número de cedula: ",
				"Confirmación de permisos", JOptionPane.QUESTION_MESSAGE));
		long rol = hotelAndes.esAdministrador(id);
		System.out.println(rol);
		if (rol == 3)
			return true;
		else
			return false;
	}

	public boolean esCliente() {
		long id = Long.parseLong(JOptionPane.showInputDialog(this, "Ingrese su número de cedula: ",
				"Confirmación de permisos", JOptionPane.QUESTION_MESSAGE));
		long rol = hotelAndes.esAdministrador(id);
		System.out.println(rol);
		if (rol == 1)
			return true;
		else
			return false;
	}

	public boolean esOrganizadorEventos() {
		long id = Long.parseLong(JOptionPane.showInputDialog(this, "Ingrese su número de cedula: ",
				"Confirmación de permisos", JOptionPane.QUESTION_MESSAGE));
		long rol = hotelAndes.esAdministrador(id);
		System.out.println(rol);
		if (rol == 4)
			return true;
		else
			return false;
	}

	public boolean esGerente() {
		long id = Long.parseLong(JOptionPane.showInputDialog(this, "Ingrese su número de cedula: ",
				"Confirmación de permisos", JOptionPane.QUESTION_MESSAGE));
		long rol = hotelAndes.esAdministrador(id);
		System.out.println(rol);
		if (rol == 5)
			return true;
		else
			return false;
	}

	/*
	 * **************************************************************** CRUD de
	 * Usuario
	 *****************************************************************/
	/**
	 * Adiciona un usuario con la información dada por el usuario Se crea una
	 * nueva tupla de Usuario en la base de datos, si un tipo de bebida con ese
	 * nombre no existía
	 */
	public void adicionarUsuarioR2() {
		try {
			String nombre = JOptionPane.showInputDialog(this, "Nombre del usuario: ", "Adicionar usuario",
					JOptionPane.QUESTION_MESSAGE);
			String tipoDocumento = JOptionPane.showInputDialog(this, "Tipo de documento: ", "Adicionar usuario",
					JOptionPane.QUESTION_MESSAGE);
			long numeroDocumento = Long.parseLong(JOptionPane.showInputDialog(this, "Numero de documento: ",
					"Adicionar usuario", JOptionPane.QUESTION_MESSAGE));
			String correo = JOptionPane.showInputDialog(this, "Correo: ", "Adicionar usuario",
					JOptionPane.QUESTION_MESSAGE);
			long rol = Long.parseLong(
					JOptionPane.showInputDialog(this, "Rol: ", "Adicionar usuario", JOptionPane.QUESTION_MESSAGE));

			if (nombre != null && tipoDocumento != null && rol != 0 && correo != null) {
				VOUsuario u = hotelAndes.adicionarUsuario(numeroDocumento, tipoDocumento, correo, rol, nombre);
				if (u == null) {
					throw new Exception("No se pudo crear el usuario con nombre: " + numeroDocumento);
				}
				String resultado = "En adicionarTipoBebida\n\n";
				resultado += "Usuario adicionado exitosamente: " + u;
				resultado += "\n Operación terminada";
				panelDatos.actualizarInterfaz(resultado);
			} else {
				panelDatos.actualizarInterfaz("Operación cancelada por el usuario");
			}
		} catch (Exception e) {
			// e.printStackTrace();
			String resultado = generarMensajeError(e);
			panelDatos.actualizarInterfaz(resultado);
		}
	}

	/*
	 * **************************************************************** CRUD de
	 * Plan de Consumo
	 *****************************************************************/
	/**
	 * Borra de la base de datos el tipo de bebida con el identificador dado po
	 * el usuario Cuando dicho tipo de bebida no existe, se indica que se
	 * borraron 0 registros de la base de datos
	 */
	public void adicionarPlanR6() {
		VOPlanUso r = null;
		try {
			double costo = Double.parseDouble(JOptionPane.showInputDialog(this, "Costo del Plan de Consumo: ",
					"Registrar Plan Consumo", JOptionPane.QUESTION_MESSAGE));
			String tipo = JOptionPane.showInputDialog(this,
					"Tipo Plan de consumo: \n Escribir únicamente: TodoIncluido, TiempoCompartido, LargaEstadia, Promocion, Convencion",
					"Registrar Plan Consumo", JOptionPane.QUESTION_MESSAGE).toUpperCase();
			VODescuento d = null;

			if (tipo != null) {
				r = hotelAndes.adicionarPlanUso(tipo, costo);
				if (r == null) {
					throw new Exception("No se pudo crear el plan de uso de costo $" + costo + " y de tipo " + tipo);
				}
				if (tipo.equals("TIEMPOCOMPARTIDO") || tipo.equals("TODOINCLUIDO") || tipo.equals("PROMOCION")
						|| tipo.equals("CONVENCION")) {
					d = null;
					double descuento = Double.parseDouble(JOptionPane.showInputDialog(this,
							"Por el plan de uso determinado, se debe inscribir un descuento. \n Ingrese el porcentaje del descuento: ",
							"Registrar Descuento del Plan Consumo", JOptionPane.QUESTION_MESSAGE));
					String idServicioString = JOptionPane.showInputDialog(this,
							"Ingrese el id del servicio sobre el que se hará el descuento. Si el descuento se hace sobre un producto y no un servicio, no ingrese nada",
							"Registrar Descuento del Plan Consumo", JOptionPane.QUESTION_MESSAGE);
					if (idServicioString.equals("")) {
						Long idProducto = Long.parseLong(JOptionPane.showInputDialog(this,
								"Ingrese el id del producto sobre el que se hará el descuento",
								"Registrar Descuento del Plan Consumo", JOptionPane.QUESTION_MESSAGE));
						d = hotelAndes.adicionarDescuento(descuento, null, idProducto, r.getId());
					} else {
						Long idServicio = Long.parseLong(idServicioString);
						d = hotelAndes.adicionarDescuento(descuento, idServicio, null, r.getId());
					}
					if (d == null) {
						throw new Exception("No se pudo crear el descuento, por lo que tampoco se creo el descuento");
					}
				}
				String resultado = "En adicionarPlanUso\n\n";
				resultado += "Plan Consumo adicionado exitosamente: " + r;
				resultado += "Descuento adicionado exitosamente: " + d;
				resultado += "\n Operación terminada";
				panelDatos.actualizarInterfaz(resultado);
			} else {
				panelDatos.actualizarInterfaz("Operación cancelada por el usuario");
			}
		} catch (Exception e) {
			// e.printStackTrace();
			if (r != null)
				hotelAndes.eliminarPlanUso(r.getId());
			String resultado = generarMensajeError(e);
			panelDatos.actualizarInterfaz(resultado);
		}
	}

	public void adicionarDescuento() {
		try {

			VODescuento d = null;
			Long idPlan = Long.parseLong(
					JOptionPane.showInputDialog(this, "Ingrese el id del plan sobre el que va a adicionar el descuento",
							"Registrar Descuento del Plan Consumo", JOptionPane.QUESTION_MESSAGE));
			double descuento = Double.parseDouble(JOptionPane.showInputDialog(this,
					"Por el plan de uso determinado, se debe inscribir un descuento. \nIngrese el porcentaje del descuento: ",
					"Registrar Descuento del Plan Consumo", JOptionPane.QUESTION_MESSAGE));
			String idServicioString = JOptionPane.showInputDialog(this,
					"Ingrese el id del servicio sobre el que se hará el descuento. Si el descuento se hace sobre un producto y no un servicio, no ingrese nada",
					"Registrar Descuento del Plan Consumo", JOptionPane.QUESTION_MESSAGE);
			if (idServicioString.equals("")) {
				Long idProducto = Long.parseLong(JOptionPane.showInputDialog(this,
						"Ingrese el id del producto sobre el que se hará el descuento",
						"Registrar Descuento del Plan Consumo", JOptionPane.QUESTION_MESSAGE));
				d = hotelAndes.adicionarDescuento(descuento, null, idProducto, idPlan);
			} else {
				Long idServicio = Long.parseLong(idServicioString);
				d = hotelAndes.adicionarDescuento(descuento, idServicio, null, idPlan);
			}
			if (d == null) {
				throw new Exception("No se pudo crear el descuento, por lo que tampoco se creo el descuento");
			}
			String resultado = "En adicionarDescuento\n\n";
			resultado += "Descuento adicionado exitosamente: " + d;
			resultado += "\n Operación terminada";
			panelDatos.actualizarInterfaz(resultado);
		} catch (Exception e) {
			// e.printStackTrace();
			String resultado = generarMensajeError(e);
			panelDatos.actualizarInterfaz(resultado);
		}
	}

	/*
	 * **************************************************************** CRUD de
	 * Reserva
	 *****************************************************************/
	/**
	 * Borra de la base de datos el tipo de bebida con el identificador dado po
	 * el usuario Cuando dicho tipo de bebida no existe, se indica que se
	 * borraron 0 registros de la base de datos
	 */
	public void adicionarReserva() {
		try {
			long idHabitacion = Long.parseLong(JOptionPane.showInputDialog(this, "Id de la habitación: ",
					"Adicionar reserva", JOptionPane.QUESTION_MESSAGE));
			String fechaEntrada = JOptionPane.showInputDialog(this,
					"Fecha en la que van a llegar: ex. 2019/2/31/23 (año/mes/dia/hora)", "Adicionar reserva",
					JOptionPane.QUESTION_MESSAGE);
			String[] fechaSplit = fechaEntrada.split("/");
			Timestamp fechaIn = new Timestamp(Integer.parseInt(fechaSplit[0]) - 1900, Integer.parseInt(fechaSplit[1]),
					Integer.parseInt(fechaSplit[2]), Integer.parseInt(fechaSplit[3]), 0, 0, 0);
			String fechaSalida = (JOptionPane.showInputDialog(this,
					"Fecha de salida: ex. 2019/2/31/23 (año/mes/dia/hora)", "Adicionar reserva",
					JOptionPane.QUESTION_MESSAGE));
			fechaSplit = fechaSalida.split("/");
			Timestamp fechaOut = new Timestamp(Integer.parseInt(fechaSplit[0]) - 1900, Integer.parseInt(fechaSplit[1]),
					Integer.parseInt(fechaSplit[2]), Integer.parseInt(fechaSplit[3]), 0, 0, 0);
			int estado = Integer.parseInt(
					JOptionPane.showInputDialog(this, "Estado: [0: No han llegado al hotel - 1:Llegaron al hotel]",
							"Adicionar reserva", JOptionPane.QUESTION_MESSAGE));
			long idPlanUso = Long.parseLong(JOptionPane.showInputDialog(this, "Id de Plan Uso: ", "Adicionar reserva",
					JOptionPane.QUESTION_MESSAGE));

			if (fechaEntrada != null && fechaSalida != null) {
				VOCuenta r = hotelAndes.adicionarReserva(idHabitacion, fechaIn, fechaOut, estado, idPlanUso);
				if (r == null) {
					throw new Exception("No se pudo crear la reserva con fecha de entrada: " + fechaIn);
				}
				String resultado = "En adicionarReserva\n\n";
				resultado += "Reserva adicionado exitosamente: " + r;
				resultado += "\n Operación terminada";
				panelDatos.actualizarInterfaz(resultado);
			} else {
				panelDatos.actualizarInterfaz("Operación cancelada por el usuario");
			}
		} catch (Exception e) {
			// e.printStackTrace();
			String resultado = generarMensajeError(e);
			panelDatos.actualizarInterfaz(resultado);
		}
	}

	public void consulta6() {
		String unidadTiempo = JOptionPane
				.showInputDialog(this, "Seleccione la unidad de tiempo de la consulta. Escriba: semana o mes.",
						"Consulta", JOptionPane.QUESTION_MESSAGE)
				.toLowerCase();
		String tipoPregunta = JOptionPane.showInputDialog(this,
				"Seleccione si quiere hacerlo por servicios o por tipo de habitaciones. Escriba: servicio o tipo.",
				"Consulta", JOptionPane.QUESTION_MESSAGE).toLowerCase();
		String detalle = JOptionPane.showInputDialog(this,
				"Si escogio tipo, escriba el nombre del tipo textual. Solo son validas las escrituras: Doble, Familiar, Suite presidencial"
						+ "\n Si escogió un servicio, escriba el id del servicio deseado",
				"Consulta", JOptionPane.QUESTION_MESSAGE).toLowerCase();
		try {
			List<String> r = hotelAndes.consulta6(unidadTiempo, tipoPregunta, detalle);
			String resp = "";
			for (String m : r) {
				resp += m + "\n";
			}
			panelDatos.actualizarInterfaz(resp);
		} catch (Exception e) {
			String resultado = generarMensajeError(e);
			panelDatos.actualizarInterfaz(resultado);

		}

	}

	public void consulta7() {
		String tipoConsulta = JOptionPane.showInputDialog(this,
				"Seleccione el criterio: tiempo de estadia o cantidad de consumo. Escriba respectivametne 1 o 2",
				"Consulta", JOptionPane.QUESTION_MESSAGE).toLowerCase();
		try {
			List r = hotelAndes.consulta7(tipoConsulta);
			String resp = "";
			for (Object m : r) {
				resp += m.toString() + "\n";
			}
			panelDatos.actualizarInterfaz(resp);
		} catch (Exception e) {
			String resultado = generarMensajeError(e);
			panelDatos.actualizarInterfaz(resultado);

		}
	}

	public void consulta8() {
		String unidadTiempo = JOptionPane
				.showInputDialog(this, "Seleccione la unidad de tiempo de la consulta. Escriba: semana o mes.",
						"Consulta", JOptionPane.QUESTION_MESSAGE)
				.toLowerCase();
		try {
			List r = hotelAndes.consulta8();
			String resp = "";
			for (Object m : r) {
				resp += m.toString() + "\n";
			}
			panelDatos.actualizarInterfaz(resp);
		} catch (Exception e) {
			String resultado = generarMensajeError(e);
			panelDatos.actualizarInterfaz(resultado);

		}
	}

	@SuppressWarnings("deprecation")
	public void consulta9() {
		try {
			String fechaEntrada = JOptionPane.showInputDialog(this,
					"Fecha inicial consulta: ex. 2019/2/31/23 (año/mes/dia/hora)", "Consultar consumo",
					JOptionPane.QUESTION_MESSAGE);
			String[] fechaSplit = fechaEntrada.split("/");
			Timestamp fechaIni = new Timestamp(Integer.parseInt(fechaSplit[0]) - 1900, Integer.parseInt(fechaSplit[1]),
					Integer.parseInt(fechaSplit[2]), Integer.parseInt(fechaSplit[3]), 0, 0, 0);
			String fechaSalida = (JOptionPane.showInputDialog(this,
					"Fecha de salida: ex. 2019/2/31/23 (año/mes/dia/hora)", "Consultar consumo",
					JOptionPane.QUESTION_MESSAGE));
			fechaSplit = fechaSalida.split("/");
			Timestamp fechaFin = new Timestamp(Integer.parseInt(fechaSplit[0]) - 1900, Integer.parseInt(fechaSplit[1]),
					Integer.parseInt(fechaSplit[2]), Integer.parseInt(fechaSplit[3]), 0, 0, 0);

			System.out.println(fechaIni);
			System.out.println(fechaFin);
			
			List todosServicios = hotelAndes.darTodosServiciosR15();
			Object[] arregloServicios = todosServicios.toArray();
			JList listaServicios = new JList(arregloServicios);
			JOptionPane.showMessageDialog(null, listaServicios, "Seleccione el servicio para la consulta",
					JOptionPane.QUESTION_MESSAGE);
			List servicioParaBuscar = (List) listaServicios.getSelectedValuesList();

			Object[] tiposDeOrdenamiento = tiposDeOrdenamiento();
			System.out.println(tiposDeOrdenamiento[0]);
			JList tiposDeOrd = new JList(tiposDeOrdenamiento);
			JOptionPane.showMessageDialog(null, tiposDeOrd, "Seleccione cómo quiere ordenar los clientes",
					JOptionPane.QUESTION_MESSAGE);
			List tipoDeOrdenamientoo = (List) tiposDeOrd.getSelectedValuesList();

			List r = hotelAndes.consulta9(fechaIni, fechaFin, servicioParaBuscar, tipoDeOrdenamientoo);
			String resp = "";

			if (tipoDeOrdenamientoo.get(0).equals("Número de veces que utilizó el servicio")) {
				
				List r2 = hotelAndes.vecesConsulta9(fechaIni, fechaFin, servicioParaBuscar);
				System.out.println(r2.get(0));
				System.out.println(r2.get(1));
				System.out.println(r2.get(2));
				System.out.println(r2.get(r2.size()-1));
				System.out.println(r2.get(r2.size()-2));
				System.out.println(r2.size());
				for (int i = 0; i < r.size(); i++) {
					resp += r.get(i).toString() + ": " + r2.get(i).toString() + " veces.\n";
				}
			} else {
				for (Object m : r) {
					resp += m.toString() + "\n";
				}
			}
			panelDatos.actualizarInterfaz(resp);
		} catch (Exception e) {
			String resultado = generarMensajeError(e);
			panelDatos.actualizarInterfaz(resultado);
			e.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	public void consulta10() {
		try {
			System.out.println("WTFFFFF");
			String fechaEntrada = JOptionPane.showInputDialog(this,
					"Fecha inicial consulta: ex. 2019/2/31/23 (año/mes/dia/hora)", "Consultar NO Servicio",
					JOptionPane.QUESTION_MESSAGE);
			String[] fechaSplit = fechaEntrada.split("/");
			Timestamp fechaIni = new Timestamp(Integer.parseInt(fechaSplit[0]) - 1900, Integer.parseInt(fechaSplit[1]),
					Integer.parseInt(fechaSplit[2]), Integer.parseInt(fechaSplit[3]), 0, 0, 0);
			String fechaSalida = (JOptionPane.showInputDialog(this,
					"Fecha de salida: ex. 2019/2/31/23 (año/mes/dia/hora)", "Consultar NO Servicio",
					JOptionPane.QUESTION_MESSAGE));
			fechaSplit = fechaSalida.split("/");
			Timestamp fechaFin = new Timestamp(Integer.parseInt(fechaSplit[0]) - 1900, Integer.parseInt(fechaSplit[1]),
					Integer.parseInt(fechaSplit[2]), Integer.parseInt(fechaSplit[3]), 0, 0, 0);

			List todosServicios = hotelAndes.darTodosServiciosR15();
			Object[] arregloServicios = todosServicios.toArray();
			JList listaServicios = new JList(arregloServicios);
			JOptionPane.showMessageDialog(null, listaServicios, "Seleccione el servicio para la NO consulta",
					JOptionPane.QUESTION_MESSAGE);
			List servicioParaBuscar = (List) listaServicios.getSelectedValuesList();

			Object[] tiposDeOrdenamiento = tiposDeOrdenamiento();
			System.out.println(tiposDeOrdenamiento[0]);
			JList tiposDeOrd = new JList(tiposDeOrdenamiento);
			JOptionPane.showMessageDialog(null, tiposDeOrd, "Seleccione cómo quiere ordenar los clientes",
					JOptionPane.QUESTION_MESSAGE);
			List tipoDeOrdenamientoo = (List) tiposDeOrd.getSelectedValuesList();

			List r = hotelAndes.consulta10(fechaIni, fechaFin, servicioParaBuscar, tipoDeOrdenamientoo);

			String resp = "";

			for (Object m : r) {
				resp += m.toString() + "\n";
			}
			panelDatos.actualizarInterfaz(resp);
		} catch (Exception e) {
			String resultado = generarMensajeError(e);
			panelDatos.actualizarInterfaz(resultado);
			e.printStackTrace();
		}
	}

	public Object[] tiposDeOrdenamiento() {
		String[] tipos = new String[4];
		tipos[0] = "Fecha";
		tipos[1] = "Número de veces que utilizó el servicio";
		tipos[2] = "Nombre de usuario";
		tipos[3] = "Número de documento";
		return tipos;
	}

	public void consulta11() {
		try {
			String criterio = JOptionPane.showInputDialog(this,
					"Seleccione el criterio de la consulta:\n"
							+ "Si desea conocer los servicios más y menos consumidos, escriba 1\n"
							+ "Si desea conocer las habitaciones más y menos reservados, escriba 2\n",
					"Consulta", JOptionPane.QUESTION_MESSAGE).toLowerCase();
			List r = hotelAndes.consulta11(Integer.valueOf(criterio));
			String resp = "";
			for (Object m : r) {
				resp += m.toString() + "\n";
			}
			panelDatos.actualizarInterfaz(resp);
		} catch (Exception e) {
			String resultado = generarMensajeError(e);
			panelDatos.actualizarInterfaz(resultado);

		}
	}

	public void consulta12() {
		String criterio = JOptionPane.showInputDialog(this,
				"Seleccione el criterio de la consulta:\n" + "Una estadía cada trimestre, escriba 1\n"
						+ "Consumo costoso, escriba 2\n" + "SPA o Salones por más de 3 horas , escriba 3\n",
				"Consulta", JOptionPane.QUESTION_MESSAGE).toLowerCase();
		try {
			List r = hotelAndes.consulta12(Integer.valueOf(criterio));
			String resp = "";
			for (Object m : r) {
				resp += m.toString() + "\n";
			}
			panelDatos.actualizarInterfaz(resp);
		} catch (Exception e) {
			String resultado = generarMensajeError(e);
			panelDatos.actualizarInterfaz(resultado);

		}
	}

	/**
	 * Actualiza el estado de una reserva cuando un cliente se va.
	 */
	public void actualizarReservaR11() {
		try {
			long numeroDocumento = Long.parseLong(JOptionPane.showInputDialog(this,
					"Numero de documetno del cliente que llega: ", "Llegada cliente", JOptionPane.QUESTION_MESSAGE));
			String tipoDocumento = JOptionPane.showInputDialog(this, "Tipo de documento: ex. cc", "Llegada cliente",
					JOptionPane.QUESTION_MESSAGE);

			if (numeroDocumento != 0 && tipoDocumento != null) {
				hotelAndes.actualizarReservaR11(numeroDocumento, tipoDocumento);
				String resultado = "En actualizarReservaR11\n\n";
				resultado += "Reserva actualizada exitosamente: ";
				resultado += "\n Operación terminada";
				panelDatos.actualizarInterfaz(resultado);
			} else {
				panelDatos.actualizarInterfaz("Operación cancelada por el usuario");
			}
		} catch (Exception e) {
			// e.printStackTrace();
			String resultado = generarMensajeError(e);
			panelDatos.actualizarInterfaz(resultado);
		}
	}

	/**
	 * Actualiza la reserva del cliente con el identificador dado por el usuario
	 * Cuando dicho tipo de bebida no existe, se indica que se borraron 0
	 * registros de la base de datos
	 */
	public void actualizarReservaR9() {
		try {
			long numeroDocumento = Long.parseLong(JOptionPane.showInputDialog(this,
					"Numero de documetno del cliente que llega: ", "Llegada cliente", JOptionPane.QUESTION_MESSAGE));
			String tipoDocumento = JOptionPane.showInputDialog(this, "Tipo de documento: ex. cc", "Llegada cliente",
					JOptionPane.QUESTION_MESSAGE);

			if (numeroDocumento != 0 && tipoDocumento != null) {
				hotelAndes.actualizarReserva(numeroDocumento, tipoDocumento);
				String resultado = "En actualizarReservaR9\n\n";
				resultado += "Reserva actualizada exitosamente: ";
				resultado += "\n Operación terminada";
				panelDatos.actualizarInterfaz(resultado);
			} else {
				panelDatos.actualizarInterfaz("Operación cancelada por el usuario");
			}
		} catch (Exception e) {
			// e.printStackTrace();
			String resultado = generarMensajeError(e);
			panelDatos.actualizarInterfaz(resultado);
		}
	}

	/*
	 * **************************************************************** CRUD de
	 * SERVICIO
	 *****************************************************************/
	/**
	 * Reserva un servicio si se puede reserva Cuando dicho tipo de bebida no
	 * existe, se indica que se borraron 0 registros de la base de datos
	 */
	public void reservarR8() {
		try {
			long idServicio = Long.parseLong(JOptionPane.showInputDialog(this, "Id del servicio que desea reserva: ",
					"Reservar servicio cliente", JOptionPane.QUESTION_MESSAGE));

			if (idServicio > 0) {
				boolean seReservo = hotelAndes.reservarR8(idServicio);
				String resultado = "En reservarR8\n\n";
				if (seReservo)
					resultado += "Reserva actualizada exitosamente: ";
				else
					resultado += "Reserva no se pudo realizar ya que el servicio ya esta ocupado: ";
				resultado += "\n Operación terminada";
				panelDatos.actualizarInterfaz(resultado);
			} else {
				panelDatos.actualizarInterfaz("Operación cancelada por el usuario");
			}
		} catch (Exception e) {
			// e.printStackTrace();
			String resultado = generarMensajeError(e);
			panelDatos.actualizarInterfaz(resultado);
		}
	}

	/**
	 * Reserva un servicio si se puede reserva Cuando dicho tipo de bebida no
	 * existe, se indica que se borraron 0 registros de la base de datos
	 */
	public void consumoR10() {
		try {
			long numeroDocumento = Long.parseLong(JOptionPane.showInputDialog(this,
					"Numero de documetno del empleado: ", "Registro consumo", JOptionPane.QUESTION_MESSAGE));
			String tipoDocumento = JOptionPane.showInputDialog(this, "Tipo de documento: ex. CC", "Registro consumo",
					JOptionPane.QUESTION_MESSAGE);

			long idCuenta = Long
					.parseLong(JOptionPane.showInputDialog(this, "ID de la cuenta a nombre de la que queda el servicio",
							"Registro consumo", JOptionPane.QUESTION_MESSAGE));
			int estado = Integer.parseInt(JOptionPane.showInputDialog(this,
					"Estado del consumo a registrar. \n0 = reserva \n1=pagado \n2=pendiente", "Registro consumo",
					JOptionPane.QUESTION_MESSAGE));

			String opcion = JOptionPane.showInputDialog(this,
					"Tipo de consumo a registrar. Ecriba literalmente: Servicio, Producto, Salon", "Registro consumo",
					JOptionPane.QUESTION_MESSAGE).toLowerCase();
			String fechaStart = "";
			String fechaEnd = "";
			if (opcion.equals("servicio")) {
				long idServicio = Long.parseLong(JOptionPane.showInputDialog(this, "ID del servicio a Reservar",
						"Registro consumo", JOptionPane.QUESTION_MESSAGE));
				fechaStart = JOptionPane.showInputDialog(this,
						"Comienzo de la reserva del servicio. \nSiga el formato '2019-03-31 16:00:00",
						"Registro consumo", JOptionPane.QUESTION_MESSAGE);
				fechaEnd = JOptionPane.showInputDialog(this,
						"Fin de la reserva del servicio. \nSiga el formato '2019-03-31 16:00:00", "Registro consumo",
						JOptionPane.QUESTION_MESSAGE);
				hotelAndes.registrarConsumo(estado, null, null, idCuenta, idServicio, numeroDocumento, tipoDocumento,
						fechaStart, fechaEnd);
			} else if (opcion.equals("producto")) {
				long idProudcto = Long.parseLong(JOptionPane.showInputDialog(this, "ID del servicio a Reservar",
						"Registro consumo", JOptionPane.QUESTION_MESSAGE));
				fechaStart = JOptionPane.showInputDialog(this,
						"Comienzo de la reserva del servicio. \nSiga el formato '2019-03-31 16:00:00",
						"Registro consumo", JOptionPane.QUESTION_MESSAGE);
				fechaEnd = JOptionPane.showInputDialog(this,
						"Fin de la reserva del servicio. \nSiga el formato '2019-03-31 16:00:00", "Registro consumo",
						JOptionPane.QUESTION_MESSAGE);
				hotelAndes.registrarConsumo(estado, idProudcto, null, idCuenta, null, numeroDocumento, tipoDocumento,
						fechaStart, fechaEnd);
			} else if (opcion.equals("salon")) {
				long idSalon = Long.parseLong(JOptionPane.showInputDialog(this, "ID del servicio a Reservar",
						"Registro consumo", JOptionPane.QUESTION_MESSAGE));
				fechaStart = JOptionPane.showInputDialog(this,
						"Comienzo de la reserva del servicio. \nSiga el formato '2019-03-31 16:00:00",
						"Registro consumo", JOptionPane.QUESTION_MESSAGE);
				fechaEnd = JOptionPane.showInputDialog(this,
						"Fin de la reserva del servicio. \nSiga el formato '2019-03-31 16:00:00", "Registro consumo",
						JOptionPane.QUESTION_MESSAGE);
				hotelAndes.registrarConsumo(estado, null, idSalon, idCuenta, null, numeroDocumento, tipoDocumento,
						fechaStart, fechaEnd);
			}
		} catch (Exception e) {
			// e.printStackTrace();
			String resultado = generarMensajeError(e);
			panelDatos.actualizarInterfaz(resultado);
		}
	}
	/*
	 * **************************************************************** CRUD de
	 * Convención
	 *****************************************************************/

	public void reservarR12() {
		try {
			long numeroDocumento = Long.parseLong(JOptionPane.showInputDialog(this,
					"Numero de documetno del organizador: ", "Reserva Convencion", JOptionPane.QUESTION_MESSAGE));
			String tipoDocumento = JOptionPane.showInputDialog(this, "Tipo de documento: ex. CC", "Reserva Convencion",
					JOptionPane.QUESTION_MESSAGE);

			long idPlanUso = Long.parseLong(JOptionPane.showInputDialog(this, "ID del Plan de Consumo acordado: ",
					"Reserva Convencion", JOptionPane.QUESTION_MESSAGE));

			String fechaIn = JOptionPane.showInputDialog(this, "Fecha de Ingreso. \nSiga el formato '2019-03-31",
					"Reserva Convencion", JOptionPane.QUESTION_MESSAGE);
			String fechaOut = JOptionPane.showInputDialog(this, "Fecha de Salida. \nSiga el formato '2019-03-31",
					"Reserva Convencion", JOptionPane.QUESTION_MESSAGE);

			long idCuenta = hotelAndes.crearCuentaOrganizador(numeroDocumento, tipoDocumento, idPlanUso, fechaIn,
					fechaOut);

			ArrayList<String> tipos = hotelAndes.darTiposHabitaciones();
			int a = 0;
			for (String tipo : tipos) {
				a = Integer.parseInt(JOptionPane.showInputDialog(this, "Numero de habitaciones de tipo " + tipo,
						"Reserva Convencion", JOptionPane.QUESTION_MESSAGE));
				String resultado1 = hotelAndes.reservarHabitaciones(tipo, a, fechaIn, fechaOut, idPlanUso);
				panelDatos.actualizarInterfaz(resultado1);

			}

			int reply = JOptionPane.showConfirmDialog(null, "¿Desea hacer una reserva sobre un servicio?",
					"Reserva Convencion", JOptionPane.YES_NO_OPTION);
			while (reply == JOptionPane.YES_OPTION) {

				long idServicio = Long.parseLong(JOptionPane.showInputDialog(this, "ID del servicio a Reservar",
						"Reserva Convencion", JOptionPane.QUESTION_MESSAGE));
				int personas = Integer
						.parseInt(JOptionPane.showInputDialog(this, "Cantidad de personas consideradas en la reserva",
								"Reserva Convencion", JOptionPane.QUESTION_MESSAGE));

				if (personas < hotelAndes.darCapacidadServicio(idServicio)) {

					String fechaStart = JOptionPane.showInputDialog(this,
							"Comienzo de la reserva del servicio. \nSiga el formato '2019-03-31 16:00:00",
							"Reserva Convencion", JOptionPane.QUESTION_MESSAGE);
					String fechaEnd = JOptionPane.showInputDialog(this,
							"Fin de la reserva del servicio. \nSiga el formato '2019-03-31 16:00:00",
							"Reserva Convencion", JOptionPane.QUESTION_MESSAGE);
					// revisar si el servicio se puede reservar con base a
					// capacidad
					String resultado = hotelAndes.registrarConsumo(0, null, null, idCuenta, idServicio, numeroDocumento,
							tipoDocumento, fechaStart, fechaEnd);
					panelDatos.actualizarInterfaz(resultado);
				} else {
					panelDatos.actualizarInterfaz("El servicio" + idServicio
							+ "no tiene la capacidad requerida por lo que no se puede reservar");
				}
				reply = JOptionPane.showConfirmDialog(null, "¿Desea hacer una nueva reserva sobre un servicio?",
						"Reserva Convencion", JOptionPane.YES_NO_OPTION);

			}

		} catch (Exception e) {
			// e.printStackTrace();
			String resultado = generarMensajeError(e);
			panelDatos.actualizarInterfaz(resultado);
		}
	}

	public void cancelarServConvR13() {

		long idConvencion = Long.parseLong(
				JOptionPane.showInputDialog(this, "Numero de la convención de la que desea cancelar servicios: ",
						"Cancelar Servicios Convencion", JOptionPane.QUESTION_MESSAGE));
		int seguir = 0;
		while (seguir == 0) {
			List servicios = hotelAndes.traerServiciosConvencionR13(idConvencion);
			if (servicios.size() != 0) {
				Object[] servicios2 = servicios.toArray();
				BigDecimal input = ((BigDecimal) JOptionPane.showInputDialog(null,
						"Eliga el servicio que desea cancelar", "Cancelar Servicios Convención",
						JOptionPane.QUESTION_MESSAGE, null, servicios2, servicios2[0]));
				try {
					hotelAndes.cancelarServiciosConvencionR13(input.longValue());
				} catch (Exception e) {
					JOptionPane.showMessageDialog(this, "No se cancelo el servicio", "Error",
							JOptionPane.ERROR_MESSAGE);
				}
				seguir = JOptionPane.showConfirmDialog(null, "¿Desea cancelar más servicios?", "Por favor elija",
						JOptionPane.YES_NO_OPTION);
				System.out.println(seguir);
			} else {
				seguir = 1;
				JOptionPane.showMessageDialog(this, "Esta convencion no tiene servicios", "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		}

		long idCuenta = Long.parseLong(
				JOptionPane.showInputDialog(this, "Numero de la convención de la que desea cancelar habitaciones: ",
						"Cancelar Habitaciones Convencion", JOptionPane.QUESTION_MESSAGE));
		seguir = 0;
		while (seguir == 0) {
			List habitaciones = hotelAndes.traerHabitacionesConvencionR13(idConvencion);
			if (habitaciones.size() != 0) {
				Object[] servicios2 = habitaciones.toArray();
				BigDecimal input = ((BigDecimal) JOptionPane.showInputDialog(null,
						"Eliga la habitacion que desea cancelar", "Cancelar Habitacion Convención",
						JOptionPane.QUESTION_MESSAGE, null, servicios2, servicios2[0]));
				hotelAndes.cancelarHabitacionConvencionR13(input.longValue());
				seguir = JOptionPane.showConfirmDialog(null, "¿Desea cancelar más habitaciones?", "Por favor elija",
						JOptionPane.YES_NO_OPTION);
				System.out.println(seguir);
			} else {
				seguir = 1;
				JOptionPane.showMessageDialog(this, "Esta convencion no tiene servicios", "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	public void finConvencionR14() {

		long idCuenta = Long
				.parseLong(JOptionPane.showInputDialog(this, "Numero de la cuenta (convención) a finalizar: ",
						"Finalización Convencion", JOptionPane.QUESTION_MESSAGE));
		try {
			hotelAndes.finalizarReserva(idCuenta);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			generarMensajeError(e);
		}

	}

	/*
	 * **************************************************************** CRUD de
	 * SERVICIO
	 *****************************************************************/

	public void mantenimientoR15() {
		List todasHabitaciones = hotelAndes.darTodasHabitacionesR15();
		Object[] arregloHabitaciones = todasHabitaciones.toArray();
		JList listaHabitaciones = new JList(arregloHabitaciones);
		JOptionPane.showMessageDialog(null, listaHabitaciones,
				"Seleccione las habitaciones que se van a poner en mantenimiento", JOptionPane.QUESTION_MESSAGE);
		List habitacionesParaMantenimiento = (List) listaHabitaciones.getSelectedValuesList();

		List todosServicios = hotelAndes.darTodosServiciosR15();
		Object[] arregloServicios = todosServicios.toArray();
		JList listaServicios = new JList(arregloServicios);
		JOptionPane.showMessageDialog(null, listaServicios,
				"Seleccione los servicios que se van a poner en mantenimiento", JOptionPane.QUESTION_MESSAGE);
		List serviciosParaMantenimiento = (List) listaHabitaciones.getSelectedValuesList();

		String fechaStart = JOptionPane.showInputDialog(this,
				"Comienzo de la reserva del servicio. \nSiga el formato '2019-03-31 16:00:00", "Mantenimiento hotel",
				JOptionPane.QUESTION_MESSAGE);
		String fechaEnd = JOptionPane.showInputDialog(this,
				"Fin de la reserva del servicio. \nSiga el formato '2019-03-31 16:00:00", "Mantenimiento hotel",
				JOptionPane.QUESTION_MESSAGE);
		String causa = JOptionPane.showInputDialog(this, "Causa del mantenimiento", "Mantenimiento hotel",
				JOptionPane.QUESTION_MESSAGE);

		List habitacionesActivas = hotelAndes.darHabitacionesActivasR15();
		List serviciosActivos = hotelAndes.darServiciosActivosR15();

		System.out.println("Llega aquí");
		try {
			hotelAndes.mantenimientoR15(habitacionesParaMantenimiento, serviciosParaMantenimiento, habitacionesActivas,
					serviciosActivos, fechaStart, fechaEnd, causa);
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			generarMensajeError(e);
		}
	}

	public void finMantenimientoR16() {
		List todosMantenimientos = hotelAndes.darTodosMantenimientosR16();
		Object[] arregloMantenimietnos = todosMantenimientos.toArray();
		JList listaMantenimientos = new JList(arregloMantenimietnos);
		JOptionPane.showMessageDialog(null, listaMantenimientos, "Seleccione los mantenimientos que desea cancelar",
				JOptionPane.QUESTION_MESSAGE);
		List habitacionesParaMantenimiento = (List) listaMantenimientos.getSelectedValuesList();
		hotelAndes.finalizarMantenimiento(habitacionesParaMantenimiento);
	}
	
	public void comparaciontiempos(){
		
		
		List tablaUsuarioNumeros = hotelAndes.darTodasUsuarioNumeros();
		List tablaResponsableNumeros = hotelAndes.darTodasResponsableNumeros();
		
		List join = new ArrayList();
		
		for (Object usuario: tablaUsuarioNumeros){
			for (Object responsable: tablaResponsableNumeros){
				if(usuario == responsable){
					join.add(usuario);
				}
			}
		}
		System.out.println(tablaUsuarioNumeros.size());
		System.out.println(tablaResponsableNumeros.size());		
		System.out.println(join.size());

	}

	/*
	 * **************************************************************** Métodos
	 * administrativos
	 *****************************************************************/
	/**
	 * Muestra el log de Parranderos
	 */
	public void mostrarLogParranderos() {
		mostrarArchivo("parranderos.log");
	}

	/**
	 * Muestra el log de datanucleus
	 */
	public void mostrarLogDatanuecleus() {
		mostrarArchivo("datanucleus.log");
	}

	/**
	 * Limpia el contenido del log de parranderos Muestra en el panel de datos
	 * la traza de la ejecución
	 */
	public void limpiarLogParranderos() {
		// Ejecución de la operación y recolección de los resultados
		boolean resp = limpiarArchivo("parranderos.log");

		// Generación de la cadena de caracteres con la traza de la ejecución de
		// la demo
		String resultado = "\n\n************ Limpiando el log de parranderos ************ \n";
		resultado += "Archivo " + (resp ? "limpiado exitosamente" : "NO PUDO ser limpiado !!");
		resultado += "\nLimpieza terminada";

		panelDatos.actualizarInterfaz(resultado);
	}

	/**
	 * Limpia el contenido del log de datanucleus Muestra en el panel de datos
	 * la traza de la ejecución
	 */
	public void limpiarLogDatanucleus() {
		// Ejecución de la operación y recolección de los resultados
		boolean resp = limpiarArchivo("datanucleus.log");

		// Generación de la cadena de caracteres con la traza de la ejecución de
		// la demo
		String resultado = "\n\n************ Limpiando el log de datanucleus ************ \n";
		resultado += "Archivo " + (resp ? "limpiado exitosamente" : "NO PUDO ser limpiado !!");
		resultado += "\nLimpieza terminada";

		panelDatos.actualizarInterfaz(resultado);
	}

	/**
	 * Muestra la presentación general del proyecto
	 */
	public void mostrarPresentacionGeneral() {
		mostrarArchivo("data/00-ST-ParranderosJDO.pdf");
	}

	/**
	 * Muestra el modelo conceptual de Parranderos
	 */
	public void mostrarModeloConceptual() {
		mostrarArchivo("data/Modelo Conceptual Parranderos.pdf");
	}

	/**
	 * Muestra el esquema de la base de datos de Parranderos
	 */
	public void mostrarEsquemaBD() {
		mostrarArchivo("data/Esquema BD Parranderos.pdf");
	}

	/**
	 * Muestra el script de creación de la base de datos
	 */
	public void mostrarScriptBD() {
		mostrarArchivo("data/EsquemaParranderos.sql");
	}

	/**
	 * Muestra la arquitectura de referencia para Parranderos
	 */
	public void mostrarArqRef() {
		mostrarArchivo("data/ArquitecturaReferencia.pdf");
	}

	/**
	 * Muestra la documentación Javadoc del proyectp
	 */
	public void mostrarJavadoc() {
		mostrarArchivo("doc/index.html");
	}

	/**
	 * Muestra la información acerca del desarrollo de esta apicación
	 */
	public void acercaDe() {
		hacerVisible(8);

		String resultado = "\n\n ************************************\n\n";
		resultado += " * Universidad	de	los	Andes	(Bogotá	- Colombia)\n";
		resultado += " * Departamento	de	Ingeniería	de	Sistemas	y	Computación\n";
		resultado += " * Licenciado	bajo	el	esquema	Academic Free License versión 2.1\n";
		resultado += " * \n";
		resultado += " * Curso: isis2304 - Sistemas Transaccionales\n";
		resultado += " * Proyecto: Parranderos Uniandes\n";
		resultado += " * @version 1.0\n";
		resultado += " * @author Germán Bravo\n";
		resultado += " * Julio de 2018\n";
		resultado += " * \n";
		resultado += " * Revisado por: Claudia Jiménez, Christian Ariza\n";
		resultado += "\n ************************************\n\n";

		panelDatos.actualizarInterfaz(resultado);
	}

	/*
	 * **************************************************************** Métodos
	 * privados para la presentación de resultados y otras operaciones
	 *****************************************************************/

	/**
	 * Genera una cadena de caracteres con la descripción de la excepcion e,
	 * haciendo énfasis en las excepcionsde JDO
	 * 
	 * @param e
	 *            - La excepción recibida
	 * @return La descripción de la excepción, cuando es
	 *         javax.jdo.JDODataStoreException, "" de lo contrario
	 */
	private String darDetalleException(Exception e) {
		String resp = "";
		if (e.getClass().getName().equals("javax.jdo.JDODataStoreException")) {
			JDODataStoreException je = (javax.jdo.JDODataStoreException) e;
			return je.getNestedExceptions()[0].getMessage();
		}
		return resp;
	}

	/**
	 * Genera una cadena para indicar al usuario que hubo un error en la
	 * aplicación
	 * 
	 * @param e
	 *            - La excepción generada
	 * @return La cadena con la información de la excepción y detalles
	 *         adicionales
	 */
	private String generarMensajeError(Exception e) {
		String resultado = "************ Error en la ejecución\n";
		resultado += e.getLocalizedMessage() + ", " + darDetalleException(e);
		resultado += "\n\nRevise datanucleus.log y parranderos.log para más detalles";
		return resultado;
	}

	/**
	 * Limpia el contenido de un archivo dado su nombre
	 * 
	 * @param nombreArchivo
	 *            - El nombre del archivo que se quiere borrar
	 * @return true si se pudo limpiar
	 */
	private boolean limpiarArchivo(String nombreArchivo) {
		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new FileWriter(new File(nombreArchivo)));
			bw.write("");
			bw.close();
			return true;
		} catch (IOException e) {
			// e.printStackTrace();
			return false;
		}
	}

	/**
	 * Abre el archivo dado como parámetro con la aplicación por defecto del
	 * sistema
	 * 
	 * @param nombreArchivo
	 *            - El nombre del archivo que se quiere mostrar
	 */
	private void mostrarArchivo(String nombreArchivo) {
		try {
			Desktop.getDesktop().open(new File(nombreArchivo));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * **************************************************************** Métodos
	 * de la Interacción
	 *****************************************************************/
	/**
	 * Método para la ejecución de los eventos que enlazan el menú con los
	 * métodos de negocio Invoca al método correspondiente según el evento
	 * recibido
	 * 
	 * @param pEvento
	 *            - El evento del usuario
	 */
	@Override
	public void actionPerformed(ActionEvent pEvento) {
		String evento = pEvento.getActionCommand();
		System.out.println(evento);
		try {
			Method req = InterfazHotelAndesApp.class.getMethod(evento);
			req.invoke(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * **************************************************************** Programa
	 * principal
	 *****************************************************************/
	/**
	 * Este método ejecuta la aplicación, creando una nueva interfaz
	 * 
	 * @param args
	 *            Arreglo de argumentos que se recibe por línea de comandos
	 */
	public static void main(String[] args) {
		try {

			// Unifica la interfaz para Mac y para Windows.
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
			InterfazHotelAndesApp interfaz = new InterfazHotelAndesApp();
			interfaz.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	class SampleMenuListener implements MenuListener {

		InterfazHotelAndesApp inter;

		public SampleMenuListener(InterfazHotelAndesApp p) {
			super();
			inter = p;
		}

		@Override
		public void menuSelected(MenuEvent e) {
			String evento = e.toString().substring(e.toString().indexOf("text="), e.toString().length() - 2);
			evento = evento.substring(5).toLowerCase();
			System.out.println(evento);
			if (evento == "administrador") {

			} else if (evento == "recepcionista") {

			} else if (evento == "empleado") {

			} else if (evento == "cliente") {

			} else if (evento == "organizadoreventos") {

			} else if (evento == "gerente") {

			} else if (evento == "funcionalidades") {

			}
			try {
				Method req = InterfazHotelAndesApp.class.getMethod(evento);
				req.invoke(inter);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		@Override
		public void menuDeselected(MenuEvent e) {
			System.out.println("menuDeselected");
		}

		@Override
		public void menuCanceled(MenuEvent e) {
			System.out.println("menuCanceled");
		}

	}

}
