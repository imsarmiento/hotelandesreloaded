package uniandes.isis2304.parranderos.negocio;

import java.sql.Timestamp;

public interface VOHorario {

	/**
	 * @return the id
	 */
	long getId();

	/**
	 * @return the fechaInicio
	 */
	Timestamp getFechaInicio();

	/**
	 * @return the horaInicio
	 */
	Timestamp getHoraInicio();

	/**
	 * @return the duracion
	 */
	double getDuracion();

}