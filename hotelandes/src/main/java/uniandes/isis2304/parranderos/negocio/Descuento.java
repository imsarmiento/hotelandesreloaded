package uniandes.isis2304.parranderos.negocio;

/**
 * Clase para modelar el concepto de Descuento
 *
 * @author Dany Benavides y Isabela Sarmiento
 */
public class Descuento implements VODescuento {

	
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El identificador UNICO 
	 */
	private Long id;

	/**
	 * El porcentaje
	 */
	private double porcentaje;
	
	/**
	 * El idServicio
	 */
	private Long idServicio;
	
	/**
	 * El idProducto
	 */
	private Long idProducto;
	
	/**
	 * El idProducto
	 */
	private Long idPlan;
	
	

	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Constructor por defecto
	 */
	public Descuento() {
		this.id =null;
		this.porcentaje = 0;
		this.idServicio = null;
		this.idProducto = null;
		this.idPlan = null;
	}

	/**
	 * Constructor con valores
	 * @param id
	 * @param porcentaje
	 * @param idServicio
	 * @param idProducto
	 * @param idPlan
	 */
	public Descuento(long id, double porcentaje, long idServicio, long idProducto, long idPlan) {
		this.id = id;
		this.porcentaje = porcentaje;
		this.idServicio = idServicio;
		this.idProducto = idProducto;
		this.idPlan = idPlan;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VODescuento#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VODescuento#getPorcentaje()
	 */
	@Override
	public double getPorcentaje() {
		return porcentaje;
	}

	/**
	 * @param porcentaje the porcentaje to set
	 */
	public void setPorcentaje(double porcentaje) {
		this.porcentaje = porcentaje;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VODescuento#getIdServicio()
	 */
	@Override
	public Long getIdServicio() {
		return idServicio;
	}

	/**
	 * @param idServicio the idServicio to set
	 */
	public void setIdServicio(long idServicio) {
		this.idServicio = idServicio;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VODescuento#getIdProducto()
	 */
	@Override
	public Long getIdProducto() {
		return idProducto;
	}

	/**
	 * @param idProducto the idProducto to set
	 */
	public void setIdProducto(long idProducto) {
		this.idProducto = idProducto;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VODescuento#getIdPlan()
	 */
	@Override
	public Long getIdPlan() {
		return idPlan;
	}

	/**
	 * @param idPlan the idPlan to set
	 */
	public void setIdPlan(long idPlan) {
		this.idPlan = idPlan;
	}
	
	/**
	 * @return Una cadena con la información básica del plan uso
	 */
	@Override
	public String toString() 
	{
		return "Descuento [id=" + id + ", porcentaje=" + porcentaje + ", idServicio=" + idServicio+ ", idProducto=" + idProducto+  ", idPlan=" + idPlan+"]";
	}


	
}
