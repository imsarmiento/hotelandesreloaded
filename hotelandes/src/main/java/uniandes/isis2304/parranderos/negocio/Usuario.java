package uniandes.isis2304.parranderos.negocio;
/**
 * Clase para modelar el concepto de usuario
 *
 * @author Dany Benavides y Isabela Sarmiento
 */
public class Usuario implements VOUsuario{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El numero de documento ÚNICO del usuario
	 */
	private long numeroDocumento;
	/**
	 * El tipo de documento
	 */
	private String tipoDocumento;
	/**
	 * El correo del usuario
	 */
	private String correo;
	
	/**
	 * El identificador del tipo de rol del usuario. Debe existir en la tabla de rol
	 */
	private long rol;
	/**
	 * El nombre del usuario
	 */
	private String nombre;
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Constructor por defecto
	 */
	public Usuario() 
	{
		this.numeroDocumento = 0;
		this.tipoDocumento = "";
		this.correo = "";
		this.rol = 0;
		this.nombre = "";
	}
	/**
	 * Constructor con valores
	 * @param numeroDocumento - El identificador ÚNICO del usuario
	 * @param tipoDocumento - El tipo de documento
	 * @param correo - El correo del usuario.
	 * @param el identificador del tipo de rol del usuario. Debe existir en la tabla de rol
	 * @param nombre - El nombre del usuario 
	 */
	public Usuario(long numeroDocumento, String tipoDocumento,String correo, long rol,String nombre) 
	{
		this.numeroDocumento = numeroDocumento;
		this.tipoDocumento = tipoDocumento;
		this.correo = correo;
		this.rol = rol;
		this.nombre = nombre;
	}
	/**
	 * @return El numero de documento ÚNICO del usuario
	 */
	public long getNumeroDocumento() {
		return numeroDocumento;
	}
	/**
	 * @param numeroDocumento - El nuevo numero de documento ÚNICO del usuario
	 */
	public void setNumeroDocumento(long numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	/**
	 * @return El tipo de documento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	/**
	 * @param tipoDocumento - El nuevo tipo de documento
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	/**
	 * @return El correo del usuario
	 */
	public String getCorreo() {
		return correo;
	}
	/**
	 * @param correo - El correo del usuario
	 */
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	/**
	 * @return El identificador del tipo de rol del usuario.
	 */
	public long getRol(){
		return rol;
	}
	/**
	 * @param rol: el identificador del tipo de rol del usuario. Debe existir en la tabla de rol
	 */
	public void setRol(long rol) {
		this.rol = rol;
	}
	/**
	 * @return El nombre del usuario
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre - El nombre del usuario
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return Una cadena de caracteres con la información básica del acompañante
	 */
	@Override
	public String toString(){
		return "Usuario [numeroDocumento=" + numeroDocumento + ", tipoDocumento=" + tipoDocumento +", correo=" + correo + ", rol=" + rol+", nombre=" + nombre+"]";
	}
}
