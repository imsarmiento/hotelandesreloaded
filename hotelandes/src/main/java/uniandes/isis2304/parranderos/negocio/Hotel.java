package uniandes.isis2304.parranderos.negocio;
/**
 * Clase para modelar el concepto del hotel
 *
 * @author Dany Benavides y Isabela Sarmiento
 */
public class Hotel implements VOHotel{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El identificador ÚNICO del hotel
	 */
	private long idHotel;
	/**
	 * El nombre del hotel
	 */
	private String nombre;
	/**
	 * La ciudad del hotel
	 */
	private String ciudad;
	/**
	 * El identificador de la cadena a la que pertenece.
	 */
	private long idCadena;
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Constructor por defecto
	 */
	public Hotel() 
	{
		this.idHotel = 0;
		this.nombre = "";
		this.ciudad = "";
		this.idCadena = 0;
	}
	/**
	 * Constructor con valores
	 * @param idHotel - El identificador ÚNICO del hotel
	 * @param nombre - El nombre del hotel
	 * @param ciudad - La ciudad del hotel
	 * @param idCadena - El identificador de la cadena a la que pertenece.
	 */
	public Hotel(long idHotel, String nombre, String ciudad, long idCadena) 
	{
		this.idHotel = idHotel;
		this.nombre = nombre;
		this.ciudad = ciudad;
		this.idCadena = idCadena;
	}
	/**
	 * @return El id de la cadena
	 */
	public long getIdHotel() {
		return idHotel;
	}
	/**
	 * @param idHotel - El nuevo identificador ÚNICO del hotel
	 */
	public void setIdHotel(long idHotel) {
		this.idHotel = idHotel;
	}
	/**
	 * @return El id de la cadena
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre - El nuevo nombre del hotel
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return El id de la cadena
	 */
	public String getCiudad() {
		return ciudad;
	}
	/**
	 * @param ciudad - La nueva ciudad del hotel
	 */
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	/**
	 * @return El id de la cadena
	 */
	public long getIdCadena() {
		return idCadena;
	}
	/**
	 * @param idCadena - El nuevo identificador de la cadena a la que pertenece.
	 */
	public void setIdCadena(long idCadena) {
		this.idCadena = idCadena;
	}
	/**
	 * @return Una cadena de caracteres con la información básica del hotel
	 */
	@Override
	public String toString(){
		return "Hotel [idHotel=" + idHotel + ", nombre=" + nombre +", ciudad=" + ciudad + ", idCadena=" + idCadena + "]";
	}
}
