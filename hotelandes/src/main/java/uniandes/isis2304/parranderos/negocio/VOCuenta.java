package uniandes.isis2304.parranderos.negocio;

import java.sql.Timestamp;

/**
 * Interfaz para los métodos get de Reserva.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz 
 * 
 * @author Dany Benavides y Isabela Sarmiento
 */
public interface VOCuenta {
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * @return the idReserva
	 */
	public long getIdCuenta();
	/**
	 * @return the fechaIn
	 */
	public Timestamp getFechaIn();
	/**
	 * @return the fechaOut
	 */
	public Timestamp getFechaOut();
	/**
	 * @return the estado
	 */
	public int getEstado();
	/**
	 * @return the idHabitacion
	 */
	public long getIdHabitacion();
	/**
	 * @return the idPlan
	 */
	public long getIdPlan();
	/**
	 * @return Una cadena de caracteres con la información básica de la reserva
	 */
	@Override
	public String toString();

}
