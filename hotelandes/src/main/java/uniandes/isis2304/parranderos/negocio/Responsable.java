package uniandes.isis2304.parranderos.negocio;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

/**
 * Clase para modelar el concepto cliente del hotel
 *
 * @author Dany Benavides y Isabela Sarimento
 */
public class Responsable implements VOResponsable
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El numero de documento. Junto con el tipo de documento son UNICOS. 
	 */
	private long numeroDocumento;
	
	/**
	 * El tipo de documento. Junto con el num de docuemento son UNICOS 
	 */
	private String tipoDocumento;
	
	/**
	 * El id de la reserva del cliente
	 */
	private long idReserva;
	
	private String tipo;
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Constructor por defecto
	 */
	public Responsable() 
	{
		this.numeroDocumento = 0;
		this.tipoDocumento = "";
		this.idReserva = 0;
		this.tipo= "INDIVIDUO";
	}

	/**
	 * Constructor con valores
	 * @param numeroDocumento - El identificador Junto con el tipo de documento son UNICOS
	 * @param tipoDocumento - El tipo de documento Junto con el num de docuemento son UNICOS
	 * @param idReserva - El identificador de la reserva a la que pertenece.
	 */
	public Responsable(long numeroDocumento, String tipoDocumento, long idReserva, String tipo) 
	{
		this.numeroDocumento = numeroDocumento;
		this.tipoDocumento = tipoDocumento;
		this.idReserva = idReserva;
		this.tipo = tipo;
	}

	/**
	 * @return El numero de documento. Junto con el tipo de documento son UNICOS.
	 */
	public long getNumeroDocumento() {
		return numeroDocumento;
	}
	/**
	 * @param numeroDocumento - El nuevo numero de documento. Junto con el tipo de documento son UNICOS.
	 */
	public void setNumeroDocumento(long numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	/**
	 * @return El tipo de documento  Junto con el num de docuemento son UNICOS.
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	/**
	 * @param tipoDocumento - El tipo de documento  Junto con el num de docuemento son UNICOS.
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	/**
	 * @return El identificador de la reserva a la que pertenece.
	 */
	public long getIdReserva() {
		return idReserva;
	}
	/**
	 * @param idReserva - El identificador de la reserva a la que pertenece.
	 */
	public void setIdReserva(long idReserva) {
		this.idReserva = idReserva;
	}
	
	/**
	 * @param idReserva - El identificador de la reserva a la que pertenece.
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param idReserva - El identificador de la reserva a la que pertenece.
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return Una cadena de caracteres con la información básica del acompañante
	 */
	@Override
	public String toString(){
		return "Responsable [numeroDocumento=" + numeroDocumento + ", tipoDocumento=" + tipoDocumento +", idReserva=" + idReserva + "]";
	}
}

