package uniandes.isis2304.parranderos.negocio;

/**
 * Clase para modelar el concepto cadena del hotel
 *
 * @author Dany Benavides y Isabela Sarimento
 */
public class Cadena implements VOCadena{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El identificador ÚNICO de la cadena
	 */
	private long idCadena;
	
	/**
	 * El nombre de la cadena
	 */
	private String nombre;
	
	/**
	 * 1 indica que la cadena es internacional 0 si no lo es
	 */
	private int international;
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Constructor por defecto
	 */
	public Cadena() 
	{
		this.idCadena = 0;
		this.nombre = "";
		this.international = 0;
	}
	/**
	 * Constructor con valores
	 * @param idCadena - El id de la cadena
	 * @param nombre - El nombre de la cadena
	 * @param international - 1 indica que la cadena es internacional 0 si no lo es
	 */
	public Cadena(long idCadena, String nombre, int international) 
	{
		this.idCadena = idCadena;
		this.nombre = nombre;
		this.international = international;
	}
	/**
	 * @return El id de la cadena
	 */
	public long getIdCadena() {
		return idCadena;
	}
	/**
	 * @param idCadena - El nuevo id de la cadena
	 */
	public void setIdCadena(long idCadena) {
		this.idCadena = idCadena;
	}
	/**
	 * @return El nombre de la cadena
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre - El nuevo nombre de la cadena
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return El indicador de si la cadena es internacional o no
	 */
	public int getInternational() {
		return international;
	}
	/**
	 * @param international - El nuevo indicador de si la cadena es internacional o no
	 */
	public void setInternational(int international) {
		this.international = international;
	}
	/**
	 * @return Una cadena de caracteres con la información básica de la cadena
	 */
	@Override
	public String toString(){
		return "Cadena [idCadena=" + idCadena + ", nombre=" + nombre + ", international=" + international + "]";
	}
}
