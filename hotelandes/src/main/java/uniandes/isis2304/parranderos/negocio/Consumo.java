package uniandes.isis2304.parranderos.negocio;

import java.sql.Timestamp;

/**
 * Clase para modelar el concepto de consumo
 *
 * @author Dany Benavides y Isabela Sarmiento
 */
public class Consumo implements VOConsumo{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 *  El id UNICO del consumo
	 */
	private long id;
	
	/**
	 *  fecha del consumo
	 */
	private Long idHorario;
	
	
	/**
	 *  El id de la Reserva. Debe estar en la tabla Reservas
	 */
	private long idCuenta;
	
	/**
	 *  El id del servicio. Debe estar en la tabla servicios.
	 */
	private long idServicio;
	
	/**
	 * El numero de documento del empleado que factura el consumo. Debe estar en la tabla usuario y se de tipo empleado
	 */
	private long numDocEmpleado;
	
	/**
	 *  El tipo de documento del empleado que factura el consumo. Debe estar en la tabla usuario y se de tipo empleado
	 */
	private String tipoDocEmpleado;

	private int estado;
	
	private long idProducto;
	
	private long idSalon;
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Constructor por defecto
	 */
	public Consumo() 
	{
		this.id = 0;
		this.idHorario =  null;
		this.idCuenta = 0;
		this.idServicio = 0;
		this.numDocEmpleado  = 0;
		this.tipoDocEmpleado = "";
		this.estado=0;
		this.idProducto = 0;
		this.idSalon = 0;
	}
	
	/**
	 * Constructor con valores
	 * @param numeroDocumento - El identificador ÚNICO del usuario
	 * @param tipoDocumento - El tipo de documento
	 * @param correo - El correo del usuario.
	 * @param el identificador del tipo de rol del usuario. Debe existir en la tabla de rol
	 * @param nombre - El nombre del usuario 
	 */
	public Consumo(long id, Long fecha,long idReserva, long idServicio,long numDocEmpleado, String tipoDocEmpleado) 
	{
		this.id = 0;
		this.idHorario =  fecha;
		this.idCuenta = idReserva;
		this.idServicio = idServicio;
		this.numDocEmpleado  = numDocEmpleado;
		this.tipoDocEmpleado = tipoDocEmpleado;
	}
	
	
	
	
	/**
	 * @return el id del consumo
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id el nuevo id del consumo
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return la fecha
	 */
	public Long getIdHorario() {
		return idHorario;
	}

	/**
	 * @param fecha la fecha nueva  del consumo
	 */
	public void setIdHorario(Long fecha) {
		this.idHorario = fecha;
	}

	/**
	 * @return el idReserva del consumo
	 */
	public long getIdCuenta() {
		return idCuenta;
	}

	/**
	 * @param idReserva el nuevo idReserva del consumo
	 */
	public void setIdCuenta(long idReserva) {
		this.idCuenta = idReserva;
	}

	/**
	 * @return el idServicio  del consumo
	 */
	public long getIdServicio() {
		return idServicio;
	}

	/**
	 * @param idServicio el nuevo idServicio del consumo
	 */
	public void setIdServicio(long idServicio) {
		this.idServicio = idServicio;
	}

	/**
	 * @return el numDocEmpleado del consumo
	 */
	public long getNumDocEmpleado() {
		return numDocEmpleado;
	}

	/**
	 * @param numDocEmpleado el nuevo numDocEmpleado del consumo
	 */
	public void setNumDocEmpleado(long numDocEmpleado) {
		this.numDocEmpleado = numDocEmpleado;
	}

	/**
	 * @return el tipoDocEmpleado
	 */
	public String getTipoDocEmpleado() {
		return tipoDocEmpleado;
	}

	/**
	 * @param tipoDocEmpleado el nuevo tipoDocEmpleado del consumo
	 */
	public void setTipoDocEmpleado(String tipoDocEmpleado) {
		this.tipoDocEmpleado = tipoDocEmpleado;
	}

	/**
	 * @return the estado
	 */
	public int getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(int estado) {
		this.estado = estado;
	}

	/**
	 * @return the idProducto
	 */
	public long getIdProducto() {
		return idProducto;
	}

	/**
	 * @param idProducto the idProducto to set
	 */
	public void setIdProducto(long idProducto) {
		this.idProducto = idProducto;
	}

	/**
	 * @return the idSalon
	 */
	public long getIdSalon() {
		return idSalon;
	}

	/**
	 * @param idSalon the idSalon to set
	 */
	public void setIdSalon(long idSalon) {
		this.idSalon = idSalon;
	}

	/**
	 * @return Una cadena de caracteres con la información básica consumo
	 */
	@Override
	public String toString(){
		return "Consumo [id=" + id + ", id del horrio=" + idHorario +", id de la reserva=" + idCuenta + ", id del servicio consumido=" + idServicio +", numero documento del empleado que factura =" + numDocEmpleado + ", tipo doc empleado que factura =" + tipoDocEmpleado+" ]";
	}
}
