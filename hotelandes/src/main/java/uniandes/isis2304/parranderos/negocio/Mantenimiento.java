package uniandes.isis2304.parranderos.negocio;

import java.sql.Timestamp;

/**
 * Clase para modelar el concepto de mantenimiento
 *
 * @author Dany Benavides y Isabela Sarmiento
 */
public class Mantenimiento implements VOMantenimiento {

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 *  El id UNICO del mantenimiento
	 */
	private long id;
	
	/**
	 *  El id del servicio. Debe estar en la tabla servicios.
	 */
	private long idServicio;
	
	/**
	 * El numero de la habitacion
	 */
	private long idHabitacion;
	
	/**
	 * El horario del mantenimiento
	 */
	private long idHorario;
	
	/**
	 * La causa del mantenimiento
	 */
	private String causa;

	
	
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Constructor por defecto
	 */
	public Mantenimiento() {
		this.id = 0;
		this.idServicio = 0;
		this.idHabitacion = 0;
		this.idHorario = 0;
		this.causa = "";
	}
	
	/**
	 * Constructor con valores
	 * @param id
	 * @param idServicio
	 * @param idHabitacion
	 * @param idHorario
	 * @param causa
	 */
	public Mantenimiento(long id, long idServicio, long idHabitacion, long idHorario, String causa) {
		this.id = id;
		this.idServicio = idServicio;
		this.idHabitacion = idHabitacion;
		this.idHorario = idHorario;
		this.causa = causa;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOMantenimiento#getId()
	 */
	@Override
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOMantenimiento#getIdServicio()
	 */
	@Override
	public long getIdServicio() {
		return idServicio;
	}

	/**
	 * @param idServicio the idServicio to set
	 */
	public void setIdServicio(long idServicio) {
		this.idServicio = idServicio;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOMantenimiento#getIdHabitacion()
	 */
	@Override
	public long getIdHabitacion() {
		return idHabitacion;
	}

	/**
	 * @param idHabitacion the idHabitacion to set
	 */
	public void setIdHabitacion(long idHabitacion) {
		this.idHabitacion = idHabitacion;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOMantenimiento#getIdHorario()
	 */
	@Override
	public long getIdHorario() {
		return idHorario;
	}

	/**
	 * @param idHorario the idHorario to set
	 */
	public void setIdHorario(long idHorario) {
		this.idHorario = idHorario;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOMantenimiento#getCausa()
	 */
	@Override
	public String getCausa() {
		return causa;
	}

	/**
	 * @param causa the causa to set
	 */
	public void setCausa(String causa) {
		this.causa = causa;
	}
	
}
