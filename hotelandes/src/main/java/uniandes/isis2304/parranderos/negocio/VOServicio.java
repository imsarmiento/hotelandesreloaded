package uniandes.isis2304.parranderos.negocio;

public interface VOServicio {

	/**
	 * @return the id
	 */
	long getId();

	/**
	 * @return the idHorario
	 */
	long getIdHorario();

}