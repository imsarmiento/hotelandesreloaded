package uniandes.isis2304.parranderos.negocio;
/**
 * Interfaz para los métodos get de Hotel.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz 
 * 
 * @author Dany Benavides y Isabela Sarmiento
 */
public interface VOHotel {
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * @return El id de la cadena
	 */
	public long getIdHotel();
	/**
	 * @return El id de la cadena
	 */
	public String getNombre();
	/**
	 * @return El id de la cadena
	 */
	public String getCiudad();
	/**
	 * @return El id de la cadena
	 */
	public long getIdCadena();
	/**
	 * @return Una cadena de caracteres con la información básica del hotel
	 */
	@Override
	public String toString();
}
