package uniandes.isis2304.parranderos.negocio;
/**
 * Interfaz para los métodos get de TipoHabitacion.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz 
 * 
 * @author Dany Benavides y Isabela Sarmiento
 */
public interface VOTipoHabitacion {
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * @return El identificador ÚNICO del tipo de habitacion
	 */
	public long getId();
	/**
	 * @return El nombre del tipo de habitacion
	 */
	public String getNombre();
	/**
	 * @param nombre - El nombre del tipo de habitacion
	 */
	public void setNombre(String nombre);
	/**
	 * @param capacidad - La nueva capacidad de personas de la habitacion
	 */
	public void setCapacidad(int capacidad);
	/**
	 * @return El costo por quedarse en esa habitacion
	 */
	public int getCosto();
	/**
	 * @return Una cadena de caracteres con la información básica del tipo habitación
	 */
	@Override
	public String toString();
}
