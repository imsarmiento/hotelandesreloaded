package uniandes.isis2304.parranderos.negocio;

public interface VOServicioBasico {

	/**
	 * @return the id
	 */
	long getId();

	/**
	 * @return the costo
	 */
	double getCosto();

	/**
	 * @return the capacidad
	 */
	int getCapacidad();

}