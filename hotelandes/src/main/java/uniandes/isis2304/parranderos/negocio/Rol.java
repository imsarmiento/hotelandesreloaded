
package uniandes.isis2304.parranderos.negocio;

/**
 * Clase para modelar el concepto Rol del hotel
 *
 * @author Dany Benavides y Isabela Sarimento
 */
public class Rol implements VORol
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El identificador del rol
	 */
	private long id;

	/**
	 * El nombre del rol
	 */
	private String nombre;

	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Constructor por defecto
	 */
	public Rol() 
	{
		this.id = 0;
		this.nombre = "Default";
	}

	/**
	 * Constructor con valores
	 * @param id - El identificador del rol
	 * @param nombre - El nombre del rol
	 */
	public Rol(long id, String nombre) 
	{
		this.id = id;
		this.nombre = nombre;
	}

	/**
	 * @return El id del rol
	 */
	public long getId() 
	{
		return id;
	}

	/**
	 * @param id - El nuevo id del rol
	 */
	public void setId(long id) 
	{
		this.id = id;
	}

	/**
	 * @return El nombre del rol
	 */
	public String getNombre() 
	{
		return nombre;
	}

	/**
	 * @param nombre - El nuevo nombre del rol
	 */
	public void setNombre(String nombre) 
	{
		this.nombre = nombre;
	}


	/**
	 * @return Una cadena de caracteres con la información del rol
	 */
	@Override
	public String toString() 
	{
		return "Rol [id=" + id + ", nombre=" + nombre + "]";
	}

	/**
	 * @param tipo - El rol a comparar
	 * @return True si tienen el mismo nombre
	 */
	public boolean equals(Object tipo) 
	{
		Rol tb = (Rol) tipo;
		return id == tb.id && nombre.equalsIgnoreCase (tb.nombre);
	}

}
