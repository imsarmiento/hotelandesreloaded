package uniandes.isis2304.parranderos.negocio;

public interface VODescuento {

	/**
	 * @return the id
	 */
	Long getId();

	/**
	 * @return the porcentaje
	 */
	double getPorcentaje();

	/**
	 * @return the idServicio
	 */
	Long getIdServicio();

	/**
	 * @return the idProducto
	 */
	Long getIdProducto();

	/**
	 * @return the idPlan
	 */
	Long getIdPlan();

}