package uniandes.isis2304.parranderos.negocio;

/**
 * Clase para modelar el concepto Producto
 *
 * @author Dany Benavides y Isabela Sarimento
 */
public class Producto implements VOProducto {

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El identificador ÚNICO del producto
	 */
	private long id;
	
	/**
	 * El id del hotel al que pertence
	 */
	private String nombre;
	
	/**
	 * Indica el tipo de habitacion, se encuentra en otra tabla.
	 */
	private double valor;
	
	/**
	 * Indica el tipo de habitacion, se encuentra en otra tabla.
	 */
	private int cantidad;
	
	/**
	 * Indica el tipo de habitacion, se encuentra en otra tabla.
	 */
	private double duracion;
	
	/**
	 * El identificador ÚNICO de la habitacion
	 */
	private long idVentaProducto;

	
	
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Constructor por defecto
	 */
	public Producto() {
		this.id = 0;
		this.nombre = "";
		this.valor = 0;
		this.cantidad = 0;
		this.duracion = 0;
		this.idVentaProducto = 0;
	}
	
	/**
	 * Constructor con valores
	 * @param id
	 * @param nombre
	 * @param valor
	 * @param cantidad
	 * @param duracion
	 * @param idVentaProducto
	 */
	public Producto(long id, String nombre, double valor, int cantidad, double duracion, long idVentaProducto) {
		this.id = id;
		this.nombre = nombre;
		this.valor = valor;
		this.cantidad = cantidad;
		this.duracion = duracion;
		this.idVentaProducto = idVentaProducto;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOProducto#getId()
	 */
	@Override
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOProducto#getNombre()
	 */
	@Override
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOProducto#getValor()
	 */
	@Override
	public double getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(double valor) {
		this.valor = valor;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOProducto#getCantidad()
	 */
	@Override
	public int getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOProducto#getDuracion()
	 */
	@Override
	public double getDuracion() {
		return duracion;
	}

	/**
	 * @param duracion the duracion to set
	 */
	public void setDuracion(double duracion) {
		this.duracion = duracion;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOProducto#getIdVentaProducto()
	 */
	@Override
	public long getIdVentaProducto() {
		return idVentaProducto;
	}

	/**
	 * @param idVentaProducto the idVentaProducto to set
	 */
	public void setIdVentaProducto(long idVentaProducto) {
		this.idVentaProducto = idVentaProducto;
	}
	
	
}
