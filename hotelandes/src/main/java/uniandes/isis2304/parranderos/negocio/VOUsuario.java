package uniandes.isis2304.parranderos.negocio;
/**
 * Interfaz para los métodos get de Acompaniantes.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz 
 * 
 * @author Dany Benavides y Isabela Sarmiento
 */
public interface VOUsuario {
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * @return El numero de documento ÚNICO del usuario
	 */
	public long getNumeroDocumento() ;
	/**
	 * @return El tipo de documento
	 */
	public String getTipoDocumento();
	/**
	 * @return El correo del usuario
	 */
	public String getCorreo();
	/**
	 * @return El identificador del tipo de rol del usuario. Debe existir en la tabla de rol
	 */
	public long getRol();
	/**
	 * @return El nombre del usuario
	 */
	public String getNombre();
	/**
	 * @return Una cadena de caracteres con la información básica del acompañante
	 */
	@Override
	public String toString();
}
