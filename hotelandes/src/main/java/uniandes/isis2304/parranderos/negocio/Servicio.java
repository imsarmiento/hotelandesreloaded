package uniandes.isis2304.parranderos.negocio;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

/**
 * Clase para modelar el concepto Servicio del hotel
 *
 * @author Dany Benavides y Isabela Sarimento
 */
public class Servicio implements VOServicio {

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 *	El id del servicio UNICO
	 */
	private long id;

	/**
	 * @return El horario del servicio.
	 */
	private long idHorario;

	
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Constructor por defecto
	 */
	public Servicio() {
		this.id = 0;
		this.idHorario = 0;
	}

	/**
	 * Constructor con valores
	 * @param id
	 * @param idHorario
	 */
	public Servicio(long id, long idHorario) {
		this.id = id;
		this.idHorario = idHorario;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOServicio#getId()
	 */
	@Override
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOServicio#getIdHorario()
	 */
	@Override
	public long getIdHorario() {
		return idHorario;
	}

	/**
	 * @param idHorario the idHorario to set
	 */
	public void setIdHorario(long idHorario) {
		this.idHorario = idHorario;
	}
	
	
	
}
