package uniandes.isis2304.parranderos.negocio;

/**
 * Clase para modelar el concepto PlanUso del hotel
 *
 * @author Dany Benavides y Isabela Sarimento
 */
public class PlanUso implements VOPlanUso
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Se definen los tipos de promociones en las constantes
	 */
	
	public final static String TODO_INCLUIDO = "TodoIncluido";
	public final static String TIEMPO_COMPARTIDO = "TiempoCompartido";
	public final static String LARGA_ESTADIA = "LargaEstadia";
	public final static String PROMOCION = "Promocion";

	
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El identificador UNICO del PlanUso
	 */
	private long id;
	
	/**
	 * El tipo del plan de uso CK(IN('TodoIncluido','TiempoCompartido','LargaEstadia','Promocion'))
	 */
	private String tipo;
	
	/**
	 * El costo del PlanUso
	 */
	private double costo;
	

	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Constructor por defecto
	 */
	public PlanUso() 
	{
		this.id = 0;
		this.tipo = "";
		this.costo = 0;

	}

	/**
	 * Constructor con valores
	 * @param id - El id del plan de uso
	 * @param tipo - El tipo del plan de uso CK(tipo IN ('TodoIncluido','TiempoCompartido','LargaEstadia','Promocion'))
	 * @param costo - El costo del PlanUso
	 * @param descuento - El descuento que tiene le plan de uso. Si no tiene descuento = 0;
	 * @param nombre - El nombre de la promocion si es de tipo promocion

	 */
	public PlanUso(long id, String tipo, double costo) 
	{
		this.id = id;
		this.tipo = tipo;
		this.costo = costo;
	}

	/**
	 * @return El id de la bebida
	 */
	public long getId() 
	{
		return id;
	}

	/**
	 * @param id - El nuevo id de la bebida 
	 */
	public void setId(long id) 
	{
		this.id = id;
	}

	
	/**
	 * @return El id de la bebida
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param id - El nuevo id de la bebida 
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return El costo del PlanUso
	 */
	public double getCosto() {
		return costo;
	}

	/**
	 * @param El nuevo costo del PlanUso
	 */
	public void setCosto(double costo) {
		this.costo = costo;
	}


	

	/**
	 * @return Una cadena con la información básica del plan uso
	 */
	@Override
	public String toString() 
	{
		return "Plan de uso [id=" + id + ", tipo=" + tipo + ", costo=" + costo+ "]";
	}

}
