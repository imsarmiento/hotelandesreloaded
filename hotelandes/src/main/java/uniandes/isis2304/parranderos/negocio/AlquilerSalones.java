package uniandes.isis2304.parranderos.negocio;

/**
 * Clase para modelar el concepto alquilar salones
 *
 * @author Dany Benavides y Isabela Sarimento
 */
public class AlquilerSalones implements VOAlquilerSalones {

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El identificador ÚNICO del Alquiler de salon
	 */
	private long id;
	/**
	 * La capacidad
	 */
	private int capacidad;
	/**
	 * El costo por hora
	 */
	private double costo;
	/**
	 * El tipo lugar
	 */
	private String tipo;

	 
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	
	/**
	 * Constructor por defecto
	 */
	public AlquilerSalones() {
		this.id = 0;
		this.capacidad = 0;
		this.costo = 0;
		this.tipo = "";
	}
	
	/**
	 * Constructor con valores
	 * @param id
	 * @param capacidad
	 * @param costo
	 * @param tipo
	 */
	public AlquilerSalones(long id, int capacidad, double costo, String tipo) {
		this.id = id;
		this.capacidad = capacidad;
		this.costo = costo;
		this.tipo = tipo;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOAlquilerSalones#getId()
	 */
	@Override
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOAlquilerSalones#getCapacidad()
	 */
	@Override
	public int getCapacidad() {
		return capacidad;
	}

	/**
	 * @param capacidad the capacidad to set
	 */
	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOAlquilerSalones#getCosto()
	 */
	@Override
	public double getCosto() {
		return costo;
	}

	/**
	 * @param costo the costo to set
	 */
	public void setCosto(double costo) {
		this.costo = costo;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOAlquilerSalones#getTipo()
	 */
	@Override
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
	
	
}
