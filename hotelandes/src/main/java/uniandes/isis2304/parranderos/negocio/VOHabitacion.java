package uniandes.isis2304.parranderos.negocio;
/**
 * Interfaz para los métodos get de Habitacion.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz 
 * 
 * @author Dany Benavides y Isabela Sarimento
 */
public interface VOHabitacion {
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * @return El id de la habitacion
	 */
	public long getIdHabitacion();
	/**
	 * @return El id del hotel al que pertence
	 */
	public long getIdHotel();
	/**
	 * @return El tipo de habitacion, se encuentra en otra tabla.
	 */
	public long getTipo();
	/**
	 * @return Una cadena de caracteres con la información básica de la habitación
	 */
	@Override
	public String toString();
}
