package uniandes.isis2304.parranderos.negocio;

import java.sql.Timestamp;

/**
 * Clase para modelar el concepto cadena del reserva
 *
 * @author Dany Benavides y Isabela Sarmiento
 */
public class Cuenta implements VOCuenta{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El identificador ÚNICO de la cuenta
	 */
	private long idCuenta;
	/**
	 * La fecha de ingreso
	 */
	private Timestamp fechaIn;
	/**
	 * La fecha de salida
	 */
	private Timestamp fechaOut;
	/**
	 * El estado de la reserva FK en TipoEstados
	 */
	private int estado;
	/**
	 * El identificador de la habitacion en la que se quedan
	 */
	private long idHabitacion;
	/**
	 * El identificador del plan que tomaron
	 */
	private long idPlan;
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Constructor por defecto
	 */
	public Cuenta() 
	{
		this.idCuenta = 0;
		this.fechaIn = new Timestamp (0);
		this.fechaOut = new Timestamp (0);
		this.estado = 0;
		this.idHabitacion = 0;
		this.idPlan = 0;
	}
	
	/**
	 * Constructor con valores
	 * @param idReserva - El id de la reserva
	 * @param fechaIn - La fecha de ingreso
	 * @param fechaOut - La fecha de salida
	 * @param estado - El estado de la reserva FK en TipoEstados
	 * @param idHabitacion - El id de la habitacion
	 * @param idPlan - El id de la plan
	 */
	public Cuenta(long idReserva, Timestamp fechaIn,Timestamp fechaOut, int estado,long idHabitacion,long idPlan) 
	{
		this.idCuenta = 0;
		this.fechaIn = new Timestamp (0);
		this.fechaOut = new Timestamp (0);
		this.estado = 0;
		this.idHabitacion = 0;
		this.idPlan = 0;
	}

	/**
	 * @return the idReserva
	 */
	public long getIdCuenta() {
		return idCuenta;
	}

	/**
	 * @param idReserva the idReserva to set
	 */
	public void setIdCuenta(long idCuenta) {
		this.idCuenta = idCuenta;
	}

	/**
	 * @return the fechaIn
	 */
	public Timestamp getFechaIn() {
		return fechaIn;
	}

	/**
	 * @param fechaIn the fechaIn to set
	 */
	public void setFechaIn(Timestamp fechaIn) {
		this.fechaIn = fechaIn;
	}

	/**
	 * @return the fechaOut
	 */
	public Timestamp getFechaOut() {
		return fechaOut;
	}

	/**
	 * @param fechaOut the fechaOut to set
	 */
	public void setFechaOut(Timestamp fechaOut) {
		this.fechaOut = fechaOut;
	}

	/**
	 * @return the estado
	 */
	public int getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(int estado) {
		this.estado = estado;
	}

	/**
	 * @return the idHabitacion
	 */
	public long getIdHabitacion() {
		return idHabitacion;
	}

	/**
	 * @param idHabitacion the idHabitacion to set
	 */
	public void setIdHabitacion(long idHabitacion) {
		this.idHabitacion = idHabitacion;
	}

	/**
	 * @return the idPlan
	 */
	public long getIdPlan() {
		return idPlan;
	}

	/**
	 * @param idPlan the idPlan to set
	 */
	public void setIdPlan(long idPlan) {
		this.idPlan = idPlan;
	}
	/**
	 * @return Una cadena de caracteres con la información básica de la reserva
	 */
	@Override
	public String toString(){
		return "Reserva [idReserva=" + idCuenta + ", fechaIn=" + fechaIn + ", fechaOut=" + fechaOut + ", estado=" + estado+", idHabitacion=" + idHabitacion+", idPlan=" + idPlan+"]";
	}
}
