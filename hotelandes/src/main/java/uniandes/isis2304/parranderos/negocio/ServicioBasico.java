package uniandes.isis2304.parranderos.negocio;

/**
 * Clase para modelar el concepto servicio basico
 *
 * @author Dany Benavides y Isabela Sarimento
 */
public class ServicioBasico implements VOServicioBasico   {

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 *	El id del servicio UNICO
	 */
	private long id;
	/**
	 *costo
	 */
	private double costo;
	/**
	 * capacidad
	 */
	private int capacidad;
	
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Constructor por defecto
	 */
	public ServicioBasico() {
		this.id = 0;
		this.costo = 0;
		this.capacidad = 0;
	}
	
	/**
	 * Constructor con valores
	 * @param id
	 * @param costo
	 * @param capacidad
	 */
	public ServicioBasico(long id, double costo, int capacidad) {
		this.id = id;
		this.costo = costo;
		this.capacidad = capacidad;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOServicioBasico#getId()
	 */
	@Override
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOServicioBasico#getCosto()
	 */
	@Override
	public double getCosto() {
		return costo;
	}

	/**
	 * @param costo the costo to set
	 */
	public void setCosto(double costo) {
		this.costo = costo;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOServicioBasico#getCapacidad()
	 */
	@Override
	public int getCapacidad() {
		return capacidad;
	}

	/**
	 * @param capacidad the capacidad to set
	 */
	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}
}
