package uniandes.isis2304.parranderos.negocio;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorCompletionService;

import org.apache.log4j.Logger;

import com.google.gson.JsonObject;

import uniandes.isis2304.parranderos.persistencia.HotelAndesPersistencia;

/**
 * Clase principal del negocio Sarisface todos los requerimientos funcionales
 * del negocio
 *
 * @author Dany Benavides y Isabela Sarmiento
 */
public class HotelAndes {

	/*
	 * ****************************************************************
	 * Constantes
	 *****************************************************************/
	/**
	 * Logger para escribir la traza de la ejecución
	 */
	private static Logger log = Logger.getLogger(HotelAndes.class.getName());
	/*
	 * ****************************************************************
	 * Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia
	 */
	private HotelAndesPersistencia hp;

	/*
	 * **************************************************************** Métodos
	 *****************************************************************/
	/**
	 * El constructor por defecto
	 */
	public HotelAndes() {
		hp = HotelAndesPersistencia.getInstance();
	}

	/**
	 * El constructor que recibe los nombres de las tablas en tableConfig
	 * 
	 * @param tableConfig
	 *            - Objeto Json con los nombres de las tablas y de la unidad de
	 *            persistencia
	 */
	public HotelAndes(JsonObject tableConfig) {
		hp = HotelAndesPersistencia.getInstance(tableConfig);
	}

	/**
	 * Cierra la conexión con la base de datos (Unidad de persistencia)
	 */
	public void cerrarUnidadPersistencia() {
		hp.cerrarUnidadPersistencia();
	}

	/*
	 * **************************************************************** Métodos
	 * para manejar los USUARIOS
	 *****************************************************************/
	/**
	 * Adiciona de manera persistente un usuario Adiciona entradas al log de la
	 * aplicación
	 * 
	 * @param numeroDocumento
	 *            - El identificador ÚNICO del usuario
	 * @param tipoDocumento
	 *            - El tipo de documento
	 * @param correo
	 *            - El correo del usuario.
	 * @param rol
	 *            - El rol del usario CK('CLIENTE', 'ACOMPANIANTE', 'GERENTE',
	 *            'EMPLEADO', 'RECEPCIONISTA')
	 * @param nombre
	 *            - El nombre del usuario * @return El objeto TipoBebida
	 *            adicionado. null si ocurre alguna Excepción
	 */
	public Usuario adicionarUsuario(long numeroDocumento, String tipoDocumento, String correo, Long rol,
			String nombre) {
		log.info("Adicionando usuario: " + numeroDocumento + ", " + tipoDocumento);
		Usuario usuario = hp.adicionarUsuario(numeroDocumento, tipoDocumento, correo, rol, nombre);
		log.info("Adicionado usuario: " + numeroDocumento + ", " + tipoDocumento);
		return usuario;
	}

	/*
	 * **************************************************************** Métodos
	 * para manejar los PLAN USO
	 *****************************************************************/
	/**
	 * Adiciona de manera persistente un plan de uso Adiciona entradas al log de
	 * la aplicación
	 * 
	 * @param tipo
	 *            - El tipo del plan de uso CK(tipo IN
	 *            ('TodoIncluido','TiempoCompartido','LargaEstadia','Promocion'))
	 * @param costo
	 *            - El costo del PlanUso
	 * @param descuento
	 *            - El descuento que tiene le plan de uso. Si no tiene descuento
	 *            = 0;
	 * @param nombre
	 *            - El nombre de la promocion si es de tipo promocion
	 */
	public PlanUso adicionarPlanUso(String tipoPlanUso, double costoPlanUso) {
		log.info("Adicionando plan uso: " + tipoPlanUso + ", costo: " + costoPlanUso);
		PlanUso planUso = hp.adicionarPlanUso(tipoPlanUso, costoPlanUso);
		log.info("Adicionado plan uso: " + tipoPlanUso + ", costo" + costoPlanUso);
		return planUso;
	}

	public void eliminarPlanUso(Long idPlanUso) {
		log.info("Eliminando plan uso con id:" + idPlanUso);
		hp.eliminarPlanUso(idPlanUso);
		log.info("Eliminado plan uso con id:" + idPlanUso);
	}

	/*
	 * **************************************************************** Métodos
	 * para manejar los DESCUENTOS
	 *****************************************************************/
	/**
	 * Adiciona de manera persistente un plan de uso Adiciona entradas al log de
	 * la aplicación
	 * 
	 * @param tipo
	 *            - El tipo del plan de uso CK(tipo IN
	 *            ('TodoIncluido','TiempoCompartido','LargaEstadia','Promocion'))
	 * @param costo
	 *            - El costo del PlanUso
	 * @param descuento
	 *            - El descuento que tiene le plan de uso. Si no tiene descuento
	 *            = 0;
	 * @param nombre
	 *            - El nombre de la promocion si es de tipo promocion
	 */
	public Descuento adicionarDescuento(double porcentaje, Object idServicio, Object idProducto, long idPlan) {
		log.info("Adicionando descuento: " + porcentaje + ", plan: " + idPlan);
		Descuento descuento = hp.adicionarDescuento(porcentaje, idServicio, idProducto, idPlan);
		log.info("Adicionado descuento: " + porcentaje + ", plan: " + idPlan);
		return descuento;
	}

	/*
	 * **************************************************************** Métodos
	 * para manejar las RESERVAS
	 *****************************************************************/
	/**
	 * Adiciona de manera persistente una Reserva Adiciona entradas al log de la
	 * aplicación
	 * 
	 * @param fechaIn
	 *            - La fecha de ingreso
	 * @param fechaOut
	 *            - La fecha de salida
	 * @param estado
	 *            - El estado de la reserva FK en TipoEstados
	 * @param idHabitacion
	 *            - El id de la habitacion
	 * @param idPlan
	 *            - El id de la plan
	 */
	public Cuenta adicionarReserva(long idHabitacion, Timestamp fechaIn, Timestamp fechaOut, int estado, long idPlan) {
		System.out.println("adicionarReserva");
		log.info("Adicionando reserva de habitación: " + idHabitacion);
		Cuenta reserva = hp.adicionarCuenta(idHabitacion, fechaIn, fechaOut, estado, idPlan);
		log.info("Adicionado reserva: " + idHabitacion);
		return reserva;
	}

	/**
	 * Acutaliza de manera persistente una Reserva Cambia el estado de la
	 * reserva de 0 a 1
	 * 
	 * @param numeroDocumento
	 *            - El numero de docuemtno del cliente que llega
	 * @param tipoDocumento
	 *            - El tipo de Documento del cliente
	 */
	public void actualizarReserva(long numeroDocumento, String tipoDocumento) {
		// TODO Auto-generated method stub
		log.info("Cambiando reserva del cliente: " + numeroDocumento);
		hp.actualizarCuentaR9(numeroDocumento, tipoDocumento);
		log.info("Cambiando reserva del cliente: " + numeroDocumento);
	}

	/**
	 * Acutaliza de manera persistente una Reserva Cambia el estado de la
	 * reserva de 1 a 2
	 * 
	 * @param numeroDocumento
	 *            - El numero de docuemtno del cliente que llega
	 * @param tipoDocumento
	 *            - El tipo de Documento del cliente
	 */
	public void actualizarReservaR11(long numeroDocumento, String tipoDocumento) {
		// TODO Auto-generated method stub
		log.info("Cambiando reserva del cliente: " + numeroDocumento);
		hp.actualizarCuentaR11(numeroDocumento, tipoDocumento);
		log.info("Cambiando reserva del cliente: " + numeroDocumento);
	}
	/*
	 * **************************************************************** Métodos
	 * para manejar las RESERVAS
	 *****************************************************************/

	public boolean reservarR8(long idServicio) {
		// TODO Auto-generated method stub
		log.info("Cambiando reserva del servicio: " + idServicio);
		boolean reservo = hp.reservarR8(idServicio);
		log.info("Cambiando reserva del servicio: " + idServicio);
		return reservo;
	}

	public Long crearCuentaOrganizador(long numeroDocumento, String tipoDocumento, long idPlanUso, String fechaIn,
			String fechaOut) {
		// TODO Auto-generated method stub
		log.info("Creando la cuenta del organizador: " + numeroDocumento);
		Long id = hp.crearCuentaOrganizador(numeroDocumento, tipoDocumento, idPlanUso, fechaIn, fechaOut);
		log.info("Creada la cuenta del organizador: " + numeroDocumento);
		return id;
	}

	public List traerServiciosConvencionR13(Long idConvencion) {
		log.info("Revisando servicios de la convencion: " + idConvencion);
		List listaServiciosRelacionados = hp.traerServiciosR13(idConvencion);
		log.info("Revisado servicios de la convencion: " + idConvencion);
		return listaServiciosRelacionados;
	}

	public void cancelarServiciosConvencionR13(long input) {
		// TODO Auto-generated method stub
		log.info("Cancelando servicios de la convencion: " + input);
		hp.cancelarServiciosConvencionR13(input);
		log.info("Cancelado servicios de la convencion: " + input);
	}

	public void finalizarReserva(long numeroCuenta) throws Exception {
		// TODO Auto-generated method stub
		log.info("Finalizando de la reserva: " + numeroCuenta);
		hp.finConvecionR14(numeroCuenta);
		log.info("Finalizado reserva: " + numeroCuenta);
	}

	public String reservarHabitaciones(String tipo, int cantidad, String fechaIn, String fechaOut, Long idplanuso) {
		log.info("Se tinentan reservar " + cantidad + " habitacoines de tipo " + tipo);
		return hp.reservarHabitaciones(tipo, cantidad, fechaIn, fechaOut, idplanuso);
	}

	/*
	 * **************************************************************** Métodos
	 * para manejar los CONSUMO
	 *****************************************************************/
	/**
	 * Agrega de manera persistente un CONSUMO
	 * 
	 * @param idCliente
	 *            - id del cliente que consume
	 * @param idServicio
	 *            - id del servicio
	 * @param tipoDocumento
	 * @param numeroDocumento
	 */
	public String registrarConsumo(int estado, Long idProducto, Long idSalon, Long idCuenta, Long idServicio,
			long numeroDocumento, String tipoDocumento, String fechaStart, String fechaEnd) {
		// TODO Auto-generated method stub
		log.info("Registrar consumo: " + idServicio);
		String r = hp.consumoR10(estado, idProducto, idSalon, idCuenta, idServicio, numeroDocumento, tipoDocumento,
				fechaStart, fechaEnd);
		log.info("Registrar consumo exitosamente: " + idServicio);
		return r;
	}

	/*
	 * **************************************************************** Métodos
	 * para manejar las HABITACIONES
	 *****************************************************************/
	public ArrayList<String> darTiposHabitaciones() {
		log.info("Buscando tipos de habitaciones");
		return hp.darTiposHabitaciones();
	}

	public int darCapacidadServicio(Long idServicio) {
		log.info("Buscando capacidad de servicios");
		return hp.darCapacidadServicio(idServicio);
	}

	public List darTodasHabitacionesR15() {
		// TODO Auto-generated method stub
		log.info("Buscando todas las habitaciones");
		return hp.darTodasHabitacionesR15();
	}

	public List darTodosServiciosR15() {
		log.info("Buscando todos los servicios");
		return hp.darTodosServiciosR15();
	}

	public List darHabitacionesActivasR15() {
		log.info("Buscando las habitaciones que estan reservadas");
		return hp.darTodasHabitacionesEnUsoR15();
	}

	public List darServiciosActivosR15() {
		log.info("Buscando los servicios que estan reservados");
		return hp.darTodosServiciosEnUsoR15();
	}

	public void mantenimientoR15(List<BigDecimal> habitacionesParaMantenimiento,
			List<BigDecimal> serviciosParaMantenimiento, List<BigDecimal> habitacionesActivas,
			List<BigDecimal> serviciosActivos, String fechaStart, String fechaEnd, String causa) throws Exception {

		long idHorario = hp.nuevoHorario(fechaStart, fechaEnd);
		boolean noEsta = true;
		for (BigDecimal idHabActual : habitacionesParaMantenimiento) {
			noEsta = true;
			for (BigDecimal idHabActivas : habitacionesActivas) {
				if (idHabActual == idHabActivas)
					noEsta = false;
			}
			if (noEsta) {
				log.info("Creando nuevo mantenimiento para la habitacion: " + idHabActual);
				hp.nuevoMantenimientoR15(idHabActual, causa, idHorario);
				log.info("Creado nuevo mantenimiento para la habitacion: " + idHabActual);
				System.out.println("Creado nuevo mantenimiento para la habitacion: " + idHabActual);

			} else {
				// Reacomodar la habitación y cuenta de esta habitación
				log.info("Moviendo reserva para la habitacion: " + idHabActual);
				System.out.println("Moviendo reserva para la habitacion: " + idHabActual);
				long nuevaHabitacion = habitacionNoActiva(habitacionesParaMantenimiento, darHabitacionesActivasR15());

				hp.reubicarCuentaR15(idHabActual, nuevaHabitacion);
				log.info("Movida la reserva para la nueva habitacion: " + nuevaHabitacion);
				log.info("Creando nuevo mantenimiento para la habitacion: " + idHabActual);
				hp.nuevoMantenimientoR15(idHabActual, causa, idHorario);
				log.info("Creado nuevo mantenimiento para la habitacion: " + idHabActual);
			}
		}
		System.out.println("Acabo habitaciones");
		for (BigDecimal idServActual : serviciosParaMantenimiento) {
			noEsta = true;
			for (BigDecimal idServActivos : serviciosActivos) {
				if (idServActual == idServActivos)
					noEsta = false;
			}
			if (noEsta) {
				log.info("Creando nuevo mantenimiento para servicio: " + idServActual);
				System.out.println("Creando nuevo mantenimiento para servicio: " + idServActual);
				hp.nuevoMantenimientoServR15(idServActual, causa, idHorario);
				log.info("Creado nuevo mantenimiento para servicio: " + idServActual);
			} else {
				// Reacomodar la habitación y cuenta de esta habitación
				log.info("Moviendo reserva para servicio: " + idServActual);
				long nuevoServicio = servicioNoActivo(serviciosParaMantenimiento, darServiciosActivosR15());

				hp.reubicarConsumoR15(idServActual, nuevoServicio);
				log.info("Movida la reserva para servicio: " + nuevoServicio);
				log.info("Creando nuevo mantenimiento para servicio: " + idServActual);
				hp.nuevoMantenimientoServR15(idServActual, causa, idHorario);
				log.info("Creado nuevo mantenimiento para servicio: " + idServActual);
			}
		}

	}

	private long servicioNoActivo(List<BigDecimal> serviciosParaMantenimiento, List<BigDecimal> darServiciosActivosR15) throws Exception {
		// TODO Auto-generated method stub
		long habitacionVacia = 0;
		boolean centinela = true;
		for (BigDecimal habitacionActual : darServiciosActivosR15) {
			for (BigDecimal habMantenimiento : serviciosParaMantenimiento) {
				if (habitacionActual == habMantenimiento) {
					centinela = false;
				}
			}
			if (centinela = true) {
				habitacionVacia = habitacionActual.longValue();
				break;
			}
		}
		if (habitacionVacia == 0) {
			throw new Exception("No se puede reasignar un servicio al cliente, no hay libres");
		}
		return habitacionVacia;
	}

	private long habitacionNoActiva(List<BigDecimal> habitacionesParaMantenimiento,
			List<BigDecimal> darHabitacionesActivasR15) throws Exception {
		long habitacionVacia = 0;
		boolean centinela = true;
		for (BigDecimal habitacionActual : darHabitacionesActivasR15) {
			for (BigDecimal habMantenimiento : habitacionesParaMantenimiento) {
				if (habitacionActual == habMantenimiento) {
					centinela = false;
				}
			}
			if (centinela = true) {
				habitacionVacia = habitacionActual.longValue();
				break;
			}
		}
		if (habitacionVacia == 0) {
			throw new Exception("No se puede reasignar una habitación al cliente, no hay libres");
		}
		return habitacionVacia;
	}

	public List traerHabitacionesConvencionR13(long idConvencion) {
		// TODO Auto-generated method stub
		log.info("Revisando habitaciones de la convencion: " + idConvencion);
		List listaHabitacionesRelacionados = hp.traerHabitacionesR13(idConvencion);
		log.info("Revisado habitaciones de la convencion: " + idConvencion);
		return listaHabitacionesRelacionados;
	}

	public void cancelarHabitacionConvencionR13(long longValue) {
		log.info("Cancelando servicios de la convencion: " + longValue);
		hp.cancelarHabitacionesConvencionR13(longValue);
		log.info("Cancelado servicios de la convencion: " + longValue);
	}

	public List darTodosMantenimientosR16() {
		// TODO Auto-generated method stub
		log.info("Revisando manteniminetos" );
		List listaHabitacionesRelacionados = hp.traerMantenimientosR16();
		log.info("Revisado mantenimientos");
		return listaHabitacionesRelacionados;
	}

	public void finalizarMantenimiento(List<BigDecimal> habitacionesParaFinalizarMantenimiento) {
		// TODO Auto-generated method stub
		log.info("Finalizando mantenimientos");
		for (BigDecimal habitacion:habitacionesParaFinalizarMantenimiento){
			hp.cancelarMantenimientos(habitacion);
		}
		log.info("Fianlizado mantenimientos");
	}
	
	public List<String> consulta6(String unidadTiempo, String tipoPregunta, String detalle)
	{
		log.info(" Consulta 6");
		return hp.consulta6( unidadTiempo,  tipoPregunta,  detalle);
	}

	public List consulta7(String tipoConsulta)
	{
		log.info(" Consulta 7");
		return hp.consulta7( tipoConsulta);
	}

	public List consulta8()
	{
		log.info(" Consulta 8");
		return hp.consulta8( );
	}

	public long esAdministrador(long idAdmin) {
		return hp.esAdminsitrador(idAdmin);
	}

	public List consulta11() {
		// TODO Auto-generated method stub
		return null;
	}

	public List consulta9(Timestamp fechaIni, Timestamp fechaFin, List servicioParaBuscar, List tipoDeOrdenamientoo) {
		return hp.consulta9(fechaIni, fechaFin, servicioParaBuscar,tipoDeOrdenamientoo);
	}

	public List consulta10(Timestamp fechaIni, Timestamp fechaFin, List servicioParaBuscar, List tipoDeOrdenamientoo) {
		return hp.consulta10(fechaIni, fechaFin, servicioParaBuscar,tipoDeOrdenamientoo);
	}
	public List consulta11(int criterio) {
		return hp.consulta11(criterio);
	}
	
	public List consulta12(int criterio) {
		return hp.consulta12(criterio);
	}

	public List vecesConsulta9(Timestamp fechaIni, Timestamp fechaFin, List servicioParaBuscar) {
		return hp.consulta9(fechaIni, fechaFin, servicioParaBuscar);
	}

	public List darTodasUsuarioNumeros() {
		// TODO Auto-generated method stub
		return hp.darTodasUsuarioNumeros();
	}

	public List darTodasResponsableNumeros() {
		// TODO Auto-generated method stub
		return hp.darTodasResponsableNumeros();
	}
}

