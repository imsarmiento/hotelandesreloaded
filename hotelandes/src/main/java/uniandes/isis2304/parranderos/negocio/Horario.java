package uniandes.isis2304.parranderos.negocio;

import java.sql.Timestamp;

/**
 * Clase para modelar el concepto cadena de Horario
 *
 * @author Dany Benavides y Isabela Sarimento
 */
public class Horario implements VOHorario {
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El identificador ÚNICO de la horario
	 */
	private long id;
	/**
	 * La hora de inicio del horario
	 */
	private Timestamp fechaInicio;
	
	/**
	 * La hora de fin del horario
	 */
	private Timestamp horaInicio;
	
	/**
	 * Duracion
	 */
	private double duracion;

	
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Constructor por defecto
	 */
	public Horario() {
		this.id = 0;
		this.fechaInicio = null;
		this.horaInicio = null;
		this.duracion = 0;
	}
	
	/**
	 * Constructor con valores
	 * @param id
	 * @param fechaInicio
	 * @param horaInicio
	 * @param duracion
	 */
	public Horario(long id, Timestamp fechaInicio, Timestamp horaInicio, double duracion) {
		this.id = id;
		this.fechaInicio = fechaInicio;
		this.horaInicio = horaInicio;
		this.duracion = duracion;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOHorario#getId()
	 */
	@Override
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOHorario#getFechaInicio()
	 */
	@Override
	public Timestamp getFechaInicio() {
		return fechaInicio;
	}

	/**
	 * @param fechaInicio the fechaInicio to set
	 */
	public void setFechaInicio(Timestamp fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOHorario#getHoraInicio()
	 */
	@Override
	public Timestamp getHoraInicio() {
		return horaInicio;
	}

	/**
	 * @param horaInicio the horaInicio to set
	 */
	public void setHoraInicio(Timestamp horaInicio) {
		this.horaInicio = horaInicio;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOHorario#getDuracion()
	 */
	@Override
	public double getDuracion() {
		return duracion;
	}

	/**
	 * @param duracion the duracion to set
	 */
	public void setDuracion(double duracion) {
		this.duracion = duracion;
	}
	
	
	
}
