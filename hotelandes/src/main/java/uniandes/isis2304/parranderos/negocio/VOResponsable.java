package uniandes.isis2304.parranderos.negocio;
/**
 * Interfaz para los métodos get de Responsable.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz 
 * 
 * @author Dany Benavides y Isabela Sarimento
 */
public interface VOResponsable {
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * @return El numero de documento del cliente. Junto con el tipo de documento son UNICOS
	 */
	public long getNumeroDocumento();
	/**
	 * @return El tipo de documento . Junto con el num de docuemento son UNICOS
	 */
	public String getTipoDocumento();
	/**
	 * @return El identificador de la reserva a la que pertenece.
	 */
	public long getIdReserva();
	/**
	 * @return El identificador de la reserva a la que pertenece.
	 */
	public String getTipo();
	/**
	 * @return Una cadena de caracteres con la información básica del cliente
	 */
	@Override
	public String toString();}
