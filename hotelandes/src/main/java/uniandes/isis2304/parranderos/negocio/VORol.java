package uniandes.isis2304.parranderos.negocio;
/**
 * Interfaz para los métodos get de Rol.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz 
 * 
 * @author Dany Benavides y Isabela Sarimento
 */
public interface VORol 
{
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * @return El id del tipo de rol
	 */
	public long getId();

	/**
	 * @return El nombre del tipo de r0p
	 */
	public String getNombre();

	/**
	 * @return Una cadena de caracteres con la información del tipo de rol
	 */
	@Override
	public String toString(); 

	/**
	 * Define la igualdad dos roles
	 * @param tb - El tipo de rol a comparar
	 * @return true si tienen el mismo identificador y el mismo nombre
	 */
	@Override
	public boolean equals (Object tb); 
}
