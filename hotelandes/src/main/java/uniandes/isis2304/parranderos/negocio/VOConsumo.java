package uniandes.isis2304.parranderos.negocio;
/**
 * Interfaz para los métodos get de Consumo.

 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz 
 * 
 * @author Dany Benavides y Isabela Sarimento
 */
import java.sql.Timestamp;

public interface VOConsumo 
{
/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * @return El id del consumo
	 */
	public long getId();

	/**
	 * @return fecha del consumo
	 */
	public Long getIdHorario();

	/**
	 * @return El id de la Reserva. Debe estar en la tabla Reservas
	 */
	public long getIdCuenta();
	
	/**
	 * @return El id del servicio. Debe estar en la tabla servicios.
	 */
	public long getIdServicio();
	
	/**
	 * @return El numero de documento del empleado. Debe estar en la tabla usuario y se de tipo empleado
	 */
	public long getNumDocEmpleado();
	
	/**
	 * @return El tipo de documento del empleado. Debe estar en la tabla usuario y se de tipo empleado
	 */
	public String getTipoDocEmpleado();
	
	public int getEstado();
	
	public long getIdProducto();
	
	public long getIdSalon();

	
	/**
	 * @return Una cadena de caracteres con la informacion del consumo
	 */
	@Override
	public String toString(); 

}
