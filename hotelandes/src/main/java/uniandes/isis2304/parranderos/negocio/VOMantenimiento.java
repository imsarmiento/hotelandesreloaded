package uniandes.isis2304.parranderos.negocio;

public interface VOMantenimiento {

	/**
	 * @return the id
	 */
	long getId();

	/**
	 * @return the idServicio
	 */
	long getIdServicio();

	/**
	 * @return the idHabitacion
	 */
	long getIdHabitacion();

	/**
	 * @return the idHorario
	 */
	long getIdHorario();

	/**
	 * @return the causa
	 */
	String getCausa();

}