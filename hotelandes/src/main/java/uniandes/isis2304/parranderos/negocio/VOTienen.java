package uniandes.isis2304.parranderos.negocio;

public interface VOTienen {

	/**
	 * @return the idProducto
	 */
	long getIdProducto();

	/**
	 * @return the idTipoHabitacion
	 */
	long getIdTipoHabitacion();

}