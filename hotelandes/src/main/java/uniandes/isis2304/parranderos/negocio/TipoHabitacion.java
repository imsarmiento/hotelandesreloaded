package uniandes.isis2304.parranderos.negocio;
/**
 * Clase para modelar el concepto cadena de Tipo de Habitacion
 *
 * @author Dany Benavides y Isabela Sarmiento
 */
public class TipoHabitacion implements VOTipoHabitacion{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El identificador ÚNICO del tipo de habitacion
	 */
	private long id;
	/**
	 * El nombre del tipo de habitacion
	 */
	private String nombre;
	
	/**
	 * La capacidad de personas de la habitacion
	 */
	private int capacidad;
	/**
	 * El costo por quedarse en esa habitacion
	 */
	private int costo;
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Constructor por defecto
	 */
	public TipoHabitacion() 
	{
		this.id = 0;
		this.nombre = "";
		this.capacidad = 0;
		this.costo = 0;
	}
	/**
	 * Constructor con valores
	 * @param id - El identificador ÚNICO del tipo de habitacion
	 * @param nombre - El nombre del tipo de habitacion
	 * @param capacidad - La capacidad de personas de la habitacion
 	 * @param costo - El costo por quedarse en esa habitacion
	 */
	public TipoHabitacion(long id, String nombre, int capacidad, int costo) 
	{
		this.id = id;
		this.nombre = nombre;
		this.capacidad = capacidad;
		this.costo = costo;
	}
	/**
	 * @return El identificador ÚNICO del tipo de habitacion
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id - El nuevo identificador ÚNICO del tipo de habitacion
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return El nombre del tipo de habitacion
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre - El nombre del tipo de habitacion
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return La capacidad de personas de la habitacion
	 */
	public int getCapacidad() {
		return capacidad;
	}
	/**
	 * @param capacidad - La nueva capacidad de personas de la habitacion
	 */
	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}
	/**
	 * @return El costo por quedarse en esa habitacion
	 */
	public int getCosto() {
		return costo;
	}
	/**
	 * @param costo - El costo por quedarse en esa habitacion
	 */
	public void setCosto(int costo) {
		this.costo = costo;
	}
	/**
	 * @return Una cadena de caracteres con la información básica del tipo habitación
	 */
	@Override
	public String toString(){
		return "TipoHabitacion [id=" + id + ", nombre=" + nombre + ", capacidad=" + capacidad + ", costo=" + costo + "]";
	}
}
