package uniandes.isis2304.parranderos.negocio;
/**
 * Interfaz para los métodos get de PlanUso.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz 
 * 
 * @author Dany Benavides y Isabela Sarimento
 */
public interface VOPlanUso 
{
	/**
	 * @return El id del PlanUso
	 */
	public long getId();

	/**
	 * @return El tipo del plan de uso CK(IN('TodoIncluido','TiempoCompartido','LargaEstadia','Promocion'))
	 */
	public String getTipo();

	/**
	 * @return El costo del PlanUso
	 */
	public double getCosto();
	
	/**
	 * @return Una cadena con la información básica del plan uso
	 */
	@Override
	public String toString();

}
