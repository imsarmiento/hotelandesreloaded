package uniandes.isis2304.parranderos.negocio;
/**
 * Interfaz para los métodos get de Cadena.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz 
 * 
 * @author Dany Benavides y Isabela Sarmiento
 */
public interface VOCadena {
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * @return El id de la cadena
	 */
	public long getIdCadena();
	/**
	 * @return El nombre de la cadena
	 */
	public String getNombre();
	/**
	 * @return El indicador de si la cadena es internacional o no
	 */
	public int getInternational();
	/**
	 * @return Una cadena de caracteres con la información básica de la cadena
	 */
	@Override
	public String toString();
}
