package uniandes.isis2304.parranderos.negocio;

public interface VOVentaProducto {

	/**
	 * @return the id
	 */
	long getId();

	/**
	 * @return the tipo
	 */
	String getTipo();

	/**
	 * @return the estilo
	 */
	String getEstilo();

	/**
	 * @return the capacidad
	 */
	int getCapacidad();

}