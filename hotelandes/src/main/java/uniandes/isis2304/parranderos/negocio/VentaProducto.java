package uniandes.isis2304.parranderos.negocio;

/**
 * Clase para modelar el concepto Servicio de ventaprouducto del hotel
 *
 * @author Dany Benavides y Isabela Sarimento
 */
public class VentaProducto implements VOVentaProducto {
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	
	/**
	 *	El id del servicio UNICO
	 */
	private long id;
	
	/**
	 * @return El horario del servicio.
	 */
	private String tipo;
	
	/**
	 * @return El horario del servicio.
	 */
	private String estilo;
	
	/**
	 * @return El horario del servicio.
	 */
	private int capacidad;

	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Constructor por defecto
	 */
	public VentaProducto() {
		this.id = 0;
		this.tipo = "";
		this.estilo = "";
		this.capacidad = 0;
	}

	/**
	 * Constructor con valores
	 * @param id
	 * @param tipo
	 * @param estilo
	 * @param capacidad
	 */
	public VentaProducto(long id, String tipo, String estilo, int capacidad) {
		this.id = id;
		this.tipo = tipo;
		this.estilo = estilo;
		this.capacidad = capacidad;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOVentaProducto#getId()
	 */
	@Override
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOVentaProducto#getTipo()
	 */
	@Override
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOVentaProducto#getEstilo()
	 */
	@Override
	public String getEstilo() {
		return estilo;
	}

	/**
	 * @param estilo the estilo to set
	 */
	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOVentaProducto#getCapacidad()
	 */
	@Override
	public int getCapacidad() {
		return capacidad;
	}

	/**
	 * @param capacidad the capacidad to set
	 */
	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}

}
