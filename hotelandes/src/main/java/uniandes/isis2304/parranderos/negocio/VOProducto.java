package uniandes.isis2304.parranderos.negocio;

public interface VOProducto {

	/**
	 * @return the id
	 */
	long getId();

	/**
	 * @return the nombre
	 */
	String getNombre();

	/**
	 * @return the valor
	 */
	double getValor();

	/**
	 * @return the cantidad
	 */
	int getCantidad();

	/**
	 * @return the duracion
	 */
	double getDuracion();

	/**
	 * @return the idVentaProducto
	 */
	long getIdVentaProducto();

}