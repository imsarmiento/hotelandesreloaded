package uniandes.isis2304.parranderos.negocio;
/**
 * Clase para modelar el concepto cadena de la habitacion
 *
 * @author Dany Benavides y Isabela Sarimento
 */
public class Habitacion implements VOHabitacion{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El identificador ÚNICO de la habitacion
	 */
	private long idHabitacion;
	
	/**
	 * El id del hotel al que pertence
	 */
	private long idHotel;
	
	/**
	 * Indica el tipo de habitacion, se encuentra en otra tabla.
	 */
	private long tipo;
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Constructor por defecto
	 */
	public Habitacion() 
	{
		this.idHabitacion = 0;
		this.idHotel = 0;
		this.tipo = 0;
	}
	/**
	 * Constructor con valores
	 * @param idHabitacion - El id de la habitacon
	 * @param idHotel - El id del hotel al que pertence
	 * @param tipo - Indica el tipo de habitacion, se encuentra en otra tabla
	 */
	public Habitacion(long idHabitacion, long idHotel, long tipo) 
	{
		this.idHabitacion = idHabitacion;
		this.idHotel = idHotel;
		this.tipo = tipo;
	}
	/**
	 * @return El id de la habitacion
	 */
	public long getIdHabitacion() {
		return idHabitacion;
	}
	/**
	 * @param idHabitacion - El nuevo id de la habitacion
	 */
	public void setIdCadena(long idHabitacion) {
		this.idHabitacion = idHabitacion;
	}
	/**
	 * @return El id del hotel al que pertence
	 */
	public long getIdHotel() {
		return idHotel;
	}
	/**
	 * @param idHabitacion - El nuevo id del hotel al que pertence
	 */
	public void setIdHotel(long idHotel) {
		this.idHotel = idHotel;
	}
	/**
	 * @return El tipo de habitacion, se encuentra en otra tabla.
	 */
	public long getTipo() {
		return tipo;
	}
	/**
	 * @param idHabitacion - El tipo de habitacion, se encuentra en otra tabla.
	 */
	public void setTipo(long tipo) {
		this.tipo = tipo;
	}
	/**
	 * @return Una cadena de caracteres con la información básica de la habitación
	 */
	@Override
	public String toString(){
		return "Habitacion [idHabitacion=" + idHabitacion + ", idHotel=" + idHotel + ", tipo=" + tipo + "]";
	}
}
