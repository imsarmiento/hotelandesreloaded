package uniandes.isis2304.parranderos.negocio;

/**
 * Clase para modelar el concepto Tienen
 *
 * @author Dany Benavides y Isabela Sarimento
 */
public class Tienen implements VOTienen {
	
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El identificador ÚNICO del producto
	 */
	private long idProducto;
	/**
	 * El identificador ÚNICO del tipo de habitacion
	 */
	private long idTipoHabitacion;
	
	
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Constructor por defecto
	 */
	public Tienen() {
		this.idProducto = 0;
		this.idTipoHabitacion = 0;
	}

	/**
	 * Constructor con valores
	 * @param ididProducto
	 * @param idTipoHabitacion
	 */
	public Tienen(long idProducto, long idTipoHabitacion) {
		this.idProducto = idProducto;
		this.idTipoHabitacion = idTipoHabitacion;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOTienen#getIdProducto()
	 */
	@Override
	public long getIdProducto() {
		return idProducto;
	}

	/**
	 * @param idProducto the idProducto to set
	 */
	public void setIdProducto(long idProducto) {
		this.idProducto = idProducto;
	}

	/* (non-Javadoc)
	 * @see uniandes.isis2304.parranderos.negocio.VOTienen#getIdTipoHabitacion()
	 */
	@Override
	public long getIdTipoHabitacion() {
		return idTipoHabitacion;
	}

	/**
	 * @param idTipoHabitacion the idTipoHabitacion to set
	 */
	public void setIdTipoHabitacion(long idTipoHabitacion) {
		this.idTipoHabitacion = idTipoHabitacion;
	}

	
}
