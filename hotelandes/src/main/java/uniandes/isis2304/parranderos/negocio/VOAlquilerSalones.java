package uniandes.isis2304.parranderos.negocio;

public interface VOAlquilerSalones {

	/**
	 * @return the id
	 */
	long getId();

	/**
	 * @return the capacidad
	 */
	int getCapacidad();

	/**
	 * @return the costo
	 */
	double getCosto();

	/**
	 * @return the tipo
	 */
	String getTipo();

}