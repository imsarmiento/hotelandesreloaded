package uniandes.isis2304.parranderos.persistencia;

import java.math.BigDecimal;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

/**
 * Clase que encapsula los métodos que hacen acceso a la base de datos para el concepto USUARIO hotel andes
 * Nótese que es una clase que es sólo conocida en el paquete de persistencia
 * 
 * @author Dany Benavides y Isabela Sarmiento
 */
public class SQLUsuario {
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = HotelAndesPersistencia.SQL;

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private HotelAndesPersistencia hp;

	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Constructor
	 * @param pp - El Manejador de persistencia de la aplicación
	 */
	public SQLUsuario (HotelAndesPersistencia hp)
	{
		this.hp = hp;
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un USUARIO a la base de datos,
	 * @param pm - El manejador de persistencia
	 * @param numeroDocumento - El identificador ÚNICO del usuario
	 * @param tipoDocumento - El tipo de documento
	 * @param correo - El correo del usuario.
	 * @param rol - El rol del usario CK('CLIENTE', 'ACOMPANIANTE', 'GERENTE', 'EMPLEADO', 'RECEPCIONISTA')
	 * @param nombre - El nombre del usuario 	 * @return El objeto TipoBebida adicionado. null si ocurre alguna Excepción
	 * @return EL número de tuplas insertadas
	 */
	public long adicionarUsuario(PersistenceManager pm, long numeroDocumento, String tipoDocumento, String correo, Long rol,String nombre) 
	{
		// TODO Auto-generated method stub
		System.out.println("llego a sql usuario");
		Query q = pm.newQuery(SQL, "INSERT INTO " + hp.darTablaUsuario() + "(numeroDocumento, tipoDocumento, correo,rol,nombre) values "
				+ "("+ numeroDocumento+",'"+tipoDocumento+"','"+ correo+"',"+rol+",'"+ nombre +"')");
		// q.setParameters(idPlanUso, tipoPlanUso, costoPlanUso, descuentoPlanUso, nombrePromocion);
		return (long) q.executeUnique();           
	}

	public BigDecimal esAdministrador(PersistenceManager pm, long idAdmin) {
		// TODO Auto-generated method stub
		System.out.println("llego a sql usuario");
		Query q2 = pm.newQuery(SQL, "SELECT ROL\n" + 
				"FROM USUARIO\n" + 
				"WHERE NUMERODOCUMENTO = " + idAdmin);
		return (BigDecimal) q2.executeUnique();
	}
}
