package uniandes.isis2304.parranderos.persistencia;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class SQLPlanUso {
	
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = HotelAndesPersistencia.SQL;


	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private HotelAndesPersistencia pp;


	private PersistenceManager pm;
	

	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/

	/**
	 * Constructor
	 * @param pp - El Manejador de persistencia de la aplicación
	 */
	public SQLPlanUso(HotelAndesPersistencia pp) {
		this.pp = pp;
	}

	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un PlanUso a la base de datos de HotelAndes
	 * @param pm - El manejador de persistencia
	 * @param idPlanUso - El identificador del planUso
	 * @param tipoPlanUso - El tipo del Plan uso ()
	 * @param costoPlanUso - El costo del plan de uso
	 * @param descuentoPlanUso - El descuento del plan uso
	 * @param nombrePromocion - el nombre de la promocion
	 * @return El número de tuplas insertadas
	 */
	public long adicionarPlanUso(PersistenceManager pm, long idPlanUso, String tipoPlanUso, double costoPlanUso) {
		System.out.println("Llego a sql plan de uso");
		Query q = pm.newQuery(SQL, "INSERT INTO " + pp.darTablaPlanUso() + "(idPlan, costo, tipo) values ("+idPlanUso+", "+ costoPlanUso +", '" + tipoPlanUso+"')");
		// q.setParameters(idPlanUso, tipoPlanUso, costoPlanUso, descuentoPlanUso, nombrePromocion);
	    return (long) q.executeUnique();
	}
	
	public long eliminarPlanUso(long id)
	{
		System.out.println("Llego a sql plan de uso");
		Query q = pm.newQuery(SQL, "DELETE FROM "+pp.darTablaPlanUso() +" WHERE IDPLAN = "+id+")");
		// q.setParameters(idPlanUso, tipoPlanUso, costoPlanUso, descuentoPlanUso, nombrePromocion);
	    return (long) q.executeUnique();
	}

}
