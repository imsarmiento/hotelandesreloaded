package uniandes.isis2304.parranderos.persistencia;

import java.math.BigDecimal;
import java.sql.Timestamp;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class SQLConsumo {

	/* **********************
	 * 			Constantes
	 ***********************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = HotelAndesPersistencia.SQL;


	/* **********************
	 * 			Atributos
	 ***********************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private HotelAndesPersistencia pp;
	
	private int cuenta;

	/* **********************
	 * 			Métodos
	 ***********************/

	/**
	 * Constructor
	 * @param pp - El Manejador de persistencia de la aplicación
	 */
	public SQLConsumo(HotelAndesPersistencia pp) {
		this.pp = pp;
		cuenta = 0;
	}

	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un PlanUso a la base de datos de HotelAndes
	 * @param pm - El manejador de persistencia
	 * @param idPlanUso - El identificador del planUso
	 * @param tipoPlanUso - El tipo del Plan uso ()
	 * @param costoPlanUso - El costo del plan de uso
	 * @param descuentoPlanUso - El descuento del plan uso
	 * @param nombrePromocion - el nombre de la promocion
	 * @return El número de tuplas insertadas
	 */
	public long consumoR10(PersistenceManager pm, int estado, Long idProducto, Long idSalon, Long idCuenta, Long idServicio, long numeroDocumento, String tipoDocumento, Long idHorario) {
		// TODO Auto-generated method stub
		cuenta++;
		System.out.println("llego a sql Consumo");
		//no se tienen en cuenta las 12 horas extra de los salones 
		Query q = pm.newQuery(SQL, "INSERT INTO " + pp.darTablaConsumo() + "(id, estado,idProducto,idSalon,idCuenta,idServicio,numDocEmpleado,tipoDocEmpleado,idhorario) values ("+ cuenta + ","+estado+"," +idProducto+"," +idSalon+","+idCuenta+"," +idServicio+","+numeroDocumento+",'" + tipoDocumento + "',"+idHorario+")");
	    return (long) q.executeUnique();	
	}
	
	
	public boolean verificarDispoibilidadConsumo(PersistenceManager pm, Long idProducto, Long idSalon, Long idCuenta, Long idServicio, String fechaStart, String fechaEnd)
	{
		Query q = null;
		if(idProducto!=null)
		{
			System.out.println("entra1");
			q = pm.newQuery(SQL,
					"(SELECT * \n" + 
					"FROM CONSUMO \n" + 
					"INNER JOIN HORARIO ON CONSUMO.IDHORARIO = HORARIO.ID\n" + 
					"WHERE IDPRODUCTO =" + idProducto +"\n" + 
					"AND FECHAINICIO <= TO_DATE('"+fechaStart+"', 'YYYY-MM-DD HH24:MI:SS')\n" + 
					"AND FECHAFIN >= TO_DATE('"+fechaEnd+"', 'YYYY-MM-DD HH24:MI:SS')\n" + 
					 ")");
		}
		else if(idServicio!=null)
		{
			System.out.println("entra2");
			q = pm.newQuery(SQL,
					"(SELECT CONSUMO.ID \n" + 
					"FROM CONSUMO \n" + 
					"INNER JOIN HORARIO ON CONSUMO.IDHORARIO = HORARIO.ID\n" + 
					"WHERE IDSERVICIO =" + idServicio +"\n" + 
					"AND FECHAINICIO <= TO_DATE('"+fechaStart+"', 'YYYY-MM-DD HH24:MI:SS')\n" + 
					"AND FECHAFIN >= TO_DATE('"+fechaEnd+"', 'YYYY-MM-DD HH24:MI:SS')\n" + 
					 ")");
		}
		else
		{
			System.out.println("entra3");
			q = pm.newQuery(SQL,
					"(SELECT * \n" + 
					"FROM CONSUMO \n" + 
					"INNER JOIN HORARIO ON CONSUMO.IDHORARIO = HORARIO.ID\n" + 
					"WHERE IDSALON =" + idSalon +"\n" + 
					"AND FECHAINICIO <= TO_DATE('"+fechaStart+"', 'YYYY-MM-DD HH24:MI:SS')\n" + 
					"AND FECHAFIN >= TO_DATE('"+fechaEnd+"', 'YYYY-MM-DD HH24:MI:SS')\n" + 
					 ")");
		}
		Object lista = q.executeResultUnique(Long.class);
		System.out.println(lista);
		return true;
	}
	
	public void revisarConvencionR14(PersistenceManager pm, Long idCuenta) throws Exception {
		// TODO Auto-generated method stub
		Query q1 = pm.newQuery(SQL, "SELECT id FROM Consumo WHERE IDCUENTA = "+ idCuenta + " AND estado = 0");
		List lista = q1.executeList();
		if (lista.size() != 0){
			throw new Exception ("No se ha podido finalizar la cuenta porque tiene consumos sin pagar");
		}
	}

	public List traerServiciosR13(PersistenceManager pm, Long idCuenta) {
		// TODO Auto-generated method stub
		Query q1 = pm.newQuery(SQL, "SELECT id FROM Consumo WHERE IDCUENTA = "+ idCuenta + " AND estado = 0");
		return q1.executeList();
	}

	public void cancelarServiciosConvencionR13(PersistenceManager pm, long input) {
		// TODO Auto-generated method stub
		Query q1 = pm.newQuery(SQL, "DELETE FROM Consumo WHERE id = "+ input);
		q1.executeUnique();
	}

	public List darTodosServiciosEnUsoR15(PersistenceManager pm) {
		// TODO Auto-generated method stub
		Query q1 = pm.newQuery(SQL, "SELECT id FROM Consumo WHERE estado = 0");
		return q1.executeList();
	}

	public void reubicarConsumoR15(PersistenceManager pm, BigDecimal idServActual, long nuevoServicio) {
		// TODO Auto-generated method stub
		Query q2 = pm.newQuery(SQL, "UPDATE Consumo SET idServicio = "+nuevoServicio+" WHERE idServicio = " + idServActual);
		q2.executeUnique();
	}

	public List traerHabitacionesR13(PersistenceManager pm, long idConvencion) {
		// TODO Auto-generated method stub
		Query q1 = pm.newQuery(SQL, "SELECT idHabitacion FROM Cuenta WHERE IDCUENTA = "+ idConvencion + " AND estado = 0");
		return q1.executeList();
	}

	public void cancelarHabitacionesConvencionR13(PersistenceManager pm, long longValue) {
		// TODO Auto-generated method stub
		Query q1 = pm.newQuery(SQL, "DELETE FROM Cuenta WHERE idHabitacion = "+ longValue);
		q1.executeUnique();
	}
	
}