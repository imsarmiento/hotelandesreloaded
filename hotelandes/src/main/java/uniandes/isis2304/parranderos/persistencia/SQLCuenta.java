package uniandes.isis2304.parranderos.persistencia;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class SQLCuenta {

	/*
	 * ****************************************************************
	 * Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las
	 * sentencias de acceso a la base de datos Se renombra acá para facilitar la
	 * escritura de las sentencias
	 */
	private final static String SQL = HotelAndesPersistencia.SQL;

	/*
	 * ****************************************************************
	 * Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private HotelAndesPersistencia pp;

	/*
	 * **************************************************************** Métodos
	 *****************************************************************/

	/**
	 * Constructor
	 * 
	 * @param pp
	 *            - El Manejador de persistencia de la aplicación
	 */
	public SQLCuenta(HotelAndesPersistencia pp) {
		this.pp = pp;
	}

	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un PlanUso a la base de
	 * datos de HotelAndes
	 * 
	 * @param pm
	 *            - El manejador de persistencia
	 * @param idPlanUso
	 *            - El identificador del planUso
	 * @param tipoPlanUso
	 *            - El tipo del Plan uso ()
	 * @param costoPlanUso
	 *            - El costo del plan de uso
	 * @param descuentoPlanUso
	 *            - El descuento del plan uso
	 * @param nombrePromocion
	 *            - el nombre de la promocion
	 * @return El número de tuplas insertadas
	 */
	public long adicionarCuenta(PersistenceManager pm, long idCuenta, Timestamp fechaIn, Timestamp fechaOut, int estado,
			long idHabitacion, long idPlan) {
		// TODO Auto-generated method stub
		System.out.println("llego a sql Cuenta");
		Query q = pm.newQuery(SQL,
				"INSERT INTO " + pp.darTablaCuenta()
						+ "(idCuenta, fechaIn, fechaOut, estado, idHabitacion, idPlanUso ) values (" + idCuenta
						+ ", TO_DATE('" + fechaIn.toString().substring(0, 19)
						+ "' , 'yyyy-mm-dd hh24:mi:ss'), TO_DATE('" + fechaOut.toString().substring(0, 19)
						+ "', 'yyyy-mm-dd hh24:mi:ss'), " + estado + ", " + idHabitacion + ", " + idPlan + ")");
		// q.setParameters(idPlanUso, tipoPlanUso, costoPlanUso,
		// descuentoPlanUso, nombrePromocion);
		return (long) q.executeUnique();
	}


	public long adicionarCuenta(PersistenceManager pm, long idCuenta, String fechaIn, String fechaOut, int estado,
			Long idHabitacion, long idPlan) {
		// TODO Auto-generated method stub
		System.out.println("llego a sql Cuenta");
		Query q = pm.newQuery(SQL,
				"INSERT INTO " + pp.darTablaCuenta()
						+ "(idCuenta, fechaIn, fechaOut, estado, idHabitacion, idPlanUso ) values (" + idCuenta
						+ ", TO_DATE('" + fechaIn + "' , 'yyyy-mm-dd hh24:mi:ss'), TO_DATE('" + fechaOut
						+ "', 'yyyy-mm-dd hh24:mi:ss'), " + estado + ", " + idHabitacion + ", " + idPlan + ")");
		// q.setParameters(idPlanUso, tipoPlanUso, costoPlanUso,
		// descuentoPlanUso, nombrePromocion);
		return (long) q.executeUnique();
	}
	public void actualizarCuentaR9(PersistenceManager pm, long numeroDocumento, String tipoDocumento) {

		System.out.println("r9");
		Query q = pm.newQuery(SQL, "SELECT IDCuenta FROM CLIENTE WHERE NUMERODOCUMENTO = " + numeroDocumento
				+ " AND TIPODOCUMENTO = '" + tipoDocumento + "'");
		List lista = q.executeList();
		if (!lista.isEmpty()) {
			BigDecimal idCuenta = (BigDecimal) lista.get(0);
			Query q2 = pm.newQuery(SQL, "UPDATE Cuenta SET ESTADO = 1 WHERE IDCuenta = " + idCuenta);
			q2.executeUnique();
		}
	}

	public void actualizarCuentaR11(PersistenceManager pm, long numeroDocumento, String tipoDocumento) {
		System.out.println("r11");
		Query q = pm.newQuery(SQL, "SELECT IDCuenta FROM CLIENTE WHERE NUMERODOCUMENTO = " + numeroDocumento
				+ " AND TIPODOCUMENTO = '" + tipoDocumento + "'");
		List lista = q.executeList();
		if (!lista.isEmpty()) {
			BigDecimal idCuenta = (BigDecimal) lista.get(0);
			Query q2 = pm.newQuery(SQL, "UPDATE Cuenta SET ESTADO = 2 WHERE IDCuenta = " + idCuenta);
			q2.executeUnique();
		}
	}

	public void finConvencionR14(PersistenceManager pm, long idCuenta) throws Exception {
		Query q2 = pm.newQuery(SQL, "UPDATE Cuenta SET ESTADO = 2 WHERE IDCuenta = " + idCuenta);
		q2.executeUnique();
	}

	public List darTodasHabitacionesEnUsoR15(PersistenceManager pm) {
		// TODO Auto-generated method stub
		Query q = pm.newQuery(SQL, "SELECT idHabitacion FROM CUENTA WHERE idHabitacion IS NOT NULL AND estado = 0");
		List lista = q.executeList();
		return lista;
	}

	public String fechaIn(PersistenceManager pm, BigDecimal idHabActual) {
		// TODO Auto-generated method stub
		Query q = pm.newQuery(SQL, "SELECT FECHAIN FROM CUENTA WHERE idHabitacion = "+idHabActual);
		String fechaIn = (String) q.executeUnique();
		return fechaIn;
	}

	public String fechaOut(PersistenceManager pm, BigDecimal idHabActual) {
		// TODO Auto-generated method stub
		Query q = pm.newQuery(SQL, "SELECT FECHAOUT FROM CUENTA WHERE idHabitacion = "+idHabActual);
		String fechaOut = (String) q.executeUnique();
		return fechaOut;
	}
	public BigDecimal idPlan(PersistenceManager pm, BigDecimal idHabActual) {
		// TODO Auto-generated method stub
		Query q = pm.newQuery(SQL, "SELECT IDPLANUSO FROM CUENTA WHERE idHabitacion = "+idHabActual);
		BigDecimal fechaOut = (BigDecimal) q.executeUnique();
		return fechaOut;
	}

	public void reubicarCuentaR15(PersistenceManager pm,BigDecimal idHabActual, long nuevaHabitacion) {
		Query q2 = pm.newQuery(SQL, "UPDATE Cuenta SET idHabitacion = "+nuevaHabitacion+" WHERE idHabitacion = " + idHabActual);
		q2.executeUnique();
	}
}