package uniandes.isis2304.parranderos.persistencia;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.jdo.JDODataStoreException;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Transaction;

import org.apache.log4j.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import uniandes.isis2304.parranderos.negocio.PlanUso;
import uniandes.isis2304.parranderos.negocio.Consumo;
import uniandes.isis2304.parranderos.negocio.Cuenta;
import uniandes.isis2304.parranderos.negocio.Descuento;
import uniandes.isis2304.parranderos.negocio.Usuario;

public class HotelAndesPersistencia {
	/*
	 * ****************************************************************
	 * Constantes
	 *****************************************************************/
	/**
	 * Logger para escribir la traza de la ejecución
	 */
	private static Logger log = Logger.getLogger(HotelAndesPersistencia.class.getName());
	/**
	 * Cadena para indicar el tipo de sentencias que se va a utilizar en una
	 * consulta
	 */
	public final static String SQL = "javax.jdo.query.SQL";
	/*
	 * ****************************************************************
	 * Atributos
	 *****************************************************************/
	/**
	 * Atributo para el acceso a las sentencias SQL propias a
	 * HotelAndesPersistencia
	 */
	private SQLUtil sqlUtil;

	/**
	 * Atributo privado que es el único objeto de la clase - Patrón SINGLETON
	 */
	private static HotelAndesPersistencia instance;
	/**
	 * Fábrica de Manejadores de persistencia, para el manejo correcto de las
	 * transacciones
	 */
	private PersistenceManagerFactory pmf;
	/**
	 * Arreglo de cadenas con los nombres de las tablas de la base de datos, en
	 * su orden: Secuenciador, tipoBebida, bebida, bar, bebedor, gustan, sirven
	 * y visitan
	 */
	private List<String> tablas;
	/**
	 * Atributo para el acceso a la tabla USUARIO de la base de datos
	 */
	private SQLUsuario sqlUsuario;
	/**
	 * Atributo para el acceso a la tabla PLANUSO de la base de datos
	 */
	private SQLPlanUso sqlPlanUso;
	/**
	 * Atributo para el acceso a la tabla CUENTA de la base de datos
	 */
	private SQLCuenta sqlCuenta;

	/**
	 * Atributo para el acceso a la tabla SERVICIO de la base de datos
	 */
	private SQLServicio sqlServicio;

	/**
	 * Atributo para el acceso a la tabla CONSUMO de la base de datos
	 */
	private SQLConsumo sqlConsumo;

	/**
	 * Atributo para el acceso a la tabla DESCUENTo de la base de datos
	 */
	private SQLDescuento sqlDescuento;
	/**
	 * Atributo para el acceso a la tabla RESPONSABLE de la base de datos
	 */
	private SQLResponsable sqlResponsable;
	/**
	 * Atributo para el acceso a la tabla RESPONSABLE de la base de datos
	 */
	private SQLMantenimiento sqlMantenimiento;

	/**
	 * Atributo para el acceso a la tabla TIPOSHABITACION de la base de datos
	 */
	private SQLTiposHabitacion sqlTiposHabitacion;
	/**
	 * Atributo para el acceso a la tabla HORARIO de la base de datos
	 */
	private SQLHorario sqlHorario;
	/**
	 * Atributo para el acceso a la tabla HABITACION de la base de datos
	 */
	private SQLHabitacion sqlHabitacion;
	/**
	 * Atributo para el acceso a la tabla HABITACION de la base de datos
	 */
	private SQLConsulta sqlconsulta;

	/*
	 * **************************************************************** Métodos
	 * del MANEJADOR DE PERSISTENCIA
	 *****************************************************************/
	/**
	 * Constructor privado con valores por defecto - Patrón SINGLETON
	 */
	private HotelAndesPersistencia() {
		// TODO

		pmf = JDOHelper.getPersistenceManagerFactory("HotelAndes");
		crearClasesSQL();

		// Define los nombres por defecto de las tablas de la base de datos
		tablas = new LinkedList<String>();
		tablas.add("HotelAndes_sequence");
		tablas.add("CADENA");
		tablas.add("RESPONSABLE");
		tablas.add("CONSUMO");
		tablas.add("HABITACION");
		tablas.add("HOTEL");
		tablas.add("PLANUSO");
		tablas.add("DESCUENTO");
		tablas.add("CUENTA");
		tablas.add("ROL");
		tablas.add("SERVICIO");
		tablas.add("HORARIO");
		tablas.add("SERVICIOBASICO");
		tablas.add("CAMPOEXTRA");
		tablas.add("VENTAPRODUCTO");
		tablas.add("PRODUCTO");
		tablas.add("ALQUILERSALONES");
		tablas.add("TIPOHABITACION");
		tablas.add("TIENEN");
		tablas.add("USUARIO");
		tablas.add("MANTENIMIENTO");
		System.out.println("El tamaño de las tablas es:" + tablas.size());
	}

	/**
	 * Crea los atributos de clases de apoyo SQL
	 */
	private void crearClasesSQL() {
		sqlUsuario = new SQLUsuario(this);
		sqlCuenta = new SQLCuenta(this);
		sqlPlanUso = new SQLPlanUso(this);
		sqlServicio = new SQLServicio(this);
		sqlConsumo = new SQLConsumo(this);
		sqlDescuento = new SQLDescuento(this);
		sqlTiposHabitacion = new SQLTiposHabitacion(this);
		sqlResponsable = new SQLResponsable(this);
		sqlUtil = new SQLUtil(this);
		sqlMantenimiento = new SQLMantenimiento(this);
		sqlHabitacion = new SQLHabitacion(this);
		sqlHorario = new SQLHorario(this);
		sqlconsulta = new SQLConsulta(this);
	}

	/**
	 * Constructor privado, que recibe los nombres de las tablas en un objeto
	 * Json - Patrón SINGLETON
	 * 
	 * @param tableConfig
	 *            - Objeto Json que contiene los nombres de las tablas y de la
	 *            unidad de persistencia a manejar
	 */
	private HotelAndesPersistencia(JsonObject tableConfig) {
		// TODO
		crearClasesSQL();
		tablas = leerNombresTablas(tableConfig);

		String unidadPersistencia = tableConfig.get("unidadPersistencia").getAsString();
		log.trace("Accediendo unidad de persistencia: " + unidadPersistencia);
		pmf = JDOHelper.getPersistenceManagerFactory(unidadPersistencia);
	}

	/**
	 * Genera una lista con los nombres de las tablas de la base de datos
	 * 
	 * @param tableConfig
	 *            - El objeto Json con los nombres de las tablas
	 * @return La lista con los nombres del secuenciador y de las tablas
	 */
	private List<String> leerNombresTablas(JsonObject tableConfig) {
		JsonArray nombres = tableConfig.getAsJsonArray("tablas");

		List<String> resp = new LinkedList<String>();
		for (JsonElement nom : nombres) {
			resp.add(nom.getAsString());
		}

		return resp;
	}

	/**
	 * @return Retorna el único objeto PersistenciaParranderos existente -
	 *         Patrón SINGLETON
	 */
	public static HotelAndesPersistencia getInstance() {
		if (instance == null) {
			instance = new HotelAndesPersistencia();
		}
		return instance;
	}

	/**
	 * Constructor que toma los nombres de las tablas de la base de datos del
	 * objeto tableConfig
	 * 
	 * @param tableConfig
	 *            - El objeto JSON con los nombres de las tablas
	 * @return Retorna el único objeto PersistenciaParranderos existente -
	 *         Patrón SINGLETON
	 */
	public static HotelAndesPersistencia getInstance(JsonObject tableConfig) {
		if (instance == null) {
			instance = new HotelAndesPersistencia(tableConfig);
		}
		return instance;
	}

	/**
	 * Cierra la conexión con la base de datos
	 */
	public void cerrarUnidadPersistencia() {
		pmf.close();
		instance = null;
	}

	public String darSeqHotelAndes() {
		// TODO Auto-generated method stub
		return tablas.get(0);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Usuario
	 */
	public String darTablaCadena() {
		return tablas.get(1);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de PlanUso
	 */
	public String darTablaResponsable() {
		return tablas.get(2);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Cuenta
	 */
	public String darTablaConsumo() {
		return tablas.get(3);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Servicio
	 */
	public String darTablaHabitacion() {
		return tablas.get(4);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Consumo
	 */
	public String darTablaHotel() {
		return tablas.get(5);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Usuario
	 */
	public String darTablaPlanUso() {
		return tablas.get(6);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de PlanUso
	 */
	public String darTablaDescuento() {
		return tablas.get(7);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Cuenta
	 */
	public String darTablaCuenta() {
		return tablas.get(8);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Servicio
	 */
	public String darTablaRol() {
		return tablas.get(9);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Consumo
	 */
	public String darTablaServicio() {
		return tablas.get(10);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Usuario
	 */
	public String darTablaHorario() {
		return tablas.get(11);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de PlanUso
	 */
	public String darTablaServicioBasico() {
		return tablas.get(12);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Cuenta
	 */
	public String darTablaCampoExtra() {
		return tablas.get(13);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Servicio
	 */
	public String darTablaVentaProducto() {
		return tablas.get(14);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Consumo
	 */
	public String darTablaProducto() {
		return tablas.get(15);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Usuario
	 */
	public String darTablaAlquilerSalones() {
		return tablas.get(16);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de PlanUso
	 */
	public String darTablaTipoHabitacion() {
		return tablas.get(17);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Cuenta
	 */
	public String darTablaTienen() {
		return tablas.get(18);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Servicio
	 */
	public String darTablaUsuario() {
		return tablas.get(19);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Consumo
	 */
	public String darTablaMantenimiento() {
		return tablas.get(20);
	}

	/**
	 * Extrae el mensaje de la exception JDODataStoreException embebido en la
	 * Exception e, que da el detalle específico del problema encontrado
	 * 
	 * @param e
	 *            - La excepción que ocurrio
	 * @return El mensaje de la excepción JDO
	 */
	private String darDetalleException(Exception e) {
		String resp = "";
		if (e.getClass().getName().equals("javax.jdo.JDODataStoreException")) {
			JDODataStoreException je = (javax.jdo.JDODataStoreException) e;
			return je.getNestedExceptions()[0].getMessage();
		}
		return resp;
	}

	/**
	 * Transacción para el generador de secuencia de HotelAndes Adiciona
	 * entradas al log de la aplicación
	 * 
	 * @return El siguiente número del secuenciador de HotelAndes
	 */
	private long nextval() {
		long resp = sqlUtil.nextval(pmf.getPersistenceManager());
		log.trace("Generando secuencia: " + resp);
		return resp;
	}

	/*
	 * **************************************************************** Métodos
	 * para manejar los USUARIOS
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla
	 * Usuario Adiciona entradas al log de la aplicación
	 * 
	 * @param numeroDocumento
	 *            - El identificador ÚNICO del usuario
	 * @param tipoDocumento
	 *            - El tipo de documento
	 * @param correo
	 *            - El correo del usuario.
	 * @param rol
	 *            - El rol del usario CK('CLIENTE', 'ACOMPANIANTE', 'GERENTE',
	 *            'EMPLEADO', 'RECEPCIONISTA')
	 * @param nombre
	 *            - El nombre del usuario * @return El objeto TipoBebida
	 *            adicionado. null si ocurre alguna Excepción
	 * @return El objeto Usuario adicionado. null si ocurre alguna Excepción
	 */
	public Usuario adicionarUsuario(long numeroDocumento, String tipoDocumento, String correo, Long rol,
			String nombre) {
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try {
			tx.begin();
			long tuplasInsertadas = sqlUsuario.adicionarUsuario(pm, numeroDocumento, tipoDocumento, correo, rol,
					nombre);
			tx.commit();

			log.trace("Inserción del usuario: " + numeroDocumento + ": " + tuplasInsertadas + " tuplas insertadas");

			return new Usuario(numeroDocumento, tipoDocumento, correo, rol, nombre);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
			pm.close();
		}
	}

	/*
	 * **************************************************************** Métodos
	 * para manejar los PLAN USO
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla PLAN
	 * USO Adiciona entradas al log de la aplicación
	 * 
	 * @param tipo
	 *            - El tipo del plan de uso CK(tipo IN
	 *            ('TodoIncluido','TiempoCompartido','LargaEstadia','Promocion'))
	 * @param costo
	 *            - El costo del PlanUso
	 * @param descuento
	 *            - El descuento que tiene le plan de uso. Si no tiene descuento
	 *            = 0;
	 * @param nombre
	 *            - El nombre de la promocion si es de tipo promocion
	 * @return El objeto Bebida adicionado. null si ocurre alguna Excepción
	 */
	public PlanUso adicionarPlanUso(String tipoPlanUso, double costoPlanUso) {
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try {
			tx.begin();
			long idPlanUso = nextval();
			long tuplasInsertadas = sqlPlanUso.adicionarPlanUso(pm, idPlanUso, tipoPlanUso, costoPlanUso);
			tx.commit();

			log.trace("Inserción Plan Uso: " + idPlanUso + ": " + tuplasInsertadas + " tuplas insertadas");
			return new PlanUso(idPlanUso, tipoPlanUso, costoPlanUso);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
			pm.close();
		}
	}

	public void eliminarPlanUso(Long id) {
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try {
			tx.begin();
			long idPlanUso = nextval();
			long tuplasEliminadas = sqlPlanUso.eliminarPlanUso(id);
			tx.commit();

			log.trace("Eliminacion Plan Uso: " + idPlanUso + ": " + tuplasEliminadas + " tuplas eliminadas");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
			pm.close();
		}
	}

	public Descuento adicionarDescuento(double porcentaje, Object idServicio, Object idProducto, long idPlan) {
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try {
			tx.begin();
			long idDescuento = nextval();
			long tuplasInsertadas = sqlDescuento.adicionarDescuento(pm, idDescuento, porcentaje, idServicio, idProducto,
					idPlan);
			tx.commit();

			log.trace("Inserción Plan Uso: " + idDescuento + ": " + tuplasInsertadas + " tuplas insertadas");
			long idServicioL;
			long idProductoL;
			if (idServicio == null) {
				idServicioL = 0;
				idProductoL = (Long) idProducto;
			} else {
				idProductoL = 0;
				idServicioL = (Long) idServicio;
			}
			return new Descuento(idDescuento, porcentaje, idServicioL, idProductoL, idPlan);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
			pm.close();
		}
	}

	/*
	 * **************************************************************** Métodos
	 * para manejar las CuentaS
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla Cuenta
	 * Adiciona entradas al log de la aplicación
	 * 
	 * @param fechaIn
	 *            - La fecha de ingreso
	 * @param fechaOut
	 *            - La fecha de salida
	 * @param estado
	 *            - El estado de la Cuenta FK en TipoEstados
	 * @param idHabitacion
	 *            - El id de la habitacion
	 * @param idPlan
	 *            - El id de la plan
	 * @return El objeto Bebida adicionado. null si ocurre alguna Excepción
	 */
	public Cuenta adicionarCuenta(long idHabitacion, Timestamp fechaIn, Timestamp fechaOut, int estado, long idPlan) {
		System.out.println("adicionarCuentaPersistencia");

		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try {
			tx.begin();
			long idCuenta = nextval();
			long tuplasInsertadas = sqlCuenta.adicionarCuenta(pm, idCuenta, fechaIn, fechaOut, estado, idHabitacion,
					idPlan);
			tx.commit();

			log.trace("Inserción Cuenta: " + idCuenta + ": " + tuplasInsertadas + " tuplas insertadas");
			return new Cuenta(idCuenta, fechaIn, fechaOut, estado, idHabitacion, idPlan);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que actualiza, de manera transaccional, una tupla en la tabla
	 * Cuenta Cambia el atributo estado
	 * 
	 * @param numeroDocumento
	 *            - El id de la habitacion
	 * @param tipoDocumento
	 *            - El id de la plan
	 * @return El objeto Bebida adicionado. null si ocurre alguna Excepción
	 */
	public void actualizarCuentaR9(long numeroDocumento, String tipoDocumento) {

		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try {
			tx.begin();
			sqlCuenta.actualizarCuentaR9(pm, numeroDocumento, tipoDocumento);
			tx.commit();

			log.trace("Actualizacion Cuenta de : " + numeroDocumento);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que actualiza, de manera transaccional, una tupla en la tabla
	 * Cuenta Cambia el atributo estado
	 * 
	 * @param numeroDocumento
	 *            - El id de la habitacion
	 * @param tipoDocumento
	 *            - El id de la plan
	 * @return El objeto Bebida adicionado. null si ocurre alguna Excepción
	 */
	public void actualizarCuentaR11(long numeroDocumento, String tipoDocumento) {

		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try {
			tx.begin();
			sqlCuenta.actualizarCuentaR11(pm, numeroDocumento, tipoDocumento);
			tx.commit();

			log.trace("Actualizacion Cuenta de : " + numeroDocumento);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
			pm.close();
		}
	}

	/*
	 * **************************************************************** Métodos
	 * para manejar las SERVICIO
	 *****************************************************************/
	/**
	 * Acutaliza de manera persistente un Servicio Cambia su estado de no
	 * Cuentado a Cuentado o si ya esta Cuentado aborta
	 * 
	 * @param idServicio
	 *            - id del servicio
	 */
	public boolean reservarR8(long idServicio) {
		// TODO Auto-generated method stub
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try {
			tx.begin();
			boolean reservo = sqlServicio.reservarR8(pm, idServicio);
			tx.commit();

			log.trace("Actualizacion Cuenta de : " + idServicio);
			return reservo;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
			pm.close();
		}
		return false;
	}

	public int darCapacidadServicio(Long idServicio) {
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try {
			tx.begin();
			int capacidad = sqlServicio.darCapacidad(pm, idServicio).intValue();
			tx.commit();

			log.trace("Actualizacion Cuenta de : " + idServicio);
			return capacidad;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			throw e;
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
			pm.close();
		}

	}

	/*
	 * **************************************************************** Métodos
	 * para manejar los CONSUMO
	 *****************************************************************/
	public String consumoR10(int estado, Long idProducto, Long idSalon, Long idCuenta, Long idServicio,
			long numeroDocumento, String tipoDocumento, String fechaStart, String fechaEnd) {
		// TODO Auto-generated method stub
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		String respuesta = "";
		try {
			tx.begin();
			if (sqlConsumo.verificarDispoibilidadConsumo(pm, idProducto, idSalon, idCuenta, idServicio, fechaStart,
					fechaEnd)) {
				System.out.println("entra");
				Long idHorario = sqlHorario.crearHorario(pm, fechaStart, fechaEnd);
				sqlConsumo.consumoR10(pm, estado, idProducto, idSalon, idCuenta, idServicio, numeroDocumento,
						tipoDocumento, idHorario);
				tx.commit();

				log.trace("Actualizacion consumo por parte de la cuenta : " + idCuenta);
				respuesta = "Se registró el consumo:"
						+ (new Consumo(12, idHorario, idCuenta, idServicio, numeroDocumento, tipoDocumento)).toString();
			} else
				respuesta = "No se puede reservar el consumo pues no hay disponibildiad en las fechas mencionadas";
			return respuesta;

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
			pm.close();
		}
		return respuesta;
	}

	/*
	 * **************************************************************** Métodos
	 * para manejar los HABITACIONES
	 *****************************************************************/

	public ArrayList<String> darTiposHabitaciones() {
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try {
			tx.begin();
			ArrayList<String> resp = sqlTiposHabitacion.darTipos(pm);
			tx.commit();

			log.trace("Tipos de habitaciones obtenidos");
			return resp;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
			pm.close();
		}
	}

	/*
	 * **************************************************************** Métodos
	 * para manejar las CONVENCIONES
	 *****************************************************************/

	public Long crearCuentaOrganizador(long numeroDocumento, String tipoDocumento, long idPlanUso, String fechaIn,
			String fechaOut) {
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		boolean resp = false;
		try {
			tx.begin();
			long idCuenta = nextval();
			Long num = sqlCuenta.adicionarCuenta(pm, idCuenta, fechaIn, fechaOut, 0, null, idPlanUso);
			Long num2 = sqlResponsable.crearResponsable(pm, numeroDocumento, tipoDocumento, "CONVENCION", idCuenta);
			tx.commit();

			log.trace("Tipos de habitaciones obtenidos");

			return idCuenta;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			throw e;
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
			pm.close();
		}
	}

	public void finConvecionR14(Long idCuenta) throws Exception {
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		sqlConsumo.revisarConvencionR14(pm, idCuenta); // Manda excepcion si hay
														// consumos sin pagar
		tx.begin();
		sqlCuenta.finConvencionR14(pm, idCuenta);
		tx.commit();

		if (tx.isActive()) {
			tx.rollback();
		}
		pm.close();

	}

	public List traerServiciosR13(Long idCuenta) {
		PersistenceManager pm = pmf.getPersistenceManager();
		List listaServicios = sqlConsumo.traerServiciosR13(pm, idCuenta);
		pm.close();
		return listaServicios;
	}

	public void cancelarServiciosConvencionR13(long input) {
		// TODO Auto-generated method stub
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		tx.begin();
		sqlConsumo.cancelarServiciosConvencionR13(pm, input);
		tx.commit();
		pm.close();

	}

	public List darTodasHabitacionesEnUsoR15() {
		// TODO Auto-generated method stub
		PersistenceManager pm = pmf.getPersistenceManager();
		List listaHabitaciones = sqlCuenta.darTodasHabitacionesEnUsoR15(pm);
		pm.close();
		return listaHabitaciones;
	}

	public List darTodasHabitacionesR15() {
		// TODO Auto-generated method stub
		PersistenceManager pm = pmf.getPersistenceManager();
		List listaHabitaciones = sqlTiposHabitacion.darTodasHabitacionesR15(pm);
		pm.close();
		return listaHabitaciones;
	}

	public List darTodosServiciosR15() {
		// TODO Auto-generated method stub
		PersistenceManager pm = pmf.getPersistenceManager();
		List listaHabitaciones = sqlServicio.darTodosServiciosR15(pm);
		pm.close();

		return listaHabitaciones;
	}

	public List darTodosServiciosEnUsoR15() {
		PersistenceManager pm = pmf.getPersistenceManager();
		List listaHabitaciones = sqlConsumo.darTodosServiciosEnUsoR15(pm);
		pm.close();

		return listaHabitaciones;
	}

	public String reservarHabitaciones(String tipo, int cantidad, String fechaIn, String fechaOut, long idPlan) {
		String respuesta = "";
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		Long idCuenta = null;
		try {
			tx.begin();
			List<BigDecimal> lista = sqlHabitacion.reservarHabitaciones(pm, tipo, cantidad, fechaIn, fechaOut);
			if (lista != null) {
				for (int i = 0; i < cantidad; i++) {
					idCuenta = nextval();
					sqlCuenta.adicionarCuenta(pm, idCuenta, fechaIn, fechaOut, 0, lista.get(i).longValue(), idPlan);
					respuesta += "\n Se adiciono una cuenta con id " + idCuenta + " para una habitacion tipo" + tipo;
				}
			} else {
				return "No hay disponibilidad para reservar " + cantidad + " habtiaciones de tipo " + tipo;
			}
			tx.commit();

			log.trace("Tipos de habitaciones obtenidos");
			return respuesta;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return respuesta;
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
			pm.close();
		}
	}

	public void nuevoMantenimientoR15(BigDecimal idHabActual, String causa, long idHorario) {
		// TODO Auto-generated method stub
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		tx.begin();
		System.out.println("Despues de crear el horario");
		sqlMantenimiento.nuevoMantenimientoR15(pm, null, idHabActual, idHorario, causa, 0);
		tx.commit();
		if (tx.isActive()) {
			tx.rollback();
		}
		pm.close();
	}

	public String fechaInReservaR15(BigDecimal idHabActual) {
		// TODO Auto-generated method stub
		PersistenceManager pm = pmf.getPersistenceManager();
		String fechaIn = sqlCuenta.fechaIn(pm, idHabActual);
		pm.close();
		return fechaIn;
	}

	public String fechaOutReservaR15(BigDecimal idHabActual) {
		// TODO Auto-generated method stub
		PersistenceManager pm = pmf.getPersistenceManager();
		String fechaOut = sqlCuenta.fechaOut(pm, idHabActual);
		pm.close();
		return fechaOut;
	}

	public BigDecimal idPlanReservaR15(BigDecimal idHabActual) {
		// TODO Auto-generated method stub
		PersistenceManager pm = pmf.getPersistenceManager();
		BigDecimal idPlan = sqlCuenta.idPlan(pm, idHabActual);
		pm.close();
		return idPlan;
	}

	public void reubicarCuentaR15(BigDecimal idHabActual, long nuevaHabitacion) {
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		tx.begin();
		sqlCuenta.reubicarCuentaR15(pm, idHabActual, nuevaHabitacion);
		tx.commit();
		if (tx.isActive()) {
			tx.rollback();
		}
		pm.close();
	}

	public void nuevoMantenimientoServR15(BigDecimal idServActual, String causa, long idHorario) {
		// TODO Auto-generated method stub
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		tx.begin();
		sqlMantenimiento.nuevoMantenimientoR15(pm, idServActual, null, idHorario, causa, 0);
		tx.commit();
		if (tx.isActive()) {
			tx.rollback();
		}
		pm.close();
	}

	public long nuevoHorario(String fechaStart, String fechaEnd) {
		PersistenceManager pm = pmf.getPersistenceManager();
		return sqlHorario.crearHorario(pm, fechaStart, fechaEnd);
	}

	public void reubicarConsumoR15(BigDecimal idServActual, long nuevoServicio) {
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		tx.begin();
		sqlConsumo.reubicarConsumoR15(pm, idServActual, nuevoServicio);
		tx.commit();
		if (tx.isActive()) {
			tx.rollback();
		}
		pm.close();

	}

	public List traerHabitacionesR13(long idConvencion) {
		PersistenceManager pm = pmf.getPersistenceManager();
		List listaServicios = sqlConsumo.traerHabitacionesR13(pm, idConvencion);
		pm.close();
		return listaServicios;
	}

	public void cancelarHabitacionesConvencionR13(long longValue) {
		// TODO Auto-generated method stub
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		tx.begin();
		sqlConsumo.cancelarHabitacionesConvencionR13(pm, longValue);
		tx.commit();
		pm.close();
	}

	public List traerMantenimientosR16() {
		// TODO Auto-generated method stub
		PersistenceManager pm = pmf.getPersistenceManager();
		List listaMantenimientos = sqlMantenimiento.traerMantenimientosR16(pm);
		pm.close();
		return listaMantenimientos;
	}

	public void cancelarMantenimientos(BigDecimal habitacionesParaMantenimiento) {
		// TODO Auto-generated method stub
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		tx.begin();
		sqlMantenimiento.cancelarHabitacionesConvencionR13(pm, habitacionesParaMantenimiento);
		tx.commit();
		pm.close();
	}

	public List<String> consulta6(String unidadTiempo, String tipoPregunta, String detalle) {
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		boolean resp = false;
		try {
			tx.begin();
			List r = sqlconsulta.consulta6(pm, unidadTiempo, tipoPregunta, detalle);
			tx.commit();

			log.trace("Tipos de habitaciones obtenidos");

			return r;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			throw e;
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
			pm.close();
		}
	}

	public List consulta7(String tipoConsulta) {
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		boolean resp = false;
		try {
			tx.begin();
			List l = sqlconsulta.consulta7(pm, tipoConsulta);
			tx.commit();

			log.trace("Tipos de habitaciones obtenidos");

			return l;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			throw e;
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
			pm.close();
		}
	}

	public List consulta8() {
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		boolean resp = false;
		try {
			tx.begin();
			List l = sqlconsulta.consulta8(pm);
			tx.commit();

			log.trace("Tipos de habitaciones obtenidos");

			return l;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			throw e;
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
			pm.close();
		}
	}

	public long esAdminsitrador(long idAdmin) {
		// TODO Auto-generated method stub
		PersistenceManager pm = pmf.getPersistenceManager();
		return sqlUsuario.esAdministrador(pm, idAdmin).longValue();
	}

	public List consulta9(Timestamp fechaIni, Timestamp fechaFin, List servicioParaBuscar, List tipoDeOrdenamientoo) {
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		boolean resp = false;
		try {
			tx.begin();
			List l = sqlconsulta.consulta9(pm, fechaIni, fechaFin, servicioParaBuscar, tipoDeOrdenamientoo);
			tx.commit();

			log.trace("Tipos de habitaciones obtenidos");

			return l;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			throw e;
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
			pm.close();
		}
	}

	public List consulta10(Timestamp fechaIni, Timestamp fechaFin, List servicioParaBuscar, List tipoDeOrdenamientoo) {
		// TODO Auto-generated method stub
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		boolean resp = false;
		try {
			tx.begin();
			List l = sqlconsulta.consulta10(pm, fechaIni, fechaFin, servicioParaBuscar, tipoDeOrdenamientoo);
			tx.commit();

			log.trace("Tipos de habitaciones obtenidos");

			return l;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			throw e;
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
			pm.close();
		}
	}
	public List consulta11(int criterio) {
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			List l = sqlconsulta.consulta11(pm,criterio);
			tx.commit();

			log.trace ("Tipos de habitaciones obtenidos");

			return l;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			throw e;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	
	public List consulta12(int criterio) {
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			List l = sqlconsulta.consulta12(pm,criterio);
			tx.commit();

			log.trace ("Tipos de habitaciones obtenidos");

			return l;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			throw e;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	public List consulta9(Timestamp fechaIni, Timestamp fechaFin, List servicioParaBuscar) {
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		boolean resp = false;
		try {
			tx.begin();
			List l = sqlconsulta.consulta9(pm, fechaIni, fechaFin, servicioParaBuscar);
			tx.commit();

			log.trace("Tipos de habitaciones obtenidos");

			return l;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			throw e;
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
			pm.close();
		}
	}

	public List darTodasUsuarioNumeros() {
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		boolean resp = false;
		try {
			tx.begin();
			List l = sqlconsulta.tablaUsuarioNumeros(pm);
			tx.commit();

			log.trace("Tipos de habitaciones obtenidos");

			return l;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			throw e;
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
			pm.close();
		}
	}

	public List darTodasResponsableNumeros() {
		// TODO Auto-generated method stub
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		boolean resp = false;
		try {
			tx.begin();
			List l = sqlconsulta.tablaResponsableNumeros(pm);
			tx.commit();

			log.trace("Tipos de habitaciones obtenidos");

			return l;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			throw e;
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
			pm.close();
		}
	}
}
