package uniandes.isis2304.parranderos.persistencia;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class SQLHorario {
	
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = HotelAndesPersistencia.SQL;


	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private HotelAndesPersistencia pp;


	private PersistenceManager pm;
	
	private long cuenta;

	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/

	/**
	 * Constructor
	 * @param pp - El Manejador de persistencia de la aplicación
	 */
	public SQLHorario(HotelAndesPersistencia pp) {
		this.pp = pp;
		cuenta=(long) (Math.random()*10000);
	}

	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un PlanUso a la base de datos de HotelAndes
	 * @param pm - El manejador de persistencia
	 * @param idPlanUso - El identificador del planUso
	 * @param tipoPlanUso - El tipo del Plan uso ()
	 * @param costoPlanUso - El costo del plan de uso
	 * @param descuentoPlanUso - El descuento del plan uso
	 * @param nombrePromocion - el nombre de la promocion
	 * @return El número de tuplas insertadas
	 */
	
	public long crearHorario(PersistenceManager pm, String fechaInicial,String fechaFinal) {
		System.out.println("Llego a sql horario");
		Query q = pm.newQuery(SQL, "INSERT INTO " + pp.darTablaHorario() + "(id, fechainicio, fechafin) values "
				+ "("+cuenta+","+ "TO_DATE('" +fechaInicial + "' , 'yyyy-mm-dd hh24:mi:ss'), TO_DATE('" + fechaFinal + "','yyyy-mm-dd hh24:mi:ss'))");
		// q.setParameters(idPlanUso, tipoPlanUso, costoPlanUso, descuentoPlanUso, nombrePromocion);
		q.executeUnique();
	    return cuenta;
	}

}
