package uniandes.isis2304.parranderos.persistencia;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class SQLConsulta {

	/*
	 * **** Constantes
	 ***/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las
	 * sentencias de acceso a la base de datos Se renombra acá para facilitar la
	 * escritura de las sentencias
	 */
	private final static String SQL = HotelAndesPersistencia.SQL;

	/*
	 * **** Atributos
	 ***/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private HotelAndesPersistencia pp;

	private int cuenta;

	/*
	 * **** Métodos
	 ***/

	/**
	 * Constructor
	 * 
	 * @param pp
	 *            - El Manejador de persistencia de la aplicación
	 */
	public SQLConsulta(HotelAndesPersistencia pp) {
		this.pp = pp;
		cuenta = 0;
	}

	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un PlanUso a la base de
	 * datos de HotelAndes
	 * 
	 * @param pm
	 *            - El manejador de persistencia
	 * @param idPlanUso
	 *            - El identificador del planUso
	 * @param tipoPlanUso
	 *            - El tipo del Plan uso ()
	 * @param costoPlanUso
	 *            - El costo del plan de uso
	 * @param descuentoPlanUso
	 *            - El descuento del plan uso
	 * @param nombrePromocion
	 *            - el nombre de la promocion
	 * @return El número de tuplas insertadas
	 */
	public long consumoR10(PersistenceManager pm, int estado, Long idProducto, Long idSalon, Long idCuenta,
			Long idServicio, long numeroDocumento, String tipoDocumento, Long idHorario) {
		// TODO Auto-generated method stub
		cuenta++;
		System.out.println("llego a sql Consumo");
		// no se tienen en cuenta las 12 horas extra de los salones
		Query q = pm.newQuery(SQL,
				"INSERT INTO " + pp.darTablaConsumo()
						+ "(id, estado,idProducto,idSalon,idCuenta,idServicio,numDocEmpleado,tipoDocEmpleado,idhorario) values ("
						+ cuenta + "," + estado + "," + idProducto + "," + idSalon + "," + idCuenta + "," + idServicio
						+ "," + numeroDocumento + ",'" + tipoDocumento + "'," + idHorario + ")");
		return (long) q.executeUnique();
	}

	public List<String> consulta6(PersistenceManager pm, String unidadTiempo, String tipoPregunta, String detalle) {
		Query q1 = null;
		Query q2 = null;

		if (unidadTiempo.equals("semana") && tipoPregunta.equals("tipo")) {
			q1 = pm.newQuery(SQL, "SELECT DISTINCT\n"
					+ "       FIRST_VALUE(WEEK)  OVER (ORDER BY FREQ DESC, WEEK ASC) AS MAYOR_DEMANDA,\n"
					+ "       FIRST_VALUE(WEEK)  OVER (ORDER BY FREQ ASC, WEEK DESC) AS MENOR_DEMANDA\n" + "FROM\n"
					+ "(\n"
					+ "SELECT to_char(DATE2 - 7/24,'IYYY') as YEAR, to_char(DATE2 - 7/24,'IW') as WEEK,COUNT(*) AS FREQ\n"
					+ "FROM (\n" + "SELECT HABITACION.IDHABITACION, horario.fechainicio AS DATE2\n"
					+ "FROM HABITACION \n" + "INNER JOIN TIPO_HABITACION ON HABITACION.TIPO=TIPO_HABITACION.ID\n"
					+ "INNER JOIN cuenta on cuenta.idhabitacion = HABITACION.idHABITACION\n"
					+ "INNER JOIN CONSUMO ON CUENTA.IDCUENTA = CONSUMO.IDCUENTA\n"
					+ "INNER JOIN HORARIO ON HORARIO.ID = CONSUMO.IDHORARIO\n" + "WHERE  TIPO_HABITACION.NOMBRE = '"
					+ detalle + "'\n" + ")\n" + "GROUP BY to_char(DATE2 - 7/24,'IYYY'), to_char(DATE2 - 7/24,'IW')\n"
					+ ")");
			q2 = pm.newQuery(SQL, "SELECT DISTINCT\n"
					+ "       FIRST_VALUE(WEEK)  OVER (ORDER BY INGRESOS DESC, WEEK ASC) AS MAYOR_INGRESOS\n" + "FROM\n"
					+ "(\n"
					+ "SELECT to_char(DATE2 - 7/24,'IYYY') as YEAR, to_char(DATE2 - 7/24,'IW') as WEEK, SUM(COSTO) AS INGRESOS\n"
					+ "FROM (\n"
					+ "SELECT CONSUMO.ID, horario.fechainicio AS DATE2, COALESCE(PRODUCTO.VALOR,0)+ COALESCE(alquilersalones.costo,0)+ COALESCE(serviciobasico.costo,0) AS COSTO\n"
					+ "FROM HABITACION \n" + "INNER JOIN TIPO_HABITACION ON HABITACION.TIPO=TIPO_HABITACION.ID\n"
					+ "INNER JOIN cuenta on cuenta.idhabitacion = HABITACION.idHABITACION\n"
					+ "INNER JOIN CONSUMO ON CUENTA.IDCUENTA = CONSUMO.IDCUENTA\n"
					+ "INNER JOIN HORARIO ON HORARIO.ID = CONSUMO.IDHORARIO\n"
					+ "LEFT OUTER JOIN PRODUCTO ON PRODUCTO.ID = CONSUMO.IDPRODUCTO\n"
					+ "LEFT OUTER JOIN ALQUILERSALONES ON ALQUILERSALONES.ID = CONSUMO.IDSERVICIO\n"
					+ "LEFT OUTER JOIN SERVICIOBASICO ON SERVICIOBASICO.ID = CONSUMO.IDSERVICIO\n"
					+ "WHERE  TIPO_HABITACION.NOMBRE = '" + detalle + "'\n" + ")\n"
					+ "GROUP BY to_char(DATE2 - 7/24,'IYYY'), to_char(DATE2 - 7/24,'IW')\n" + "\n" + ")");
		} else if (unidadTiempo.equals("mes")) {
			if (tipoPregunta.equals("alquilersalones")) {
				q1 = pm.newQuery(SQL,

						"SELECT DISTINCT\n"
								+ "       FIRST_VALUE(WEEK)  OVER (ORDER BY INGRESOS DESC, WEEK ASC) AS MAYOR_INGRESOS\n"
								+ "FROM\n" + "(\n"
								+ "SELECT to_char(DATE2 - 7/24,'IYYY') as YEAR, to_char(DATE2 - 7/24,'MONTH') as WEEK, SUM(COSTO) AS INGRESOS\n"
								+ "FROM (\n" + "SELECT horario.fechainicio, ALQUILERSALONES.COSTO\n"
								+ "FROM ALQUILERSALONES\n"
								+ "INNER JOIN CONSUMO ON CONSUMO.IDSERVICIO = ALQUILERSALONES.ID\n"
								+ "INNER JOIN HORARIO ON HORARIO.ID = CONSUMO.IDHORARIO\n"
								+ "WHERE ALQUILERSALONES.ID = 11" + detalle + ")\n"
								+ "GROUP BY to_char(DATE2 - 7/24,'IYYY'), to_char(DATE2 - 7/24,'MONTH')\n" + "\n" + ")"

				);
				q2 = pm.newQuery(SQL,

						"SELECT DISTINCT\n"
								+ "       FIRST_VALUE(WEEK)  OVER (ORDER BY FREQ DESC, WEEK ASC) AS MAYOR_DEMANDA,\n"
								+ "       FIRST_VALUE(WEEK)  OVER (ORDER BY FREQ ASC, WEEK DESC) AS MENOR_DEMANDA\n"
								+ "FROM\n" + "(\n" + "SELECT horario.fechainicio, ALQUILERSALONES.COSTO\n"
								+ "FROM ALQUILERSALONES\n"
								+ "INNER JOIN CONSUMO ON CONSUMO.IDSERVICIO = ALQUILERSALONES.ID\n"
								+ "INNER JOIN HORARIO ON HORARIO.ID = CONSUMO.IDHORARIO\n"
								+ "WHERE ALQUILERSALONES.ID =" + detalle + ")\n"
								+ "GROUP BY to_char(DATE2 - 7/24,'IYYY'), to_char(DATE2 - 7/24,'MONTH')\n" + ")");

			}
			if (tipoPregunta.equals("serviciobasico")) {
				q1 = pm.newQuery(SQL, "SELECT DISTINCT\n"
						+ "       FIRST_VALUE(WEEK)  OVER (ORDER BY FREQ DESC, WEEK ASC) AS MAYOR_DEMANDA,\n"
						+ "       FIRST_VALUE(WEEK)  OVER (ORDER BY FREQ ASC, WEEK DESC) AS MENOR_DEMANDA\n" + "FROM\n"
						+ "(\n"
						+ "SELECT to_char(DATE2 - 7/24,'IYYY') as YEAR, to_char(DATE2 - 7/24,'MONTH') as WEEK,COUNT(*) AS FREQ\n"
						+ "FROM (\n" + "SELECT horario.fechainicio, SERVICIOBASICO.COSTO\n" + "FROM SERVICIOBASICO\n"
						+ "INNER JOIN CONSUMO ON CONSUMO.IDSERVICIO = SERVICIOBASICO.ID\n"
						+ "INNER JOIN HORARIO ON HORARIO.ID = CONSUMO.IDHORARIO\n" + "WHERE SERVICIOBASICO.ID = "
						+ detalle + ")\n" + "GROUP BY to_char(DATE2 - 7/24,'IYYY'), to_char(DATE2 - 7/24,'MONTH')\n"
						+ ")");
				q2 = pm.newQuery(SQL, "\n" + "SELECT DISTINCT\n"
						+ "       FIRST_VALUE(WEEK)  OVER (ORDER BY INGRESOS DESC, WEEK ASC) AS MAYOR_INGRESOS\n"
						+ "FROM\n" + "(\n"
						+ "SELECT to_char(DATE2 - 7/24,'IYYY') as YEAR, to_char(DATE2 - 7/24,'MONTH') as WEEK, SUM(COSTO) AS INGRESOS\n"
						+ "FROM (\n" + "SELECT horario.fechainicio, SERVICIOBASICO.COSTO\n" + "FROM SERVICIOBASICO\n"
						+ "INNER JOIN CONSUMO ON CONSUMO.IDSERVICIO = SERVICIOBASICO.ID\n"
						+ "INNER JOIN HORARIO ON HORARIO.ID = CONSUMO.IDHORARIO\n" + "WHERE SERVICIOBASICO.ID = "
						+ detalle + ")\n" + "GROUP BY to_char(DATE2 - 7/24,'IYYY'), to_char(DATE2 - 7/24,'MONTH')\n"
						+ "\n" + ")");

			}
			if (tipoPregunta.equals("ventaproducto")) {
				q2 = pm.newQuery(SQL, "SELECT DISTINCT\n"
						+ "       FIRST_VALUE(WEEK)  OVER (ORDER BY INGRESOS DESC, WEEK ASC) AS MAYOR_INGRESOS\n"
						+ "FROM\n" + "(\n"
						+ "SELECT to_char(DATE2 - 7/24,'IYYY') as YEAR, to_char(DATE2 - 7/24,'MONTH') as WEEK, SUM(COSTO) AS INGRESOS\n"
						+ "FROM (\n" + "SELECT horario.fechainicio, PRODUCTO.VALOR\n" + "FROM SERVICIO\n"
						+ "INNER JOIN PRODUCTO ON PRODUCTO.IDVENTAPRODUCTO = SERVICIO.ID\n"
						+ "INNER JOIN CONSUMO ON CONSUMO.IDPRODUCTO = PRODUCTO.ID\n"
						+ "INNER JOIN HORARIO ON HORARIO.ID = CONSUMO.IDHORARIO\n" + "WHERE SERVICIO.ID  = " + detalle
						+ ")\n" + "GROUP BY to_char(DATE2 - 7/24,'IYYY'), to_char(DATE2 - 7/24,'MONTH')\n" + "\n"
						+ ")");
				q1 = pm.newQuery(SQL,

						"SELECT DISTINCT\n"
								+ "       FIRST_VALUE(WEEK)  OVER (ORDER BY FREQ DESC, WEEK ASC) AS MAYOR_DEMANDA,\n"
								+ "       FIRST_VALUE(WEEK)  OVER (ORDER BY FREQ ASC, WEEK DESC) AS MENOR_DEMANDA\n"
								+ "FROM\n" + "(\n"
								+ "SELECT to_char(DATE2 - 7/24,'IYYY') as YEAR, to_char(DATE2 - 7/24,'MONTH') as WEEK,COUNT(*) AS FREQ\n"
								+ "FROM (\n" + "SELECT horario.fechainicio, PRODUCTO.VALOR\n" + "FROM SERVICIO\n"
								+ "INNER JOIN PRODUCTO ON PRODUCTO.IDVENTAPRODUCTO = SERVICIO.ID\n"
								+ "INNER JOIN CONSUMO ON CONSUMO.IDPRODUCTO = PRODUCTO.ID\n"
								+ "INNER JOIN HORARIO ON HORARIO.ID = CONSUMO.IDHORARIO\n" + "WHERE SERVICIO.ID  ="
								+ detalle + ")\n"
								+ "GROUP BY to_char(DATE2 - 7/24,'IYYY'), to_char(DATE2 - 7/24,'MONTH')\n" + ")");

			}

		} else if (unidadTiempo.equals("semana") && tipoPregunta.equals("servicio")) {
			if (tipoPregunta.equals("alquilersalones")) {
				q1 = pm.newQuery(SQL,

						"SELECT DISTINCT\n"
								+ "       FIRST_VALUE(WEEK)  OVER (ORDER BY INGRESOS DESC, WEEK ASC) AS MAYOR_INGRESOS\n"
								+ "FROM\n" + "(\n"
								+ "SELECT to_char(DATE2 - 7/24,'IYYY') as YEAR, to_char(DATE2 - 7/24,'IW') as WEEK, SUM(COSTO) AS INGRESOS\n"
								+ "FROM (\n" + "SELECT horario.fechainicio, ALQUILERSALONES.COSTO\n"
								+ "FROM ALQUILERSALONES\n"
								+ "INNER JOIN CONSUMO ON CONSUMO.IDSERVICIO = ALQUILERSALONES.ID\n"
								+ "INNER JOIN HORARIO ON HORARIO.ID = CONSUMO.IDHORARIO\n"
								+ "WHERE ALQUILERSALONES.ID = 11" + detalle + ")\n"
								+ "GROUP BY to_char(DATE2 - 7/24,'IYYY'), to_char(DATE2 - 7/24,'IW')\n" + "\n" + ")"

				);
				q2 = pm.newQuery(SQL,

						"SELECT DISTINCT\n"
								+ "       FIRST_VALUE(WEEK)  OVER (ORDER BY FREQ DESC, WEEK ASC) AS MAYOR_DEMANDA,\n"
								+ "       FIRST_VALUE(WEEK)  OVER (ORDER BY FREQ ASC, WEEK DESC) AS MENOR_DEMANDA\n"
								+ "FROM\n" + "(\n"
								+ "SELECT to_char(DATE2 - 7/24,'IYYY') as YEAR, to_char(DATE2 - 7/24,'IW') as WEEK,COUNT(*) AS FREQ\n"
								+ "FROM (\n" + "SELECT horario.fechainicio, ALQUILERSALONES.COSTO\n"
								+ "FROM ALQUILERSALONES\n"
								+ "INNER JOIN CONSUMO ON CONSUMO.IDSERVICIO = ALQUILERSALONES.ID\n"
								+ "INNER JOIN HORARIO ON HORARIO.ID = CONSUMO.IDHORARIO\n"
								+ "WHERE ALQUILERSALONES.ID = " + detalle + ")\n"
								+ "GROUP BY to_char(DATE2 - 7/24,'IYYY'), to_char(DATE2 - 7/24,'IW')\n" + ")");

			}
			if (tipoPregunta.equals("serviciobasico")) {
				q1 = pm.newQuery(SQL, "SELECT DISTINCT\n"
						+ "       FIRST_VALUE(WEEK)  OVER (ORDER BY FREQ DESC, WEEK ASC) AS MAYOR_DEMANDA,\n"
						+ "       FIRST_VALUE(WEEK)  OVER (ORDER BY FREQ ASC, WEEK DESC) AS MENOR_DEMANDA\n" + "FROM\n"
						+ "(\n"
						+ "SELECT to_char(DATE2 - 7/24,'IYYY') as YEAR, to_char(DATE2 - 7/24,'IW') as WEEK,COUNT(*) AS FREQ\n"
						+ "FROM (\n" + "SELECT horario.fechainicio, SERVICIOBASICO.COSTO\n" + "FROM SERVICIOBASICO\n"
						+ "INNER JOIN CONSUMO ON CONSUMO.IDSERVICIO = SERVICIOBASICO.ID\n"
						+ "INNER JOIN HORARIO ON HORARIO.ID = CONSUMO.IDHORARIO\n" + "WHERE SERVICIOBASICO.ID = "
						+ detalle + ")\n" + "GROUP BY to_char(DATE2 - 7/24,'IYYY'), to_char(DATE2 - 7/24,'IW')\n"
						+ ")");
				q2 = pm.newQuery(SQL, "SELECT DISTINCT\n"
						+ "       FIRST_VALUE(WEEK)  OVER (ORDER BY INGRESOS DESC, WEEK ASC) AS MAYOR_INGRESOS\n"
						+ "FROM\n" + "(\n"
						+ "SELECT to_char(DATE2 - 7/24,'IYYY') as YEAR, to_char(DATE2 - 7/24,'MONTH') as WEEK, SUM(COSTO) AS INGRESOS\n"
						+ "FROM (\n" + "SELECT horario.fechainicio, SERVICIOBASICO.COSTO\n" + "FROM SERVICIOBASICO\n"
						+ "INNER JOIN CONSUMO ON CONSUMO.IDSERVICIO = SERVICIOBASICO.ID\n"
						+ "INNER JOIN HORARIO ON HORARIO.ID = CONSUMO.IDHORARIO\n" + "WHERE SERVICIOBASICO.ID = "
						+ detalle + ")\n" + "GROUP BY to_char(DATE2 - 7/24,'IYYY'), to_char(DATE2 - 7/24,'MONTH')\n"
						+ "\n" + ")");

			}
			if (tipoPregunta.equals("ventaproducto")) {
				q2 = pm.newQuery(SQL, "SELECT DISTINCT\n"
						+ "       FIRST_VALUE(WEEK)  OVER (ORDER BY INGRESOS DESC, WEEK ASC) AS MAYOR_INGRESOS\n"
						+ "FROM\n" + "(\n"
						+ "SELECT to_char(DATE2 - 7/24,'IYYY') as YEAR, to_char(DATE2 - 7/24,'IW') as WEEK, SUM(COSTO) AS INGRESOS\n"
						+ "FROM (\n" + "SELECT horario.fechainicio, PRODUCTO.VALOR\n" + "FROM SERVICIO\n"
						+ "INNER JOIN PRODUCTO ON PRODUCTO.IDVENTAPRODUCTO = SERVICIO.ID\n"
						+ "INNER JOIN CONSUMO ON CONSUMO.IDPRODUCTO = PRODUCTO.ID\n"
						+ "INNER JOIN HORARIO ON HORARIO.ID = CONSUMO.IDHORARIO\n" + "WHERE SERVICIO.ID  = " + detalle
						+ ")\n" + "GROUP BY to_char(DATE2 - 7/24,'IYYY'), to_char(DATE2 - 7/24,'IW')\n" + "\n" + ")");
				q1 = pm.newQuery(SQL,

						"SELECT DISTINCT\n"
								+ "       FIRST_VALUE(WEEK)  OVER (ORDER BY FREQ DESC, WEEK ASC) AS MAYOR_DEMANDA,\n"
								+ "       FIRST_VALUE(WEEK)  OVER (ORDER BY FREQ ASC, WEEK DESC) AS MENOR_DEMANDA\n"
								+ "FROM\n" + "(\n"
								+ "SELECT to_char(DATE2 - 7/24,'IYYY') as YEAR, to_char(DATE2 - 7/24,'IW') as WEEK,COUNT(*) AS FREQ\n"
								+ "FROM (\n" + "SELECT horario.fechainicio, PRODUCTO.VALOR\n" + "FROM SERVICIO\n"
								+ "INNER JOIN PRODUCTO ON PRODUCTO.IDVENTAPRODUCTO = SERVICIO.ID\n"
								+ "INNER JOIN CONSUMO ON CONSUMO.IDPRODUCTO = PRODUCTO.ID\n"
								+ "INNER JOIN HORARIO ON HORARIO.ID = CONSUMO.IDHORARIO\n" + "WHERE SERVICIO.ID  ="
								+ detalle + ")\n"
								+ "GROUP BY to_char(DATE2 - 7/24,'IYYY'), to_char(DATE2 - 7/24,'IW')\n" + ")");
			}

		} else if (unidadTiempo.equals("mes") && tipoPregunta.equals("tipo")) {
			q1 = pm.newQuery(SQL, "" + "SELECT DISTINCT\n"
					+ "       FIRST_VALUE(WEEK)  OVER (ORDER BY FREQ DESC, WEEK ASC) AS MAYOR_DEMANDA,\n"
					+ "       FIRST_VALUE(WEEK)  OVER (ORDER BY FREQ ASC, WEEK DESC) AS MENOR_DEMANDA\n" + "FROM\n"
					+ "(\n"
					+ "SELECT to_char(DATE2 - 7/24,'IYYY') as YEAR, to_char(DATE2 - 7/24,'MONTH') as WEEK,COUNT(*) AS FREQ\n"
					+ "FROM (\n" + "SELECT HABITACION.IDHABITACION, horario.fechainicio AS DATE2\n"
					+ "FROM HABITACION \n" + "INNER JOIN TIPO_HABITACION ON HABITACION.TIPO=TIPO_HABITACION.ID\n"
					+ "INNER JOIN cuenta on cuenta.idhabitacion = HABITACION.idHABITACION\n"
					+ "INNER JOIN CONSUMO ON CUENTA.IDCUENTA = CONSUMO.IDCUENTA\n"
					+ "INNER JOIN HORARIO ON HORARIO.ID = CONSUMO.IDHORARIO\n" + "WHERE  TIPO_HABITACION.NOMBRE = '"
					+ detalle + "'\n" + ")\n" + "GROUP BY to_char(DATE2 - 7/24,'IYYY'), to_char(DATE2 - 7/24,'MONTH')\n"
					+ ")");

			q2 = pm.newQuery(SQL, "" + "SELECT DISTINCT\n"
					+ "       FIRST_VALUE(WEEK)  OVER (ORDER BY INGRESOS DESC, WEEK ASC) AS MAYOR_INGRESOS\n" + "FROM\n"
					+ "(\n"
					+ "SELECT to_char(DATE2 - 7/24,'IYYY') as YEAR, to_char(DATE2 - 7/24,'MONTH') as WEEK, SUM(COSTO) AS INGRESOS\n"
					+ "FROM (\n"
					+ "SELECT CONSUMO.ID, horario.fechainicio AS DATE2, COALESCE(PRODUCTO.VALOR,0)+ COALESCE(alquilersalones.costo,0)+ COALESCE(serviciobasico.costo,0) AS COSTO\n"
					+ "FROM HABITACION \n" + "INNER JOIN TIPO_HABITACION ON HABITACION.TIPO=TIPO_HABITACION.ID\n"
					+ "INNER JOIN cuenta on cuenta.idhabitacion = HABITACION.idHABITACION\n"
					+ "INNER JOIN CONSUMO ON CUENTA.IDCUENTA = CONSUMO.IDCUENTA\n"
					+ "INNER JOIN HORARIO ON HORARIO.ID = CONSUMO.IDHORARIO\n"
					+ "LEFT OUTER JOIN PRODUCTO ON PRODUCTO.ID = CONSUMO.IDPRODUCTO\n"
					+ "LEFT OUTER JOIN ALQUILERSALONES ON ALQUILERSALONES.ID = CONSUMO.IDSERVICIO\n"
					+ "LEFT OUTER JOIN SERVICIOBASICO ON SERVICIOBASICO.ID = CONSUMO.IDSERVICIO\n"
					+ "WHERE  TIPO_HABITACION.NOMBRE = '" + detalle + "'\n" + ")\n"
					+ "GROUP BY to_char(DATE2 - 7/24,'IYYY'), to_char(DATE2 - 7/24,'MONTH')\n" + "\n" + ")");
		}

		q1.executeList();
		q1.setResultClass(MyResultClass.class);
		List<MyResultClass> results = (List<MyResultClass>) q1.execute();
		Object e = q2.executeUnique();
		List<String> result = new ArrayList<String>();
		result.add(results.get(0).get1());
		result.add(results.get(0).get2());
		result.add(e.toString());
		return result;

	}

	public class MyResultClass {
		protected String mayorDemanda = "";
		protected String menorDemanda = "";

		public MyResultClass(String may, String men) {
			this.mayorDemanda = may;
			this.menorDemanda = men;
		}

		public String get1() {
			return mayorDemanda;
		}

		public String get2() {
			return menorDemanda;
		}
	}

	public List consulta7(PersistenceManager pm, String tipoConsulta) {

		Query q1 = null;
		if (tipoConsulta.equals(1)) {
			q1 = pm.newQuery(SQL,
					"SELECT  tipodocumento, NUMERODOCUMENTO, SUM(FECHAOUT-FECHAIN) AS DÍAS_DE_ESTADÍA\n"
							+ "FROM RESPONSABLE\n" + "INNER JOIN CUENTA ON CUENTA.IDCUENTA = RESPONSABLE.IDCUENTA\n"
							+ "GROUP BY NUMERODOCUMENTO,TIPODOCUMENTO\n" + "HAVING SUM(FECHAOUT-FECHAIN)>14");
		} else {
			q1 = pm.newQuery(SQL,
					"SELECT tipodocumento, NUMERODOCUMENTO, SUM(COALESCE(PRODUCTO.VALOR,0)+ COALESCE(alquilersalones.costo,0)+ COALESCE(serviciobasico.costo,0)) AS CONSUMO_TOTAL\n"
							+ "FROM RESPONSABLE\n" + "INNER JOIN CUENTA ON CUENTA.IDCUENTA = RESPONSABLE.IDCUENTA\n"
							+ "INNER JOIN CONSUMO ON CONSUMO.IDCUENTA = CUENTA.IDCUENTA\n"
							+ "LEFT OUTER JOIN PRODUCTO ON PRODUCTO.ID = CONSUMO.IDPRODUCTO\n"
							+ "LEFT OUTER JOIN ALQUILERSALONES ON ALQUILERSALONES.ID = CONSUMO.IDSERVICIO\n"
							+ "LEFT OUTER JOIN SERVICIOBASICO ON SERVICIOBASICO.ID = CONSUMO.IDSERVICIO\n"
							+ "GROUP BY NUMERODOCUMENTO,TIPODOCUMENTO\n"
							+ "HAVING SUM(COALESCE(PRODUCTO.VALOR,0)+ COALESCE(alquilersalones.costo,0)+ COALESCE(serviciobasico.costo,0))>15000 ");
		}
		List lista = q1.executeList();
		return lista;

	}

	public List consulta8(PersistenceManager pm) {
		Query q1 = pm.newQuery(SQL,
				"SELECT to_char(DATE2 - 7/24,'IYYY') as YEAR, to_char(DATE2 - 7/24,'IW') as WEEK,COUNT(*) AS FREQ, SERVICIO\n"
						+ "FROM (\n"
						+ "SELECT horario.fechainicio as date2, SERVICIOBASICO.COSTO, SERVICIOBASICO.NOMBRE AS SERVICIO\n"
						+ "FROM SERVICIOBASICO\n" + "INNER JOIN CONSUMO ON CONSUMO.IDSERVICIO = SERVICIOBASICO.ID\n"
						+ "INNER JOIN HORARIO ON HORARIO.ID = CONSUMO.IDHORARIO\n" + ")\n"
						+ "GROUP BY SERVICIO, to_char(DATE2 - 7/24,'IYYY'), to_char(DATE2 - 7/24,'IW') \n"
						+ "HAVING COUNT(*)<3");
		Query q2 = pm.newQuery(SQL,
				"SELECT to_char(DATE2 - 7/24,'IYYY') as YEAR, to_char(DATE2 - 7/24,'IW') as WEEK,COUNT(*) AS FREQ, TIPO\n"
						+ "FROM (\n"
						+ "SELECT horario.fechainicio AS DATE2, ALQUILERSALONES.COSTO, ALQUILERSALONES.TIPO AS TIPO\n"
						+ "FROM ALQUILERSALONES\n" + "INNER JOIN CONSUMO ON CONSUMO.IDSERVICIO= ALQUILERSALONES.ID\n"
						+ "INNER JOIN HORARIO ON HORARIO.ID = CONSUMO.IDHORARIO\n" + "WHERE ALQUILERSALONES.ID = 11\n"
						+ ")\n" + "GROUP BY TIPO, to_char(DATE2 - 7/24,'IYYY'), to_char(DATE2 - 7/24,'IW') \n"
						+ "HAVING COUNT(*)<3");

		Query q3 = pm.newQuery(SQL,
				"SELECT to_char(DATE2 - 7/24,'IYYY') as YEAR, to_char(DATE2 - 7/24,'IW') as WEEK, COUNT(*) AS FREQ\n"
						+ "FROM (\n" + "SELECT horario.fechainicio AS DATE2, PRODUCTO.VALOR\n" + "FROM SERVICIO\n"
						+ "INNER JOIN PRODUCTO ON PRODUCTO.IDVENTAPRODUCTO = SERVICIO.ID\n"
						+ "INNER JOIN CONSUMO ON CONSUMO.IDPRODUCTO = PRODUCTO.ID\n"
						+ "INNER JOIN HORARIO ON HORARIO.ID = CONSUMO.IDHORARIO\n" + ")\n"
						+ "GROUP BY to_char(DATE2 - 7/24,'IYYY'), to_char(DATE2 - 7/24,'IW')\n" + "HAVING COUNT(*)<4");

		List lista = q1.executeList();
		List lista1 = q2.executeList();
		List lista2 = q3.executeList();

		lista.addAll(lista1);
		lista.addAll(lista2);
		return lista;
	}

	public List consulta11(PersistenceManager pm, int criterio) {
		List<String> listaSring = new ArrayList<String>();
		List<Object[]> listaObject = null;
		if (criterio == 1) {
			Query q1 = pm.newQuery(SQL, "SELECT idservicio, \n" + "       semana, \n" + "       nombre, \n"
					+ "       cantidad \n" + "FROM   (SELECT idservicio, \n" + "               semana, \n"
					+ "               nombre, \n" + "               cantidad, \n" + "               Row_number() \n"
					+ "                 OVER ( \n" + "                   partition BY semana \n"
					+ "                   ORDER BY cantidad DESC) AS rn \n" + "        FROM   (SELECT idservicio, \n"
					+ "                       semana, \n" + "                       cantidad, \n"
					+ "                       nombre \n"
					+ "                FROM   (SELECT serv.id                               AS \n"
					+ "                               idservicio, \n"
					+ "                               To_char(h.fechainicio - 7 / 24, 'IW') AS semana, \n"
					+ "                               Count(serv.id)                        AS cantidad, \n"
					+ "                               servb.nombre \n"
					+ "                               || vent.tipo \n"
					+ "                               || alqui.tipo                         AS nombre \n"
					+ "                        FROM   horario h \n"
					+ "                               INNER JOIN consumo cons \n"
					+ "                                       ON h.id = cons.idhorario \n"
					+ "                               INNER JOIN servicio serv \n"
					+ "                                       ON cons.idservicio = serv.id \n"
					+ "                               LEFT OUTER JOIN serviciobasico servb \n"
					+ "                                            ON servb.id = serv.id \n"
					+ "                               LEFT OUTER JOIN ventaproducto vent \n"
					+ "                                            ON vent.id = serv.id \n"
					+ "                               LEFT OUTER JOIN alquilersalones alqui \n"
					+ "                                            ON alqui.id = serv.id \n"
					+ "                        GROUP  BY serv.id, \n"
					+ "                                  h.fechainicio, \n"
					+ "                                  servb.nombre, \n"
					+ "                                  vent.tipo, \n"
					+ "                                  alqui.tipo \n"
					+ "                        ORDER  BY Count(serv.id)))) \n" + "WHERE  rn = 1\n" + "UNION ALL\n"
					+ "SELECT idservicio, \n" + "       semana, \n" + "       nombre, \n" + "       cantidad \n"
					+ "FROM   (SELECT idservicio, \n" + "               semana, \n" + "               nombre, \n"
					+ "               cantidad, \n" + "               Row_number() \n" + "                 OVER ( \n"
					+ "                   partition BY semana \n" + "                   ORDER BY cantidad ASC) AS rn \n"
					+ "        FROM   (SELECT idservicio, \n" + "                       semana, \n"
					+ "                       cantidad, \n" + "                       nombre \n"
					+ "                FROM   (SELECT serv.id                               AS \n"
					+ "                               idservicio, \n"
					+ "                               To_char(h.fechainicio - 7 / 24, 'IW') AS semana, \n"
					+ "                               Count(serv.id)                        AS cantidad, \n"
					+ "                               servb.nombre \n"
					+ "                               || vent.tipo \n"
					+ "                               || alqui.tipo                         AS nombre \n"
					+ "                        FROM   horario h \n"
					+ "                               INNER JOIN consumo cons \n"
					+ "                                       ON h.id = cons.idhorario \n"
					+ "                               INNER JOIN servicio serv \n"
					+ "                                       ON cons.idservicio = serv.id \n"
					+ "                               LEFT OUTER JOIN serviciobasico servb \n"
					+ "                                            ON servb.id = serv.id \n"
					+ "                               LEFT OUTER JOIN ventaproducto vent \n"
					+ "                                            ON vent.id = serv.id \n"
					+ "                               LEFT OUTER JOIN alquilersalones alqui \n"
					+ "                                            ON alqui.id = serv.id \n"
					+ "                        GROUP  BY serv.id, \n"
					+ "                                  h.fechainicio, \n"
					+ "                                  servb.nombre, \n"
					+ "                                  vent.tipo, \n"
					+ "                                  alqui.tipo \n"
					+ "                        ORDER  BY Count(serv.id)))) \n" + "WHERE  rn = 1");
			listaObject = q1.executeList();
			int idServicio = 0;
			String semana = "";
			String nombre = "";
			int cantidad = 0;
			listaSring.add("Lista servicios más consumidos:\n");
			int i = 0;
			for (Object[] obj : listaObject) {
				if (i == 4)
					listaSring.add("Lista servicios menos consumidos:\n");
				idServicio = ((BigDecimal) obj[0]).intValue();
				semana = (String) obj[1];
				nombre = (String) obj[2];
				cantidad = ((BigDecimal) obj[3]).intValue();
				listaSring.add("IDserivicio: " + idServicio + " NombreSerivicio: " + nombre + " Semana: " + semana
						+ " #Consumos: " + cantidad + "\n");
				i++;
			}

		} else {
			Query q2 = pm.newQuery(SQL,
					"SELECT id, \n" + "       semana, \n" + "       cantidad \n" + "FROM   (SELECT id, \n"
							+ "               semana, \n" + "               cantidad, \n"
							+ "               Row_number() \n" + "                 OVER ( \n"
							+ "                   partition BY semana \n"
							+ "                   ORDER BY cantidad DESC) AS rn \n" + "        FROM   (SELECT id, \n"
							+ "                       semana, \n" + "                       cantidad \n"
							+ "                FROM   (SELECT cuenta.idhabitacion                    AS id, \n"
							+ "                               Count(cuenta.idhabitacion)             AS CANTIDAD, \n"
							+ "                               To_char(cuenta.fechain - 7 / 24, 'IW') AS SEMANA \n"
							+ "                        FROM   cuenta \n"
							+ "                               INNER JOIN habitacion \n"
							+ "                                       ON cuenta.idhabitacion = \n"
							+ "                                          habitacion.idhabitacion \n"
							+ "                        GROUP  BY cuenta.idhabitacion, \n"
							+ "                                  cuenta.fechain \n" + "                        ))) \n"
							+ "WHERE  rn = 1\n" + "UNION ALL \n" + "SELECT id, \n" + "       semana, \n"
							+ "       cantidad \n" + "FROM   (SELECT id, \n" + "               semana, \n"
							+ "               cantidad, \n" + "               Row_number() \n"
							+ "                 OVER ( \n" + "                   partition BY semana \n"
							+ "                   ORDER BY cantidad ASC) AS rn \n" + "        FROM   (SELECT id, \n"
							+ "                       semana, \n" + "                       cantidad \n"
							+ "                FROM   (SELECT cuenta.idhabitacion                    AS id, \n"
							+ "                               Count(cuenta.idhabitacion)             AS CANTIDAD, \n"
							+ "                               To_char(cuenta.fechain - 7 / 24, 'IW') AS SEMANA \n"
							+ "                        FROM   cuenta \n"
							+ "                               INNER JOIN habitacion \n"
							+ "                                       ON cuenta.idhabitacion = \n"
							+ "                                          habitacion.idhabitacion \n"
							+ "                        GROUP  BY cuenta.idhabitacion, \n"
							+ "                                  cuenta.fechain \n" + "                        ))) \n"
							+ "WHERE  rn = 1");
			listaObject = q2.executeList();
			int idServicio = 0;
			String semana = "";
			int cantidad = 0;
			listaSring.add("Lista habitaciones más reservadas:\n");
			int i = 0;
			for (Object[] obj : listaObject) {
				if (i == 4)
					listaSring.add("Lista habitaciones menos reservadas:\n");
				idServicio = ((BigDecimal) obj[0]).intValue();
				semana = (String) obj[1];
				cantidad = ((BigDecimal) obj[2]).intValue();
				listaSring.add(
						"IDhabitacion: " + idServicio + " Semana: " + semana + " #Reservaciones: " + cantidad + "\n");
				i++;
			}
		}
		return listaSring;

	}

	public List consulta12(PersistenceManager pm, int criterio) {
		if (criterio == 1) {
			return consulta12_1(pm);
		} else if (criterio == 2) {
			return consulta12_2(pm);
		} else {
			return consulta12_3(pm);

		}
	}

	private List consulta12_2(PersistenceManager pm) {
		List<String> listaSring = new ArrayList<String>();
		List<Object[]> listaObject = null;
		Query q = pm.newQuery(SQL, "SELECT numDocCliente, TipoDoc, CLIENTE, IDSERVICIO, NOMBRE, COSTO\n" + "FROM (\n"
				+ "SELECT usu.numerodocumento as numDocCliente, usu.tipodocumento as TipoDoc, usu.nombre as CLIENTE, serv.id as IDSERVICIO, ventProd.valor || servb.costo || alqui.costo as costo, ventProd.nombre || servb.nombre || alqui.tipo as NOMBRE\n"
				+ "FROM   servicio serv \n" + "       LEFT OUTER JOIN serviciobasico servb \n"
				+ "                    ON servb.id = serv.id \n" + "                    \n"
				+ "       LEFT OUTER JOIN (SELECT vent.id as id, prod.nombre as nombre, prod.valor as valor \n"
				+ "                        FROM   ventaproducto vent \n"
				+ "                               INNER JOIN producto prod \n"
				+ "                                       ON prod.idventaproducto = vent.id) ventProd \n"
				+ "                    ON ventProd.id = serv.id \n" + "       LEFT OUTER JOIN alquilersalones alqui \n"
				+ "                    ON alqui.id = serv.id\n" + "       INNER JOIN CONSUMO cons\n"
				+ "                    ON cons.idservicio = serv.id\n" + "       INNER JOIN CUENTA cu\n"
				+ "                    ON cu.idcuenta = cons.idcuenta\n" + "       INNER JOIN RESPONSABLE re\n"
				+ "                    ON re.idcuenta = cu.idcuenta\n" + "       INNER JOIN USUARIO usu\n"
				+ "                    ON usu.numerodocumento = re.numerodocumento\n"
				+ "                    AND usu.tipodocumento = re.tipodocumento\n"
				+ "       GROUP  BY usu.numerodocumento,\n" + "                 usu.tipodocumento,\n"
				+ "                 usu.nombre,\n" + "                 serv.id, \n"
				+ "                 servb.nombre, \n" + "                 ventProd.nombre, \n"
				+ "                 alqui.tipo,\n" + "                 ventProd.valor,\n"
				+ "                 servb.costo,\n" + "                 alqui.costo\n" + ")\n" + "WHERE COSTO>=300000");
		listaObject = q.executeList();
		int numDoc = 0;
		String tipoDoc = "";
		String nombre = "";
		int idServ = 0;
		String nombreServicio = "";
		String costo = "";
		listaSring.add("Lista BUENOS clientes");
		int i = 0;
		for (Object[] obj : listaObject) {
			if (i == 10)
				return listaSring;
			numDoc = ((BigDecimal) obj[0]).intValue();
			tipoDoc = (String) obj[1];
			nombre = (String) obj[2];
			idServ = ((BigDecimal) obj[3]).intValue();
			nombreServicio = (String) obj[4];
			costo = (String) obj[5];
			listaSring.add("Numero doc: " + numDoc + " TipoDoc: " + tipoDoc + " Nombre: " + nombre + " IdServ " + idServ
					+ " Nombre Serv: " + nombreServicio + " Costo " + costo + "\n");
			i++;
		}
		return listaSring;
	}

	private List consulta12_1(PersistenceManager pm) {

		List<String> listaSring = new ArrayList<String>();
		List<Object[]> listaObject = null;
		Query q = pm.newQuery(SQL,
				"select numeroDocumento, tipodocumento, nombre, COUNT(numeroDocumento) as TrimestresDiferentes\n"
						+ "FROM\n" + "(\n"
						+ "SELECT usu.numeroDocumento, usu.tipodocumento, usu.nombre, To_char(CU.fechain - 7 / 24, 'Q')\n"
						+ "FROM USUARIO usu\n" + "INNER JOIN RESPONSABLE re\n"
						+ "ON usu.numerodocumento = re.numerodocumento\n"
						+ "AND usu.tipodocumento = re.tipodocumento \n" + "INNER JOIN CUENTA cu\n"
						+ "ON re.idcuenta = cu.idcuenta \n"
						+ "group by CU.fechain, usu.numeroDocumento, usu.tipodocumento, usu.nombre  \n"
						+ "order by usu.numeroDocumento\n" + ")\n" + "GROUP BY numeroDocumento, tipodocumento, nombre\n"
						+ "HAVING COUNT(numeroDocumento)>3");
		listaObject = q.executeList();
		int numDoc = 0;
		String tipoDoc = "";
		String nombre = "";
		int numTrim = 0;
		listaSring.add("Lista BUENOS clientes");
		int i = 0;
		for (Object[] obj : listaObject) {
			if (i == 10)
				break;
			numDoc = ((BigDecimal) obj[0]).intValue();
			tipoDoc = (String) obj[1];
			nombre = (String) obj[2];
			numTrim = ((BigDecimal) obj[3]).intValue();
			listaSring.add("Numero doc: " + numDoc + " TipoDoc: " + tipoDoc + " Nombre: " + nombre
					+ "Trimestres en los que hizo reservas" + numTrim + "\n");
			i++;
		}
		return listaSring;
	}

	private List consulta12_3(PersistenceManager pm) {
		List<String> listaSring = new ArrayList<String>();
		List<Object[]> listaObject = null;
		Query q = pm.newQuery(SQL,
				"SELECT usu.numerodocumento as numDocCliente, usu.tipodocumento as TipoDoc, usu.nombre as CLIENTE, SERV.ID, (H.FECHAFIN - H.FECHAINICIO)*24 AS DURATION\n"
						+ "FROM USUARIO USU\n" + "INNER JOIN RESPONSABLE RE\n"
						+ "ON usu.numerodocumento = re.numerodocumento\n" + "AND usu.tipodocumento = re.tipodocumento\n"
						+ "INNER JOIN CUENTA CU\n" + "ON re.idcuenta = cu.idcuenta\n" + "INNER JOIN CONSUMO CONS\n"
						+ "ON cu.idcuenta = cons.idcuenta\n" + "INNER JOIN HORARIO H\n" + "ON H.ID = CONS.IDHORARIO\n"
						+ "INNER JOIN SERVICIO SERV\n" + "ON cons.idservicio = serv.id\n"
						+ "LEFT OUTER JOIN ALQUILERSALONES ALQUI\n" + "ON ALQUI.ID = SERV.ID\n"
						+ "WHERE ((H.FECHAFIN - H.FECHAINICIO)*24 > 3\n" + "AND SERV.ID>10)\n" + "OR SERV.ID=7\n"
						+ "GROUP BY  usu.numerodocumento, usu.tipodocumento, usu.nombre, SERV.ID, (H.FECHAFIN- H.FECHAINICIO)*24\n");
		listaObject = q.executeList();
		int numDoc = 0;
		String tipoDoc = "";
		String nombre = "";
		int idServ = 0;
		int duration = 0;
		listaSring.add("Lista BUENOS clientes");
		int i = 0;
		for (Object[] obj : listaObject) {
			if (i == 10)
				break;
			numDoc = ((BigDecimal) obj[0]).intValue();
			tipoDoc = (String) obj[1];
			nombre = (String) obj[2];
			idServ = ((BigDecimal) obj[3]).intValue();
			duration = ((BigDecimal) obj[4]).intValue();
			listaSring.add("Numero doc: " + numDoc + " TipoDoc: " + tipoDoc + " Nombre: " + nombre + " ID servicio: "
					+ idServ + " Duracion: " + duration + "\n");
			i++;
		}
		return listaSring;
	}

	public List consulta9(PersistenceManager pm, Timestamp fechaIni, Timestamp fechaFin, List servicioParaBuscar,
			List tipoDeOrdenamientoo) {
		if (tipoDeOrdenamientoo.get(0).equals("Fecha")) {
			System.out.println("Fecha Bien");
			Query q1 = pm.newQuery(SQL,
					"SELECT unique NUMERODOCUMENTO\n " + "FROM RESPONSABLE c INNER JOIN\n " + "(SELECT idCuenta\n "
							+ "FROM HORARIO a INNER JOIN (SELECT idCuenta, idHorario\n " + "FROM CONSUMO\n"
							+ "WHERE IDSERVICIO = " + servicioParaBuscar.get(0) + ") b\n " + "ON b.idHorario = a.id\n "
							+ "WHERE a.fechainicio between TO_DATE('" + fechaIni.toString().substring(0, 19)
							+ "', 'YYYY-MM-DD HH24:MI:SS') and TO_DATE('" + fechaFin.toString().substring(0, 19)
							+ "', 'YYYY-MM-DD HH24:MI:SS')\n" + "order by a.fechainicio) d\n "
							+ "ON c.idcuenta = d.idcuenta\n ");
			List lista = q1.executeList();
			return lista;
		} else if (tipoDeOrdenamientoo.get(0).equals("Número de veces que utilizó el servicio")) {
			System.out.println("Veces Servicio bien");
			Query q1 = pm.newQuery(SQL,
					"select numeroDocumento\n" + "FROM\n" + "(\n" + "SELECT NUMERODOCUMENTO\n "
							+ "FROM RESPONSABLE c INNER JOIN\n " + "(SELECT idCuenta\n "
							+ "FROM HORARIO a INNER JOIN (SELECT idCuenta, idHorario\n " + "FROM CONSUMO\n"
							+ "WHERE IDSERVICIO = " + servicioParaBuscar.get(0) + ") b\n " + "ON b.idHorario = a.id\n "
							+ "WHERE a.fechainicio between TO_DATE('" + fechaIni.toString().substring(0, 19)
							+ "', 'YYYY-MM-DD HH24:MI:SS') and TO_DATE('" + fechaFin.toString().substring(0, 19)
							+ "', 'YYYY-MM-DD HH24:MI:SS')) d\n " + "ON c.idcuenta = d.idcuenta\n "
							+ ")\n" + "GROUP BY numeroDocumento\n"
							+ "order by COUNT(numeroDocumento) DESC");
			List lista = q1.executeList();
			return lista;
		} else if (tipoDeOrdenamientoo.get(0).equals("Nombre de usuario")) {
			System.out.println("Nombre Bien");
			Query q1 = pm.newQuery(SQL,
					"SELECT unique NOMBRE FROM USUARIO usu INNER JOIN (SELECT unique NUMERODOCUMENTO\n "
							+ "FROM RESPONSABLE c INNER JOIN\n " + "(SELECT idCuenta\n "
							+ "FROM HORARIO a INNER JOIN (SELECT idCuenta, idHorario\n " + "FROM CONSUMO\n"
							+ "WHERE IDSERVICIO = " + servicioParaBuscar.get(0) + ") b\n " + "ON b.idHorario = a.id\n "
							+ "WHERE a.fechainicio between TO_DATE('" + fechaIni.toString().substring(0, 19)
							+ "', 'YYYY-MM-DD HH24:MI:SS') and TO_DATE('" + fechaFin.toString().substring(0, 19)
							+ "', 'YYYY-MM-DD HH24:MI:SS')) d\n " + "ON c.idcuenta = d.idcuenta) otr\n"
							+ "ON usu.numerodocumento = otr.numerodocumento\n" + "order by nombre");
			List lista = q1.executeList();
			return lista;
		} else if (tipoDeOrdenamientoo.get(0).equals("Número de documento")) {
			System.out.println("Número de documento bien");
			Query q1 = pm.newQuery(SQL,
					"SELECT unique NUMERODOCUMENTO\n " + "FROM RESPONSABLE c INNER JOIN\n " + "(SELECT idCuenta\n "
							+ "FROM HORARIO a INNER JOIN (SELECT idCuenta, idHorario\n " + "FROM CONSUMO\n"
							+ "WHERE IDSERVICIO = " + servicioParaBuscar.get(0) + ") b\n " + "ON b.idHorario = a.id\n "
							+ "WHERE a.fechainicio between TO_DATE('" + fechaIni.toString().substring(0, 19)
							+ "', 'YYYY-MM-DD HH24:MI:SS') and TO_DATE('" + fechaFin.toString().substring(0, 19)
							+ "', 'YYYY-MM-DD HH24:MI:SS')) d\n " + "ON c.idcuenta = d.idcuenta\n "
							+ "ORDER BY c.numerodocumento");
			List lista = q1.executeList();
			return lista;
		} else {
			return null;
		}
	}

	public List consulta10(PersistenceManager pm, Timestamp fechaIni, Timestamp fechaFin, List servicioParaBuscar,
			List tipoDeOrdenamientoo) {
		if (tipoDeOrdenamientoo.get(0).equals("Fecha")) {
			System.out.println("Fecha Bien");
			Query q1 = pm.newQuery(SQL,
					"SELECT unique NUMERODOCUMENTO\n " + "FROM RESPONSABLE c INNER JOIN\n " + "(SELECT idCuenta\n "
							+ "FROM HORARIO a INNER JOIN (SELECT idCuenta, idHorario\n " + "FROM CONSUMO\n"
							+ "WHERE IDSERVICIO != " + servicioParaBuscar.get(0) + ") b\n " + "ON b.idHorario = a.id\n "
							+ "WHERE a.fechainicio between TO_DATE('" + fechaIni.toString().substring(0, 19)
							+ "', 'YYYY-MM-DD HH24:MI:SS') and TO_DATE('" + fechaFin.toString().substring(0, 19)
							+ "', 'YYYY-MM-DD HH24:MI:SS')\n" + "order by a.fechainicio) d\n "
							+ "ON c.idcuenta = d.idcuenta\n ");
			List lista = q1.executeList();
			return lista;
		} else if (tipoDeOrdenamientoo.get(0).equals("Número de veces que utilizó el servicio")) {
			System.out.println("Veces Servicio bien");
			Query q1 = pm.newQuery(SQL,
					"select numeroDocumento\n" + "FROM\n" + "(\n" + "SELECT NUMERODOCUMENTO\n "
							+ "FROM RESPONSABLE c INNER JOIN\n " + "(SELECT idCuenta\n "
							+ "FROM HORARIO a INNER JOIN (SELECT idCuenta, idHorario\n " + "FROM CONSUMO\n"
							+ "WHERE IDSERVICIO != " + servicioParaBuscar.get(0) + ") b\n " + "ON b.idHorario = a.id\n "
							+ "WHERE a.fechainicio between TO_DATE('" + fechaIni.toString().substring(0, 19)
							+ "', 'YYYY-MM-DD HH24:MI:SS') and TO_DATE('" + fechaFin.toString().substring(0, 19)
							+ "', 'YYYY-MM-DD HH24:MI:SS')) d\n " + "ON c.idcuenta = d.idcuenta\n "
							+ "ORDER BY c.numerodocumento\n" + ")\n" + "GROUP BY numeroDocumento\n"
							+ "order by COUNT(numeroDocumento) ASC");
			List lista = q1.executeList();
			return lista;
		} else if (tipoDeOrdenamientoo.get(0).equals("Nombre de usuario")) {
			System.out.println("Nombre Bien");
			Query q1 = pm.newQuery(SQL,
					"SELECT unique NOMBRE FROM USUARIO usu INNER JOIN (SELECT unique NUMERODOCUMENTO\n "
							+ "FROM RESPONSABLE c INNER JOIN\n " + "(SELECT idCuenta\n "
							+ "FROM HORARIO a INNER JOIN (SELECT idCuenta, idHorario\n " + "FROM CONSUMO\n"
							+ "WHERE IDSERVICIO != " + servicioParaBuscar.get(0) + ") b\n " + "ON b.idHorario = a.id\n "
							+ "WHERE a.fechainicio between TO_DATE('" + fechaIni.toString().substring(0, 19)
							+ "', 'YYYY-MM-DD HH24:MI:SS') and TO_DATE('" + fechaFin.toString().substring(0, 19)
							+ "', 'YYYY-MM-DD HH24:MI:SS')) d\n " + "ON c.idcuenta = d.idcuenta) otr\n"
							+ "ON usu.numerodocumento = otr.numerodocumento\n" + "order by nombre");
			List lista = q1.executeList();
			return lista;
		} else if (tipoDeOrdenamientoo.get(0).equals("Número de documento")) {
			System.out.println("Número de documento bien");
			Query q1 = pm.newQuery(SQL,
					"SELECT unique NUMERODOCUMENTO\n " + "FROM RESPONSABLE c INNER JOIN\n " + "(SELECT idCuenta\n "
							+ "FROM HORARIO a INNER JOIN (SELECT idCuenta, idHorario\n " + "FROM CONSUMO\n"
							+ "WHERE IDSERVICIO != " + servicioParaBuscar.get(0) + ") b\n " + "ON b.idHorario = a.id\n "
							+ "WHERE a.fechainicio between TO_DATE('" + fechaIni.toString().substring(0, 19)
							+ "', 'YYYY-MM-DD HH24:MI:SS') and TO_DATE('" + fechaFin.toString().substring(0, 19)
							+ "', 'YYYY-MM-DD HH24:MI:SS')) d\n " + "ON c.idcuenta = d.idcuenta\n "
							+ "ORDER BY c.numerodocumento");
			List lista = q1.executeList();
			return lista;
		} else {
			return null;
		}
	}

	public List consulta9(PersistenceManager pm, Timestamp fechaIni, Timestamp fechaFin, List servicioParaBuscar) {
		Query q1 = pm.newQuery(SQL,
				"select COUNT(numeroDocumento)\n" + "FROM\n" + "(\n" + "SELECT NUMERODOCUMENTO\n "
						+ "FROM RESPONSABLE c INNER JOIN\n " + "(SELECT idCuenta\n "
						+ "FROM HORARIO a INNER JOIN (SELECT idCuenta, idHorario\n " + "FROM CONSUMO\n"
						+ "WHERE IDSERVICIO = " + servicioParaBuscar.get(0) + ") b\n " + "ON b.idHorario = a.id\n "
						+ "WHERE a.fechainicio between TO_DATE('" + fechaIni.toString().substring(0, 19)
						+ "', 'YYYY-MM-DD HH24:MI:SS') and TO_DATE('" + fechaFin.toString().substring(0, 19)
						+ "', 'YYYY-MM-DD HH24:MI:SS')) d\n " + "ON c.idcuenta = d.idcuenta\n "
						+ ")\n" + "GROUP BY numeroDocumento\n"
						+ "order by COUNT(numeroDocumento) DESC");
		List lista = q1.executeList();
		return lista;
	}
	
	public List tablaUsuarioNumeros(PersistenceManager pm)
	{
		Query q1 = pm.newQuery(SQL,"SELECT NUMERODOCUMENTO FROM USUARIO");
		List lista = q1.executeList();
		return lista;
	}
	public List tablaUsuarioCorreos(PersistenceManager pm)
	{
		Query q1 = pm.newQuery(SQL,"SELECT correo FROM USUARIO");
		List lista = q1.executeList();
		return lista;
	}
	public List tablaResponsableNumeros(PersistenceManager pm)
	{
		Query q1 = pm.newQuery(SQL,"SELECT NUMERODOCUMENTO FROM RESPONSABLE");
		List lista = q1.executeList();
		return lista;
	}
}