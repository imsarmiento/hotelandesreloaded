package uniandes.isis2304.parranderos.persistencia;

import java.math.BigDecimal;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class SQLResponsable {
	/* **********************
	 * 			Constantes
	 ***********************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = HotelAndesPersistencia.SQL;

	/* **********************
	 * 			Atributos
	 ***********************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private HotelAndesPersistencia hp;

	/* **********************
	 * 			Métodos
	 ***********************/

	/**
	 * Constructor
	 * @param pp - El Manejador de persistencia de la aplicación
	 */
	public SQLResponsable(HotelAndesPersistencia hp)
	{
		this.hp = hp;
	}

	/**
	 * Crea y ejecuta la sentencia SQL para obtener un nuevo número de secuencia
	 * @param pm - El manejador de persistencia
	 * @return El número de secuencia generado
	 */
	public long crearResponsable(PersistenceManager pm, long numeroDocumento, String tipoDocumento, String tipo , long idcuenta)
	{
		System.out.println("numDoc"+ numeroDocumento + tipoDocumento + "cuenta" + idcuenta);
		Query q = pm.newQuery(SQL, "INSERT INTO " +hp.darTablaResponsable()+ "(NUMERODOCUMENTO, TIPODOCUMENTO, IDCUENTA, TIPO) "
				+ "VALUES (?, ?, ?, ?)");
		q.setParameters(numeroDocumento,tipoDocumento,idcuenta,tipo);
		return (long) q.executeUnique();
	}
}
