package uniandes.isis2304.parranderos.persistencia;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class SQLTiposHabitacion  {
	
	/* **********************
	 * 			Constantes
	 ***********************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = HotelAndesPersistencia.SQL;

	/* **********************
	 * 			Atributos
	 ***********************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private HotelAndesPersistencia hp;

	/* **********************
	 * 			Métodos
	 ***********************/

	/**
	 * Constructor
	 * @param pp - El Manejador de persistencia de la aplicación
	 */
	public SQLTiposHabitacion(HotelAndesPersistencia hp)
	{
		this.hp = hp;
	}

	/**
	 * Crea y ejecuta la sentencia SQL para obtener un nuevo número de secuencia
	 * @param pm - El manejador de persistencia
	 * @return El número de secuencia generado
	 */
	public ArrayList<String> darTipos(PersistenceManager pm)
	{
		ArrayList<String> result = new ArrayList<String>();
		Query q = pm.newQuery(SQL,"SELECT TIPO_HABITACION.NOMBRE FROM TIPO_HABITACION");
		List lista = q.executeList();
		for(Object l : lista)
		{
			result.add(l.toString());
			System.out.println(l.toString());

		}
		return result;
	}

	public List darTodasHabitacionesR15(PersistenceManager pm) {
		Query q = pm.newQuery(SQL, "SELECT idHabitacion FROM HABITACION");
		List lista = q.executeList();
		return lista;
	}
}
