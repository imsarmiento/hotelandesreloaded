package uniandes.isis2304.parranderos.persistencia;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class SQLHabitacion {
	
	/* ********
	 * 			Constantes
	 *********/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = HotelAndesPersistencia.SQL;

	/* ********
	 * 			Atributos
	 *********/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private HotelAndesPersistencia hp;

	/* ********
	 * 			Métodos
	 *********/

	/**
	 * Constructor
	 * @param pp - El Manejador de persistencia de la aplicación
	 */
	public SQLHabitacion(HotelAndesPersistencia hp)
	{
		this.hp = hp;
	}

	/**
	 * Crea y ejecuta la sentencia SQL para obtener un nuevo número de secuencia
	 * @param pm - El manejador de persistencia
	 * @return El número de secuencia generado
	 */
	public List<BigDecimal> reservarHabitaciones(PersistenceManager pm, String tipo, int cantidad, String fechaIn, String fechaOut)
	{
		String respuesta = "";
		
		//LA CANTIDAD DE HABITACIONES
		Query q2 = pm.newQuery(SQL,""
				+ "SELECT *\n" + 
				"FROM HABITACION\n" + 
				"INNER JOIN TIPO_HABITACION ON HABITACION.TIPO = TIPO_HABITACION.ID\n" + 
				"WHERE TIPO_HABITACION.NOMBRE = '"+tipo+"'");
		List lista2 = q2.executeList();
		if (lista2.size() < cantidad){
			return null;
		}
		
		//LA CANTIDAD DE HABITACIONES RESERVADAS EN ESA FECHA
		Query q1 = pm.newQuery(SQL,""
				+ "SELECT HABITACION.IDHABITACION\n" + 
				"FROM HABITACION\n" + 
				"INNER JOIN TIPO_HABITACION ON HABITACION.TIPO = TIPO_HABITACION.ID\n" + 
				"INNER JOIN CUENTA ON CUENTA.IDHABITACION = HABITACION.IDHABITACION\n" + 
				"WHERE FECHAIN <= TO_DATE('"+fechaIn+"', 'YYYY-MM-DD HH24:MI:SS')\n" + 
				"AND FECHAOUT >= TO_DATE('"+fechaOut+"', 'YYYY-MM-DD HH24:MI:SS')\n" + 
				"AND TIPO_HABITACION.NOMBRE = '"+tipo+"'");
		List lista = q1.executeList();
		if (lista2.size() - lista.size() < cantidad){
			return null;
		}
		else
		{
			//las habitaciones no reservadas
			Query q3 = pm.newQuery(SQL,""
					+ "SELECT HABITACION.IDHABITACION\n" + 
					"FROM HABITACION\n" + 
					"INNER JOIN TIPO_HABITACION ON HABITACION.TIPO = TIPO_HABITACION.ID\n" + 
					"AND TIPO_HABITACION.NOMBRE = '"+tipo+"'\n" + 
					"AND  HABITACION.IDHABITACION NOT IN \n" + 
					"(SELECT HABITACION.IDHABITACION\n" + 
					"FROM HABITACION\n" + 
					"INNER JOIN TIPO_HABITACION ON HABITACION.TIPO = TIPO_HABITACION.ID\n" + 
					"INNER JOIN CUENTA ON CUENTA.IDHABITACION = HABITACION.IDHABITACION\n" + 
					"WHERE FECHAIN <= TO_DATE('"+fechaIn+"', 'YYYY-MM-DD HH24:MI:SS')\n" + 
					"AND FECHAOUT >= TO_DATE('"+fechaOut+"', 'YYYY-MM-DD HH24:MI:SS')\n" + 
					"AND TIPO_HABITACION.NOMBRE = '"+tipo+"')");
			List lista3 = q3.executeList();
			return lista3;
		}
		
	}
}