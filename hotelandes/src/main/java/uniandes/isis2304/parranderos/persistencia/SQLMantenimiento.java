package uniandes.isis2304.parranderos.persistencia;

import java.math.BigDecimal;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class SQLMantenimiento {

	/*
	 * ****************************************************************
	 * Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las
	 * sentencias de acceso a la base de datos Se renombra acá para facilitar la
	 * escritura de las sentencias
	 */
	private final static String SQL = HotelAndesPersistencia.SQL;

	/*
	 * ****************************************************************
	 * Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private HotelAndesPersistencia pp;
	
	/**
	 * id del último manteimiento agregado
	 */
	private long idUltimo;
	
	/*
	 * **************************************************************** Métodos
	 *****************************************************************/

	/**
	 * Constructor
	 * 
	 * @param pp
	 *            - El Manejador de persistencia de la aplicación
	 */
	public SQLMantenimiento(HotelAndesPersistencia pp) {
		this.pp = pp;
		idUltimo = (long)( Math.random()*10000);
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un PlanUso a la base de datos de HotelAndes
	 * @param pm - El manejador de persistencia
	 * @param idPlanUso - El identificador del planUso
	 * @param tipoPlanUso - El tipo del Plan uso ()
	 * @param costoPlanUso - El costo del plan de uso
	 * @param descuentoPlanUso - El descuento del plan uso
	 * @param nombrePromocion - el nombre de la promocion
	 * @return El número de tuplas insertadas
	 */
	public long nuevoMantenimientoR15(PersistenceManager pm, BigDecimal idServicio, BigDecimal idHabitacion, Long idHorario, String causa, int estado) {
		// TODO Auto-generated method stub
		idUltimo++;
		System.out.println("llego a sql Consumo");
		try{
			Query q = pm.newQuery(SQL, "INSERT INTO " + pp.darTablaMantenimiento() + "(id,idServicio,idHabitacion,idHorario,causa,estado) values ("+ idUltimo +"," +idServicio+"," +idHabitacion+","+idHorario+",'"+causa+"'," + estado +")");
			return (long) q.executeUnique();	
		}catch(Exception e){
			Query q = pm.newQuery(SQL, "INSERT INTO " + pp.darTablaMantenimiento() + "(id,idServicio,idHabitacion,idHorario,causa,estado) values ("+ idUltimo++ +"," +idServicio+"," +idHabitacion+","+idHorario+",'"+causa+"'," + estado +")");
			return (long) q.executeUnique();	
		}
	}

	public List traerMantenimientosR16(PersistenceManager pm) {
		// TODO Auto-generated method stub
		Query q1 = pm.newQuery(SQL, "SELECT id FROM MANTENIMIENTO WHERE estado = 0");
		return q1.executeList();
	}

	public void cancelarHabitacionesConvencionR13(PersistenceManager pm, BigDecimal habitacionesParaMantenimiento) {
		// TODO Auto-generated method stub
		Query q2 = pm.newQuery(SQL, "UPDATE Mantenimiento SET estado = 1 WHERE id = " + habitacionesParaMantenimiento);
		q2.executeUnique();
	}
}
