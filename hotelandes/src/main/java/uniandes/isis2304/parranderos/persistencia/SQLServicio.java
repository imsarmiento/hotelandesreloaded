package uniandes.isis2304.parranderos.persistencia;

import java.math.BigDecimal;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class SQLServicio {
	/* **********************
	 * 			Constantes
	 ***********************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = HotelAndesPersistencia.SQL;

	/* **********************
	 * 			Atributos
	 ***********************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private HotelAndesPersistencia hp;

	/* **********************
	 * 			Métodos
	 ***********************/

	/**
	 * Constructor
	 * @param pp - El Manejador de persistencia de la aplicación
	 */
	public SQLServicio (HotelAndesPersistencia hp)
	{
		this.hp = hp;
	}

	/**
	 * Crea y ejecuta la sentencia SQL para obtener un nuevo número de secuencia
	 * @param pm - El manejador de persistencia
	 * @return El número de secuencia generado
	 */
	public boolean reservarR8(PersistenceManager pm, long idServicio )
	{
		Query q = pm.newQuery(SQL, "SELECT RESERVADO FROM SERVICIO WHERE ID = " + idServicio);

		BigDecimal reservado = (BigDecimal) q.executeList().get(0);		

		if(reservado.compareTo(BigDecimal.ZERO)==0) 
		{
			Query q1 = pm.newQuery(SQL, "UPDATE SERVICIO SET RESERVADO = 1 WHERE ID = " + idServicio);
			q1.executeUnique();
			return true;
		}
		else
		return false;

	}
	
	public BigDecimal darCapacidad(PersistenceManager pm,Long idServicio)
	{
		// TODO Auto-generated method stub
		Query q1 = pm.newQuery(SQL, "SELECT VENTAPRODUCTO.CAPACIDAD\n" + 
				"FROM VENTAPRODUCTO\n" + 
				"WHERE VENTAPRODUCTO.ID=?");
		q1.setParameters(idServicio);
		List lista = q1.executeList();
		if (lista.size() != 0){
			return (BigDecimal) lista.get(0);
		}
		else
		{
			Query q2 = pm.newQuery(SQL, "SELECT CAPACIDAD\n" + 
					"FROM ALQUILERSALONES\n" + 
					"WHERE ID = ?");
			q2.setParameters(idServicio);
			List lista2 = q2.executeList();
			return (BigDecimal) lista2.get(0);
		}
	}

	public List darTodosServiciosR15(PersistenceManager pm) {
		Query q = pm.newQuery(SQL, "SELECT id FROM SERVICIO");
		List lista = q.executeList();
		return lista;
	}
	
}