-- REGISTRAR TIPO DE HABITACIÓN

insert into TIPO_HABITACION (ID, nombre, capacidad, costo, dotacion) values (1, 'Suite presidencial', 4, 100, 'Con todo bb');
insert into TIPO_HABITACION (ID, nombre, capacidad, costo, dotacion) values (2, 'Familiar', 8, 10, 'Con comedor y cocina');
insert into TIPO_HABITACION (ID, nombre, capacidad, costo, dotacion) values (3, 'Doble', 3, 5, 'Con jacuzzi');

commit;