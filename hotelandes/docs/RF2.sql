--Los usuarios tienen una identificación (Tipo de documento, número de documento), un nombre, un correo electrónico y su rol respectivo. Esta operación es realizada por el administrador de datos del hotel. Considere inicialmente un gerente, un administrador de datos, 2 recepcionistas, 4 empleados y 4 clientes.
insert into USUARIO (numerodocumento, tipodocumento, nombre, correo, rol) values (1019, 'CC', 'Paula', 'pa@uniandes.edu.co', 6);
insert into USUARIO (numerodocumento, tipodocumento, nombre, correo, rol) values (1020, 'CC', 'Isabela', 'im@uniandes.edu.co', 5);
insert into USUARIO (numerodocumento, tipodocumento, nombre, correo, rol) values (1021, 'CC', 'Dany', 'da@uniandes.edu.co', 4);
insert into USUARIO (numerodocumento, tipodocumento, nombre, correo, rol) values (1022, 'CC', 'Allan', 'all@uniandes.edu.co', 2);
insert into USUARIO (numerodocumento, tipodocumento, nombre, correo, rol) values (1023, 'CC', 'Brito', 'br@uniandes.edu.co', 2);
insert into USUARIO (numerodocumento, tipodocumento, nombre, correo, rol) values (1024, 'CC', 'Felipe', 'fe@uniandes.edu.co', 3);
insert into USUARIO (numerodocumento, tipodocumento, nombre, correo, rol) values (1025, 'CC', 'Sofia', 'so@uniandes.edu.co', 3);
insert into USUARIO (numerodocumento, tipodocumento, nombre, correo, rol) values (1026, 'CC', 'Chiara', 'ch@uniandes.edu.co', 3);
insert into USUARIO (numerodocumento, tipodocumento, nombre, correo, rol) values (1027, 'CC', 'Daniela', 'dani@uniandes.edu.co', 3);
insert into USUARIO (numerodocumento, tipodocumento, nombre, correo, rol) values (1028, 'CC', 'Nestor', 'ne@uniandes.edu.co', 1);
insert into USUARIO (numerodocumento, tipodocumento, nombre, correo, rol) values (1029, 'CC', 'Claudia', 'ca@uniandes.edu.co', 1);
insert into USUARIO (numerodocumento, tipodocumento, nombre, correo, rol) values (1030, 'CC', 'Gloria', 'go@uniandes.edu.co', 1);
insert into USUARIO (numerodocumento, tipodocumento, nombre, correo, rol) values (1031, 'CC', 'Samuel', 'sam@uniandes.edu.co', 1);

commit;