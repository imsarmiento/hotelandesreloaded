
SELECT x.puestosLlenos, x.idHabitacion, y.capacidad AS puestosDisponibles
FROM TipoHabitacion y INNER JOIN
(SELECT idHabitacion, COUNT(*) AS puestosLlenos
FROM Habitacion a INNER JOIN 
(SELECT idHabitacion AS hab
FROM Reserva NATURAL JOIN Cliente) d
ON a.idHabitacion = d.hab
GROUP BY COUNT(*)) x